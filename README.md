# Predictive Features of Gene Expression Variation Reveal a Mechanistic Link Between Expression Variation and Differential Expression

<div align="justify">

This repository contains code to reproduce the results of our paper [Predictive features of gene expression variation reveal a mechanistic link between expression variation and differential expression](https://www.biorxiv.org/content/10.1101/2020.02.10.942276v1) by Olga Sigalova, Amirreza Shaeiri, Mattia Forneris, [Eileen Furlong](http://furlonglab.embl.de/), and [Judith Zaugg](https://www.zaugg.embl.de/).

</div>

![synopsis](/images/synopsis_image_resized.jpg)

<div align="justify">

We used a machine-learning approach to identify genomic features predictive of gene expression variation across individuals in Drosophila and human and applied this framework to link differential expression upon various perturbations to genomically encoded regulatory features.

* Expression variation across individuals is a robust gene-specific property
* Expression variation is informative of gene regulation 
* Promoter architecture and overall gene regulatory complexity are predictive of gene expression variation 
* Expression variation features could also predict differential expression upon various perturbations

</div>

## Installation

Clone this repo:

```bash
$ git clone git@git.embl.de:sigalova/regulation_of_expression_variation.git YOUR_PROJECT_NAME
$ cd YOUR_PROJECT_NAME
```

Install the preliminary dependencies:

* Make sure your are running bash version > 4
* Make sure `yq` is available on your machine (https://github.com/mikefarah/yq)


## Generating the Results

* You can check `/src/Drosophila` to generate the Drosophila related results
* You can check `src/Human` to generate the Human related results
* In case you want to access the final code for for the figures in the manuscript, you can check `/manuscript`


## Citation
Please use the following bibtex for citing this work:

    @article {Sigalova2020.02.10.942276,
	author = {Sigalova, Olga M. and Shaeiri, Amirreza and Forneris, Mattia and Furlong, Eileen E.M. and Zaugg, Judith B},
	title = {Predictive features of gene expression variation reveal a mechanistic link between expression variation and differential expression},
	elocation-id = {2020.02.10.942276},
	year = {2020},
	doi = {10.1101/2020.02.10.942276},
	publisher = {}



<!--# Installation-->

<!--Clone this repo-->

<!--```bash-->
<!--$ git clone git@git.embl.de:grp-furlong/project-backbone.git YOUR_PROJECT_NAME-->
<!--$ cd YOUR_PROJECT_NAME-->
<!--```-->

<!--Install the dependencies:-->

<!--TO UPDATE-->

<!--* Make sure your are running bash version > 4-->
<!--* Make sure `yq` is available on your machine (https://github.com/mikefarah/yq)-->
