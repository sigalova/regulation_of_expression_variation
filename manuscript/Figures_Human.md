-   [Figure 6. Features in human promoters predict both expression
    variation and differential
    expression.](#figure-6.-features-in-human-promoters-predict-both-expression-variation-and-differential-expression.)
    -   [Figure 6a](#figure-6a)
    -   [Figure 6b](#figure-6b)
    -   [Figure 6c,e](#figure-6ce)
    -   [Figure 6d](#figure-6d)
    -   [Figure 6f](#figure-6f)
-   [Supplementary figure 6](#supplementary-figure-6)
-   [Supplementary figure 7](#supplementary-figure-7)
    -   [Figure S7d](#figure-s7d)
-   [Supplementary figure 8](#supplementary-figure-8)
    -   [Figure S8a](#figure-s8a)
    -   [Figure S8b](#figure-s8b)
    -   [Figure S8c](#figure-s8c)
-   [Session Info](#session-info)

This is Markdown document that generates Figure 6 and Supplementary
Figures 6-8 (validation with human GTEx data) from the Manuscript.

    suppressPackageStartupMessages(library(tidyverse))
    suppressPackageStartupMessages(library(magrittr))
    suppressPackageStartupMessages(library(data.table))
    suppressPackageStartupMessages(library(ggpubr))
    suppressPackageStartupMessages(library(ggrepel))
    suppressPackageStartupMessages(library(RColorBrewer))
    suppressPackageStartupMessages(library(gridExtra))
    suppressPackageStartupMessages(library(ggExtra))
    suppressPackageStartupMessages(library(pheatmap))
    suppressPackageStartupMessages(library(rcompanion))
    suppressPackageStartupMessages(library(psych))
    options(stringsAsFactors = FALSE)

    source("/g/furlong/project/62_expression_variation/src/shared/utils.R")
    config = load_config() # json file with all paths to data
    times = load_times() # time-points used for analysis
    project_folder = config$project_parameters$project_dir
    human_path1 = file.path(project_folder, "/analysis/human_data")
    human_path2 = "/g/scb2/zaugg/shaeiri/variation_prediction/table/final"

    project_folder = config$project_parameters$project_dir

    outdir = file.path(project_folder, "/manuscript/Figures")

    df1 = read.csv(file.path(human_path2, "all_variations2.csv")) # per tissue expression level and variation
    df2 = read.csv(file.path(human_path2, "share.csv")) # average variation
    df3 = read.csv(file.path(human_path2, "prior.csv")) # DE prior 

Figure 6. Features in human promoters predict both expression variation and differential expression.
====================================================================================================

Figure 6a
---------

Performance of random forest predictions (mean R^2 from 5-fold
cross-validation, whiskers = standard deviation) for expression level
(orange) and variation (blue) trained on expression variation in
tissue-specific RNA-seq (lung, ovary, and muscle), as well as mean
variation across 43 tissues (Methods).

    rf_res_path =  file.path(human_path1, "Random_forest_results.csv")  
    res = read.csv(rf_res_path)
    res %<>% filter(response != "DE prior")

    df_sum = res %>% 
      group_by(type, response) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) 

    df_sum %<>% mutate(set = paste0(response, " (", type, ")"))

    df_sum$set = factor(df_sum$set, levels = rev(df_sum$set))


    ggplot(df_sum, aes(x = set, y = mean_rsq, fill = response)) + 
      geom_bar(stat = "identity", position=position_dodge(.9)) +  
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.7, size = 0.2) +
      theme_bw() + 
      scale_alpha_manual(values = c(0.5, 1), guide = FALSE) + 
      xlab("")  + 
      ylab("R^2, 5-fold cross validation") +
      scale_y_continuous(limits = c(-0.05, 0.60), breaks = seq(0.1, 0.5, by = 0.1)) +
      coord_flip() + 
      scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
      theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=18), 
            axis.title.x = element_text(size=22), axis.title.y = element_text(size=22),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-2-1.svg)

    ggsave(filename = file.path(outdir, "fig6a_rf_performance_human.pdf"), width = 7, height = 5)

Figure 6b
---------

Top-20 features for predicting expression variation using Boruta feature
selection. Features ordered by their importance for expression variation
(blue), showing the corresponding importance for level (orange). Shapes
indicate four different datasets (three tissues and mean variation).

    path = file.path(human_path1, "Boruta_median_importance_results.csv")

    df = read.csv(path)
    df %<>% filter(responce != "DE prior") %>% mutate(feature = gsub("_", " ", feature)) 

    # top features
    tmp = df %>% arrange(-median_importance) %>% filter(responce == "variation") %>% select(feature) %>% unique() %>% unlist()
    rl = c("mean BivFlnk", "transcript width", "mean EnhBiv", "mean ReprPC", "mean exon length", "mean Quies", "GC num", "CpG num", "number of transcripts", "mean TssAFlnk", 
           "mean TxWk", "promoter width", "exon length mean absolute deviation" )

    features_sel = setdiff(tmp, rl)[1:20]

    df %<>% filter(feature %in% features_sel) 

    df$feature = factor(df$feature, levels = features_sel)

    #df[is.na(df)] = 0.1 # pseudocount for non-significant features

    ggplot(df, aes(x = feature, y = median_importance, fill = responce, color = responce, shape = factor(tissue))) + 
      geom_point(size = 10, alpha = 0.8) +
      theme_bw() +
      ylab("Median feature importance") + 
      xlab("") +
      scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
      scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
      scale_shape_manual(values = c(21, 22, 24, 25), name = "Dataset") +
      guides(color = guide_legend(override.aes = list(size=8), order = 2),
             shape = guide_legend(override.aes = list(size=8, fill = "black"), order = 1)) +
      theme(axis.text.x = element_text(size=25, angle = 90), axis.text.y = element_text(size=25, hjust=0.95,vjust=0.2), 
            axis.title.y = element_text(size=25), axis.title.x = element_text(size=30),
            legend.text=element_text(size=30), legend.title=element_text(size=30),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.justification = "top")

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-3-1.svg)

    ggsave(filename = file.path(outdir, "fig6b_rf_top_features_human.eps"), device = cairo_ps, width = 20, height = 8) # eps format is parsed better in illustrator

Figure 6c,e
-----------

Differences in expression variation (c) and DE prior (e) for some of the
top-predictive features from (b). ‘Share TssBiv &gt; 0’ indicates genes
that have “TSS bivalent” chromatin state (chomHMM, Methods) in at least
one tissue. ‘Share broad &gt; 0.8’ indicates genes which have broad
promoter in at least 80% of tissues where it is expressed. ‘\#TF &gt;
20’ indicates genes with more than 20 different TF peaks in TSS-proximal
region. ‘With TATA-box’ and ‘With CGI’ indicate presence of TATA-box and
CpG island in gene core promoter, respectively. (Methods) P-values =
Wilcoxon test, number of genes indicated. Cohen’s d in (c): 0.72 (‘Share
TssBiv &gt;0’), 0.93 (‘Share broad &gt; 0.8’), 1.43 (‘\#TF &gt; 20’),
1.02 (‘With TATA-box’), 1.03 (‘With CGI’). Cohen’s d in (e): 0.71, 0.48,
0.5, 0.52, 0.35, respectively (same order as in (c)).

**Mean variation**

    fun_boxplot = function(p, xlab = "", ylab = "expression variation", comp = list(c("True", "False")), col = cbPalette) {
      
      p +
      geom_violin(alpha = 0.5, width = 0.8) +
      geom_boxplot(width = 0.4, outlier.size = 0.1) +
      stat_compare_means(comparisons = comp) + 
      theme_bw() + 
     # ylim(c(-1.06, 2.2)) +  
      scale_fill_manual(values = col, guide = FALSE) +
      xlab(xlab) + 
      ylab(ylab) + 
      stat_summary(fun.data = function(x) c(y = median(x) + 0.1, label = length(x)), geom = "text", size = 5, fontface = "bold") +
      theme(axis.text.y = element_text(size=14), 
            axis.text.x = element_text(size = 14), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position = c(0, 1), legend.justification = c(-0.2, 1.1),
            legend.background = element_rect(fill = "transparent"),
            plot.margin = unit(c(0.75, 0.75, 0.75, 0.05), "cm"))
      
    }

    mt = df2

    mt$biv = ifelse(mt$mean_TssBiv > 0, "True", "False")
    p1 = fun_boxplot(ggplot(mt, aes(y = mean_variation, fill = biv, x = biv)), ylab = "Mean variation") + ggtitle("Share TssBiv > 0")

    mt$shape = ifelse(mt$percent_of_broad > 0.8, "True", "False")
    mt$shape = factor(mt$shape, levels = c("True", "False"))
    p2 = fun_boxplot(ggplot(mt, aes(y = mean_variation, fill = shape, x = shape)), ylab = "Mean variation") + ggtitle("Share broad > 0.8")

    mt$high_tf = ifelse(mt$number_of_TFs > 20, "True", "False")
    mt$high_tf  = factor(mt$high_tf, levels = c("True", "False"))
    p3 = fun_boxplot(ggplot(mt, aes(y = mean_variation, fill = high_tf, x = high_tf)), ylab = "Mean variation")  + ggtitle("# TF > 20")

    mt$tata = ifelse(mt$TATA_box > 0, "True", "False")
    p4 = fun_boxplot(ggplot(mt, aes(y = mean_variation, fill = tata, x = tata)), ylab = "Mean variation") + ggtitle("With TATA-box")

    mt$cpg = ifelse(mt$CpG_length > 0, "True", "False")
    mt$cpg = factor(mt$cpg, levels = c("True", "False"))
    p5 = fun_boxplot(ggplot(mt, aes(y = mean_variation, fill = cpg, x = cpg)), ylab = "Mean variation") + ggtitle("With CGI")



    p = grid.arrange(p1, p2, p3, p4, p5, ncol = 5)

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-4-1.svg)

    ggsave(filename = file.path(outdir, "fig6c_feature_examples_human_variation.pdf"), p, width = 12, height = 5) 

and Cohen’s d (absolute values):

    features = c("biv", "shape", "high_tf", "tata", "cpg")


    vapply(features, 
           function(x) { tmp = mt[ , c(x, "mean_variation")]  
                         tmp[, x] = ifelse(tmp[, x] == "True", 1, 0);
                         round(abs(cohen.d(tmp, x)$cohen.d[2]), 2)}, numeric(1))

    ##     biv   shape high_tf    tata     cpg 
    ##    0.72    0.93    1.43    1.02    1.03

**DE Prior**

    mt = df3

    mt$biv = ifelse(mt$mean_TssBiv > 0, "True", "False")
    p1 = fun_boxplot(ggplot(mt, aes(y = DE_Prior_Rank, fill = biv, x = biv)), ylab = "DE Prior") + ggtitle("Share TssBiv > 0")

    mt$shape = ifelse(mt$percent_of_broad > 0.8, "True", "False")
    mt$shape = factor(mt$shape, levels = c("True", "False"))
    p2 = fun_boxplot(ggplot(mt, aes(y = DE_Prior_Rank, fill = shape, x = shape)), ylab = "DE Prior") + ggtitle("Share broad > 0.8")

    mt$high_tf = ifelse(mt$number_of_TFs > 20, "True", "False")
    mt$high_tf  = factor(mt$high_tf, levels = c("True", "False"))
    p3 = fun_boxplot(ggplot(mt, aes(y = DE_Prior_Rank, fill = high_tf, x = high_tf)), ylab = "DE Prior")  + ggtitle("# TF > 20")

    mt$tata = ifelse(mt$TATA_box > 0, "True", "False")
    p4 = fun_boxplot(ggplot(mt, aes(y = DE_Prior_Rank, fill = tata, x = tata)), ylab = "DE Prior") + ggtitle("With TATA-box")

    mt$cpg = ifelse(mt$CpG_length > 0, "True", "False")
    mt$cpg = factor(mt$cpg, levels = c("True", "False"))
    p5 = fun_boxplot(ggplot(mt, aes(y = DE_Prior_Rank, fill = cpg, x = cpg)), ylab = "DE Prior") + ggtitle("With CGI")


    p = grid.arrange(p1, p2, p3, p4, p5, ncol = 5)

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-6-1.svg)

    ggsave(filename = file.path(outdir, "fig6e_feature_examples_human_prior.pdf"), p, width = 12, height = 5) 

and Cohen’s d:

    features = c("biv", "shape", "high_tf", "tata", "cpg")


    vapply(features, 
           function(x) { tmp = mt[ , c(x, "DE_Prior_Rank")]  
                         tmp[, x] = ifelse(tmp[, x] == "True", 1, 0);
                         round(abs(cohen.d(tmp, x)$cohen.d[2]), 2)}, numeric(1))

    ##     biv   shape high_tf    tata     cpg 
    ##    0.71    0.48    0.50    0.52    0.35

Figure 6d
---------

ROC-curves for predicting DE prior (top-30% variable vs. bottom-30%)
with random forest models trained on DE prior (light blue) and mean
expression variation (dark blue). Models trained and tested on
non-overlapping subsets of genes in 10 random sampling rounds (all
plotted), with median AUC values indicated.

    dir = file.path(project_folder, "/analysis/human_data")
    pred_all = read.csv(file.path(dir,  "transfer_learning_performance_human.csv"))

    # median AUC across 10 iteration for models trained on prior and variation
    auc_sum = pred_all %>% group_by(learner) %>% summarize(auc = median(AUC))

    rocl_labels = c(paste0("Train on DE prior, AUC = ", round(auc_sum[1, 2], 2)),
                    paste0("Train on mean variation, AUC = ", round(auc_sum[2, 2], 2)))

    ggplot(pred_all, aes(x = fpr, y = tpr, color = learner, group = interaction(i, learner))) +
      geom_line(size = 0.3) + 
      geom_abline(intercept = 0, slope = 1, linetype = "dashed") +
      theme_bw() +
      xlab("False positive rate") +
      ylab("True positive rate") +
      scale_color_manual(values = c("#B0C4DE", "#4682B4"), name = "Training dataset:", labels = rocl_labels) +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=14), axis.title.y = element_text(size=14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.title = element_text(size=12), legend.text = element_text(size=12),
            legend.position = c(1, 0), legend.justification = c(1.02, -0.2))

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-8-1.svg)

    ggsave(filename = file.path(outdir, "fig6d_transfer_roc_curves.pdf"), width = 6, height = 4)

Figure 6f
---------

Mean expression variation of specific genes groups (GWAS hits, essential
genes, drug targets) compared to the distribution of mean expression
variation for all genes in the dataset. Cohen’s d: 0.2, 0.71, and 0.74
for GWAS catalogue, Drug targets, and Essential genes respectively
(comparison to All genes).

    # this table contains lists of essential genes, drug targets, disease genes and GWAS catalog 
    path = file.path(human_path2, "disease.csv")
    gene_groups = read.csv(path)
    gene_groups = data.frame(sapply(gene_groups, function(x) {x = gsub("YES", "True", x); gsub("NO", "False", x)}))
    gene_groups$mean_variation = as.numeric(gene_groups$mean_variation)
    gene_groups$DE_Prior_Rank = as.numeric(gene_groups$DE_Prior_Rank)
    gene_groups$drug_targets = ifelse(gene_groups$drug_targets_nelson.tsv == "True" | gene_groups$fda_approved_drug_targets.tsv == "True", "True", "False")

    # EBI GWAS list - used instead of GWAS catalog above
    path = file.path(human_path2, "disease2.csv")
    ebi_gwas = read.csv(path) %>% na.omit() %>% filter(UPSTREAM_GENE_ID == "YES" | DOWNSTREAM_GENE_ID == "YES") %>% 
      select(gene_id) %>% unlist(use.names = FALSE)
    gene_groups$gwas_ebi = ifelse(gene_groups$gene_id %in% ebi_gwas, "True", "False")


    # selecting essential genes, dominant disease genes, GWAS catalog and drug targets
    tmp = gene_groups %>% 
      select(mean_variation,DE_Prior_Rank, CEGv2_subset_universe.tsv, drug_targets, gwas_ebi, 
             #clinvar_path_likelypath.tsv, 
             all_ad.tsv) %>% 
      rename(`Essential genes` = CEGv2_subset_universe.tsv, `Drug targets` = drug_targets,
             `GWAS catalog` = gwas_ebi,
         #    `Disease genes (Clinvar)` = clinvar_path_likelypath.tsv,
             `Disease genes (Dominant)` = all_ad.tsv) %>%
      gather(group_type, gene_group, -mean_variation, -DE_Prior_Rank) %>%
      mutate(group_type = factor(group_type))
    # excluding SNP_GENE_IDS == "YES"

    tmp$group_type = relevel(tmp$group_type, ref = "Essential genes")
    tmp %>% group_by(group_type, gene_group) %>% summarize(median(mean_variation), median(DE_Prior_Rank, na.rm = TRUE), n())

    ## # A tibble: 8 x 5
    ## # Groups:   group_type [?]
    ##   group_type     gene_group `median(mean_vari… `median(DE_Prior_Ran… `n()`
    ##   <fct>          <chr>                   <dbl>                 <dbl> <int>
    ## 1 Essential gen… False                  0.0881                 0.561 10902
    ## 2 Essential gen… True                  -0.242                  0.344   410
    ## 3 Disease genes… False                  0.0517                 0.544 10843
    ## 4 Disease genes… True                   0.400                  0.731   469
    ## 5 Drug targets   False                  0.0551                 0.547 11008
    ## 6 Drug targets   True                   0.522                  0.764   304
    ## 7 GWAS catalog   False                 -0.0124                 0.508  7410
    ## 8 GWAS catalog   True                   0.231                  0.643  3902

    tmp2 = rbind.data.frame(
      tmp %>% filter(gene_group == "True") %>% select(-gene_group),
      gene_groups %>% select(mean_variation, DE_Prior_Rank) %>% mutate(group_type = "All genes")
    )

    # do without disease genes
    tmp2 %<>% filter(group_type != "Disease genes (Dominant)")

    tmp2$group_type = relevel(tmp2$group_type, ref = "All genes")

    pal1 = c("darkgrey", "#E69F00", "#009E73", "darksalmon")
    pal2 = c("black", "darkblue", "darkgreen", "darkred")
    ggplot(tmp2, aes(x = mean_variation, color = group_type, fill = group_type)) + geom_density(alpha = 0.5) +
      scale_fill_manual(values = pal1) +
      scale_color_manual(values = pal2) +
      theme_bw() +
      xlab("Mean variation") +
      ylab("Density") +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=14), axis.title.y = element_text(size=14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.title = element_text(size=12), legend.text = element_text(size=12))

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-10-1.svg)

    ggsave(filename = file.path(outdir, "fig6f_gene_groups.pdf"), width = 8, height = 4)

Wilcoxon test p-values:

    wilcox.test(mean_variation ~ group_type, tmp2 %>% filter(group_type %in% c("All genes", "GWAS catalog")))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  mean_variation by group_type
    ## W = 19374000, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

    wilcox.test(mean_variation ~ group_type, tmp2 %>% filter(group_type %in% c("All genes", "Drug targets")))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  mean_variation by group_type
    ## W = 1043800, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

    wilcox.test(mean_variation ~ group_type, tmp2 %>% filter(group_type %in% c("All genes", "Essential genes")))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  mean_variation by group_type
    ## W = 3353600, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

Cohen’s d (absolute values):

    groups = c("GWAS catalog", "Drug targets", "Essential genes")

    vapply(groups, 
           function(x) {df = tmp2[tmp2$group_type %in% c("All genes", x), ] 
                        df %<>% select(mean_variation, group_type)
                        df$group_type = ifelse(df$group_type == "All genes", 0, 1)
                        round(abs(cohen.d(df, "group_type")$cohen.d[2]), 2)}, 
           numeric(1))

    ##    GWAS catalog    Drug targets Essential genes 
    ##            0.20            0.71            0.74

Supplementary figure 6
======================

    df11 = df1[ , grep("gene_id|_resid_cv|_median", names(df1))] %>% select(-mean_median)  
    df12 = df2[ , grep("gene_id|mean_variation|mean_median", names(df2))] 
    df13 = df3[ , grep("gene_id|DE_Prior_Rank", names(df3))] 

    df = merge_df_list(list(df11, df12, df13), all_both = TRUE, merge_by = "gene_id") %>% select(-gene_id)

    names(df) = gsub("\\.+", " ", names(df))
    names(df) = gsub("_", " ", names(df))

    df %<>% rename(`Mean variation (by tissue adj)` = `mean variation`,
                       `Mean variation (global adj)` = `mean variation global`,
                       `Mean expression level` = `mean median`)

    cor_var = cor(df, use = "pairwise.complete.obs", method = "spearman")

    pheatmap(cor_var, 
              filename = file.path(outdir, "fig6_tissue_correlations_var_lev.pdf"), width = 16, height = 16)

    pheatmap(cor_var) # same code to print to screen

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-14-1.svg)

Supplementary figure 7
======================

Figure S7d
----------

    path = file.path(human_path2, "shapes.csv")
    df = read.csv(path)

    df1 = df[ , grep("width_", names(df))] 

    names(df1) = gsub("__", "_", names(df1))


    cor_var = abs(cor(df1, use = "pairwise.complete.obs", method = "spearman"))

    cor = as.matrix(round(cor_var , 2))
    upper_tri = get_upper_tri(cor)

    # Melt the correlation matrix
    melted_cormat <- melt(upper_tri, na.rm = TRUE)

    ggplot(melted_cormat, aes(Var2, Var1, fill = value))+
     geom_tile(color = "white")+
     scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
       midpoint = 0.5, limit = c(0,1), space = "Lab", 
        name="Spearman Correlation") +
      theme_minimal()+ # minimal theme
     theme(axis.text.x = element_text(angle = 45, vjust = 1, 
        size = 12, hjust = 1))+
     coord_fixed() + 
      geom_text(aes(Var2, Var1, label = value), color = "black", size = 4) +
      theme(
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        panel.grid.major = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank(),
        axis.ticks = element_blank(),
        legend.justification = c(1, 0),
        legend.position = c(0.6, 0.7),
        legend.direction = "horizontal")+
        guides(fill = guide_colorbar(barwidth = 7, barheight = 1,
                      title.position = "top", title.hjust = 0.5))

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-15-1.svg)

    ggsave(file.path(outdir, "figS7d_tissue_correlations_width.pdf"), width = 16, height = 16)

Not used - correlation between promoter width and shape index:

    path = "/g/scb2/zaugg/shaeiri/variation_prediction/table/final/shapes.csv"
    df = read.csv(path)

    df1 = df[ , grep("width_|si_", names(df))] 

    names(df1) = gsub("__", "_", names(df1))


    cor_var = abs(cor(df1, use = "pairwise.complete.obs", method = "spearman"))

    cor_var_ss = round(cor_var[grep("width_", rownames(cor_var)), grep("si_", colnames(cor_var))], 2)

    cor = as.matrix(round(cor_var_ss , 2))
    upper_tri = get_upper_tri(cor)

    # Melt the correlation matrix
    melted_cormat <- melt(upper_tri, na.rm = TRUE)

    ggplot(melted_cormat, aes(Var2, Var1, fill = value))+
     geom_tile(color = "white")+
     scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
       midpoint = 0.5, limit = c(0,1), space = "Lab", 
        name="Spearman Correlation") +
      theme_minimal()+ # minimal theme
     theme(axis.text.x = element_text(angle = 45, vjust = 1, 
        size = 12, hjust = 1))+
     coord_fixed() + 
      geom_text(aes(Var2, Var1, label = value), color = "black", size = 4) +
      theme(
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        panel.grid.major = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank(),
        axis.ticks = element_blank(),
        legend.justification = c(1, 0),
        legend.position = c(0.6, 0.7),
        legend.direction = "horizontal")+
        guides(fill = guide_colorbar(barwidth = 7, barheight = 1,
                      title.position = "top", title.hjust = 0.5))

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-16-1.svg)

    #ggsave(file.path(outdir, "S8a_tissue_correlations_shape_si.pdf"), width = 16, height = 16)

Supplementary figure 8
======================

Figure S8a
----------

    res = read.csv(file.path(human_path1, "Random_forest_results_lung_batches.csv"))
    df_sum = res %>% 
      group_by(type, response) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) 

    df_sum %<>% mutate(set = paste0(response, " (", type, ")"))

    df_sum$type = factor(df_sum$type, levels = rev(df_sum$type))


    ggplot(df_sum, aes(x = type, y = mean_rsq)) + 
      geom_bar(stat = "identity", position=position_dodge(.9)) +  
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.7, size = 0.2) +
      theme_bw() + 
      scale_alpha_manual(values = c(0.5, 1), guide = FALSE) + 
      xlab("")  + 
      ylab("R^2, 5-fold cross validation") +
      scale_y_continuous(limits = c(-0.05, 0.60), breaks = seq(0.1, 0.5, by = 0.1)) +
      coord_flip() + 
     # scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
      theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=18), 
            axis.title.x = element_text(size=22), axis.title.y = element_text(size=22),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-17-1.svg)

    ggsave(filename = file.path(outdir, "figS8a_rf_performance_batches.pdf"), width = 7, height = 5)

Figure S8b
----------

    res = read.csv(file.path(human_path1, "TF_correlations_change_promoter_radius.csv"))
    res_top = res[1:10, ]
    res_top$var = factor(res_top$var, levels = res_top$var)

    res_top %<>% gather(cor, val, -var)
    res_top$var = factor(res_top$var, levels = unique(res_top$var))
    res_top$cor = factor(res_top$cor, levels = c("cor_300", "cor_500", "cor_2000"))

    ggplot(res_top, aes(x = var, y = abs(val), color = cor)) + 
      geom_point(size = 10, alpha = 0.5) +
      theme_bw() +
      ylab("Feature correlation\nwith mean expression variation") + 
      xlab("") +
      ylim(c(0, 0.7)) +
      # scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
      scale_color_manual(values = cbPalette, name = "Radius around TSS", labels = c("-300/+200", "-500/+500", "-2000/+2000")) +
      # scale_shape_manual(values = c(21, 22, 24, 25), name = "Dataset") +
      guides(color = guide_legend(override.aes = list(size=8), order = 2),
             shape = guide_legend(override.aes = list(size=8, fill = "black"), order = 1)) +
      theme(axis.text.x = element_text(size=18, angle = 90), axis.text.y = element_text(size=18, hjust=0.95,vjust=0.2), 
            axis.title.y = element_text(size=18), axis.title.x = element_text(size=18),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.justification = "top")

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-18-1.svg)

    ggsave(filename = file.path(outdir, "figS8b_TF_correlations_change_promoter_radius.eps"), device = cairo_ps, width = 12, height = 7) # eps format is parsed better in illustrator

Figure S8c
----------

    # tmp2 is generated above (Fig 6f), important not to overwrite it

    ggplot(tmp2, aes(x = DE_Prior_Rank, color = group_type, fill = group_type)) + 
      geom_density(alpha = 0.5) +
      scale_fill_manual(values = pal1) +
      scale_color_manual(values = pal2) +
      theme_bw() +
      xlab("Mean variation") +
      ylab("Density") +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=14), axis.title.y = element_text(size=14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.title = element_text(size=12), legend.text = element_text(size=12))

![](Figures_Human_files/figure-markdown_strict/unnamed-chunk-19-1.svg)

    ggsave(filename = file.path(outdir, "figS8c_prior_gene_groups.pdf"), width = 8, height = 4)

Session Info
============

    sessionInfo()

    ## R version 3.5.1 (2018-07-02)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: CentOS Linux 7 (Core)
    ## 
    ## Matrix products: default
    ## BLAS/LAPACK: /g/easybuild/x86_64/CentOS/7/haswell/software/OpenBLAS/0.2.20-GCC-6.4.0-2.28/lib/libopenblas_haswellp-r0.2.20.so
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] stats     graphics  grDevices utils     datasets  methods   base     
    ## 
    ## other attached packages:
    ##  [1] bindrcpp_0.2.2     yaml_2.1.19        psych_1.8.4       
    ##  [4] rcompanion_2.2.2   pheatmap_1.0.10    ggExtra_0.9       
    ##  [7] gridExtra_2.3      RColorBrewer_1.1-2 ggrepel_0.8.0     
    ## [10] ggpubr_0.1.7       data.table_1.11.4  magrittr_1.5      
    ## [13] forcats_0.3.0      stringr_1.3.1      dplyr_0.7.5       
    ## [16] purrr_0.2.5        readr_1.1.1        tidyr_0.8.1       
    ## [19] tibble_1.4.2       ggplot2_3.0.0      tidyverse_1.2.1   
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] nlme_3.1-137           bitops_1.0-6           lubridate_1.7.4       
    ##  [4] httr_1.3.1             GenomeInfoDb_1.18.2    rprojroot_1.3-2       
    ##  [7] tools_3.5.1            backports_1.1.2        utf8_1.1.4            
    ## [10] R6_2.2.2               BiocGenerics_0.28.0    nortest_1.0-4         
    ## [13] lazyeval_0.2.1         colorspace_1.3-2       withr_2.1.2           
    ## [16] tidyselect_0.2.4       mnormt_1.5-5           compiler_3.5.1        
    ## [19] cli_1.0.0              rvest_0.3.2            expm_0.999-2          
    ## [22] xml2_1.2.0             sandwich_2.4-0         labeling_0.3          
    ## [25] scales_0.5.0           lmtest_0.9-36          mvtnorm_1.0-8         
    ## [28] multcompView_0.1-7     digest_0.6.15          foreign_0.8-70        
    ## [31] rmarkdown_1.10         XVector_0.22.0         pkgconfig_2.0.1       
    ## [34] htmltools_0.3.6        manipulate_1.0.1       rlang_0.2.1           
    ## [37] readxl_1.1.0           rstudioapi_0.7         shiny_1.1.0           
    ## [40] bindr_0.1.1            zoo_1.8-2              jsonlite_1.5          
    ## [43] RCurl_1.95-4.10        GenomeInfoDbData_1.2.0 modeltools_0.2-21     
    ## [46] Matrix_1.2-14          S4Vectors_0.20.1       Rcpp_0.12.17          
    ## [49] DescTools_0.99.28      munsell_0.5.0          stringi_1.2.3         
    ## [52] multcomp_1.4-8         zlibbioc_1.28.0        MASS_7.3-50           
    ## [55] plyr_1.8.4             grid_3.5.1             parallel_3.5.1        
    ## [58] promises_1.0.1         crayon_1.3.4           miniUI_0.1.1.1        
    ## [61] lattice_0.20-35        haven_1.1.1            splines_3.5.1         
    ## [64] hms_0.4.2              knitr_1.20             pillar_1.2.3          
    ## [67] EMT_1.1                GenomicRanges_1.34.0   boot_1.3-20           
    ## [70] ggsignif_0.4.0         reshape2_1.4.3         codetools_0.2-15      
    ## [73] stats4_3.5.1           glue_1.2.0             evaluate_0.10.1       
    ## [76] modelr_0.1.2           httpuv_1.4.4.1         cellranger_1.1.0      
    ## [79] gtable_0.2.0           assertthat_0.2.1       mime_0.5              
    ## [82] coin_1.2-2             xtable_1.8-2           broom_0.4.4           
    ## [85] later_0.7.3            survival_2.42-3        IRanges_2.16.0        
    ## [88] TH.data_1.0-8
