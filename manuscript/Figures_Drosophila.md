-   [Figure 1. Genomic features can predict expression variation
    independent of expression
    levels.](#figure-1.-genomic-features-can-predict-expression-variation-independent-of-expression-levels.)
    -   [Figure 1a](#figure-1a)
    -   [Figure 1b](#figure-1b)
    -   [Figure 1с](#figure-1)
    -   [Figure 1d](#figure-1d)
    -   [Figure 1e](#figure-1e)
-   [Supplementary Figure 1.](#supplementary-figure-1.)
    -   [Figures S1a-c](#figures-s1a-c)
    -   [Figure S1d](#figure-s1d)
    -   [Figure S1e](#figure-s1e)
    -   [Figure S1f](#figure-s1f)
-   [Figure 2. Promoter architecture is the most important predictor of
    expression
    variation](#figure-2.-promoter-architecture-is-the-most-important-predictor-of-expression-variation)
    -   [Figure 2a](#figure-2a)
    -   [Figure 2b-e](#figure-2b-e)
    -   [Figure 2f](#figure-2f)
-   [Supplementary Figure 2](#supplementary-figure-2)
    -   [Figure S2a](#figure-s2a)
    -   [Figure S2b](#figure-s2b)
    -   [Figure S2c](#figure-s2c)
    -   [Figure S2d](#figure-s2d)
    -   [Figure S2e](#figure-s2e)
    -   [Figure S2f](#figure-s2f)
-   [Figure 3. Expression variation in broad versus narrow promoter
    genes reflects trade-offs between expression robustness and
    regulatory
    plasticity.](#figure-3.-expression-variation-in-broad-versus-narrow-promoter-genes-reflects-trade-offs-between-expression-robustness-and-regulatory-plasticity.)
    -   [Figure 3a](#figure-3a)
    -   [Figure 3b](#figure-3b)
    -   [Figure 3c](#figure-3c)
-   [Supplementary Figure 3.](#supplementary-figure-3.)
    -   [Figure S3a](#figure-s3a)
    -   [Figure S3b.](#figure-s3b.)
    -   [Figure S3c.](#figure-s3c.)
-   [Figure 4. Different regulatory mechanisms lead to expression
    robustness in genes with broad and narrow
    promoters.](#figure-4.-different-regulatory-mechanisms-lead-to-expression-robustness-in-genes-with-broad-and-narrow-promoters.)
    -   [Figure 4a-b](#figure-4a-b)
    -   [Figure 4c](#figure-4c)
    -   [Figure4d-f](#figure4d-f)
    -   [Figure 4g](#figure-4g)
-   [Supplementary Figure 4.](#supplementary-figure-4.)
    -   [Figure S4a-f](#figure-s4a-f)
    -   [Figure S4g](#figure-s4g)
    -   [Figure S4h](#figure-s4h)
-   [Figure 5. Expression variation can predict signatures of
    differential expression upon various
    conditions.](#figure-5.-expression-variation-can-predict-signatures-of-differential-expression-upon-various-conditions.)
    -   [Figure 5a](#figure-5a)
    -   [Figire 5b-c](#figire-5b-c)
    -   [Figure 5d](#figure-5d)
    -   [Figure 5f](#figure-5f)
    -   [Figure 5g-h](#figure-5g-h)
    -   [Figure 5i](#figure-5i)
-   [Supplementary Figgure 5](#supplementary-figgure-5)
    -   [Figure S5a](#figure-s5a)
    -   [Figure S5b](#figure-s5b)
    -   [Figure S5c-f](#figure-s5c-f)
    -   [Figure S5g-j](#figure-s5g-j)
-   [Session Info](#session-info)

This is Markdown document that generates all *Drosophila* figures
(Figures 1-5 and Supplementary Figures 1-5) from the Manuscript.

    suppressPackageStartupMessages(library(tidyverse))
    suppressPackageStartupMessages(library(magrittr))
    suppressPackageStartupMessages(library(data.table))
    suppressPackageStartupMessages(library(ggpubr))
    #suppressPackageStartupMessages(library(ggrepel))
    suppressPackageStartupMessages(library(RColorBrewer))
    suppressPackageStartupMessages(library(gridExtra))
    suppressPackageStartupMessages(library(ggExtra))
    suppressPackageStartupMessages(library(rcompanion))
    suppressPackageStartupMessages(library(psych))
    suppressPackageStartupMessages(library(pheatmap))
    options(stringsAsFactors = FALSE)

    # here all the functions are stored 
    source("/g/furlong/project/62_expression_variation/src/shared/utils.R")
    # json file with all paths to data
    config = load_config() 
    # set directories
    project_folder = config$project_parameters$project_dir
    outdir = file.path(project_folder, "/manuscript/Figures")
    # create directory if not exists
    make_recursive_dir(outdir)

    # loading and processing master table 
    # full table - three time-points and additional variables not included in the final analysis
    master_table = load_master_table()
    # processed master table (4074 genes at 10-12h). set remove_list to NA to keep additional variables (see utils.R)
    mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = FALSE, remove_names = FALSE, remove_list = NA)
    mt = add_complexity_indixes(mt)

Figure 1. Genomic features can predict expression variation independent of expression levels.
=============================================================================================

Figure 1a
---------

Differences of gene regulatory mechanisms related to noise amplification
and noise buffering would result in different observed expression
variation given the same variation sources (left).

![](schematics/fig1a_model.jpg)

Figure 1b
---------

Dependence between coefficient of variation (CV) and median expression
level of 4074 genes across 75 samples (left). Residuals from LOESS
regression of CV on the median were used as the measure of variation
throughout the analysis (right). Median expression level and coefficient
of variation plotted on log2-scale, red line represents LOESS regression
fit.

    # s
    df = master_table %>% filter(time == "10_12h" & gene_id %in% mt$gene_id)
    df = mt

    # plot dependence with median
    p1 = ggplot(df, aes(x = median, y = cv)) + 
      geom_point(size = 0.2, color = "steelblue") +
      geom_smooth(method = "loess", color = "darkred") + 
      theme_bw() +
      scale_x_continuous(trans='log2', breaks = c(10, 100, 1000)) +
      scale_y_continuous(trans='log2') +
      xlab("Median expression level") +
      ylab("Coefficient of variation") +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=16, hjust=0.5))



    p2 = ggplot(df, aes(x = median, y = resid_cv)) + 
      geom_point(size = 0.2, color = "steelblue") +
      geom_smooth(method = "loess", color = "darkred") + 
      scale_x_continuous(trans='log2', breaks = c(10, 100, 1000)) +
      theme_bw() +
      xlab("Median expresson level") +
      ylab("Expression variation\n(residual CV)") +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=16, hjust=0.5))


    p = grid.arrange(p1, p2, ncol = 2)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-3-1.svg)

    ggsave(filename = file.path(outdir, "fig1b_final_variation_measure.pdf"), p, width = 9, height = 4)

Figure 1с
---------

Correlation of expression variation calculated from subsets of samples
versus the full data set. Error bars = standard deviation across 100
independent selections of samples.

    dir = file.path(project_folder, "/analysis/robustness_tests_and_validations")

    path = file.path(dir, "/subset_samples.correlations.csv")
    cor_df = read.csv(path)

    df_sum = cor_df %>% 
      group_by(num_samples) %>% 
      summarize(mean_cor = mean(cor), sd_cor = sd(cor)) 


    ggplot(df_sum, aes(x = num_samples, y = mean_cor, group = 1)) + 
      geom_point(size = 0.5) + 
      geom_line(size = 0.4) + 
      theme_bw() +
      scale_y_continuous(breaks = seq(0.6, 1, by = 0.1), limits = c(0.6, 1.01), name = "Correlation of variation \nwith full dataset") + 
      scale_x_continuous(breaks = c(5, seq(10, 70, by = 10), 74), name = "Number of samples") + 
     # xlab("number of samples") +
      geom_errorbar(aes(ymin = mean_cor - sd_cor, ymax = mean_cor + sd_cor), position=position_dodge(.9), width = 1, size = 0.4) +
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=15), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-4-1.svg)

    ggsave(filename = file.path(outdir, "fig1c_subsetting_samples.pdf"), width = 8, height = 3)

Figure 1d
---------

Schematic overview of the random forest models and feature selection
with Boruta algorithm (left). Performance shown as R^2 from 5-fold
cross-validation and compared to randomly permuted data (right).
Whiskers = standard deviation across the 5-fold cross validation.

![](schematics/fig1d_random_forest.jpg)

    dir = file.path(project_folder, "/analysis/random_forests")

    # R^2 for expression variation on full dataset
    rf_res_path =  file.path(dir, "rf_performance.full_and_selected_features.csv")  
    res = read.csv(rf_res_path)
    res_var = res %>% filter(dataset == "selected features")

    df_sum = res_var %>% 
      group_by(set, response, dataset) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
      ungroup() 
    df_sum$set = factor(df_sum$set, 
                        levels = c("permuted variation", "permuted level", "expression variation", "expression level"))

    # Performance on variation - for plot below
    full_perf = df_sum %>% filter(set == "expression variation" & dataset == "selected features") %>% select(mean_rsq) %>% unlist()


    ggplot(df_sum, aes(x = set, y = mean_rsq, fill = response)) + 
      geom_bar(stat = "identity", position=position_dodge(.9)) +  
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.7, size = 0.2) +
      theme_bw() + 
      scale_alpha_manual(values = c(0.5, 1), guide = FALSE) + 
      xlab("")  + 
      ylab("R^2, 5-fold cross validation") +
      scale_y_continuous(limits = c(-0.05, 0.55), breaks = seq(0.1, 0.5, by = 0.1)) +
      coord_flip() + 
      scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
      theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=18), 
            axis.title.x = element_text(size=22), axis.title.y = element_text(size=22),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-5-1.svg)

    ggsave(filename = file.path(outdir, "fig1d_rf_performance.pdf"), width = 6.5, height = 4.5)

for Synopsis Figure

    df_sum %<>% filter(set %in% c("expression variation", "expression level")) 
    df_sum$set = factor(df_sum$set, levels = c("expression variation", "expression level"))

    ggplot(df_sum, aes(x = set, y = mean_rsq, fill = response)) + 
      geom_bar(stat = "identity", position=position_dodge(.9)) +  
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.7, size = 0.2) +
      theme_bw() + 
      scale_alpha_manual(values = c(0.5, 1), guide = FALSE) + 
      xlab("")  + 
      ylab("R^2, 5-fold cross validation") +
      scale_y_continuous(limits = c(-0.05, 0.55), breaks = seq(0.1, 0.5, by = 0.1)) +
      coord_flip() + 
      scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) +
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=18), 
            axis.title.x = element_text(size=20), axis.title.y = element_text(size=20))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-6-1.svg)

    ggsave(filename = file.path(outdir, "synopsis_rf_performance.pdf"), width = 4, height = 2)

Figure 1e
---------

Performance (R^2, 5-fold cross validation) for genes grouped by
expression levels (quantiles). Whiskers represent standard deviation
from 5-fold cross validation, number of genes per quantile indicated
(x-axis). Red dotted line indicates performance of full model.

    dir = file.path(project_folder, "/analysis/random_forests")
    path =  file.path(dir, "rf_performance.selected_features.expression_level_bins.csv")  
    df = read.csv(path)


    df_sum = df %>% 
      group_by(bin_median, label_bin_median) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes)) %>% 
      ungroup() %>% 
      arrange(bin_median) %>%
      mutate(label_bin_median = factor(label_bin_median, levels = label_bin_median))

    ggplot(df_sum, aes(x = label_bin_median, y = mean_rsq, group = 1)) + 
      geom_hline(yintercept = full_perf, color = "darkred", linetype = "dashed") +
      geom_point(size = 0.5) + 
      geom_line(size = 0.4) + 
      theme_bw() +
      geom_text(aes(x = label_bin_median, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred") +
      ylim(c(0.2, 0.68)) + 
      xlab("Quantiles by gene expression level") +
      ylab("R^2") +
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.1, size = 0.2) +
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=15), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-7-1.svg)

    ggsave(filename = file.path(outdir, "fig1e_expr_level_bins.pdf"), width = 8, height = 3)

Supplementary Figure 1.
=======================

Figures S1a-c
-------------

Since generating these figures requires loading raw data we don’t
provide the code here. The corresponding scripts are provided in the scr
folder:
src/Drosophila/3.make\_feature\_tables/3.process\_expression\_and\_get\_variation.Rmd

Figure S1d
----------

    master_table$time = factor(master_table$time, levels = times)

    tmp = master_table %>% filter(gene_id %in% mt$gene_id) %>%
      select(gene_id, time, resid_cv_decrExprFilt_filtNA) %>%
      spread(time, resid_cv_decrExprFilt_filtNA) 


    {pdf(file.path(outdir,"figS1d_time_cor.pdf"), width = 6, height = 6)
    pairs.panels(tmp[, 2:4], 
                 method = "pearson", # correlation method
                 hist.col = "#00AFBB",
                 density = TRUE,  # show density plots
                 #ellipses = TRUE # show correlation ellipses
                 )

    dev.off()}

    ## pdf 
    ##   2

    # repeating same code to print to screen

    pairs.panels(tmp[, 2:4], 
                 method = "pearson", # correlation method
                 hist.col = "#00AFBB",
                 density = TRUE,  # show density plots
                 )

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-8-1.svg)

Figure S1e
----------

    dir = file.path(project_folder, "/analysis/genomic_contacts_and_neghbourhoods")
    contacts_info = read.csv(file.path(dir, "Genomic_contacts_pairwise.csv"), stringsAsFactors = FALSE)


    contacts_info %<>% mutate(bin_contacts = cut(norm * 100, round(quantile(norm * 100, 0:5/5), 2), include.lowest = TRUE),
                              bin_dist = cut(dist/1000, round(quantile(dist/1000, 0:5/5)), include.lowest = TRUE),
                              same_tad = tad_id.ramirez1 == tad_id.ramirez2)
                                                
      
    contacts_info$bin_contacts = factor(contacts_info$bin_contacts, levels = rev(levels(contacts_info$bin_contacts)))
    #contacts_info$bin_dist = factor(contacts_info$bin_dist, levels = 1:5)

    res1 = contacts_info %>% group_by(bin_contacts) %>% summarize(n = n(), cor_var = cor(resid_cv1, resid_cv2, method = "spearman"), cor_med = cor(median1, median2, method = "spearman")) %>%
      gather(group, cor, cor_var:cor_med) 

    res2 = contacts_info %>% group_by(bin_dist, same_tad) %>% 
      summarize(n = n(), cor_var = cor(resid_cv1, resid_cv2, method = "spearman"), cor_med = cor(median1, median2, method = "spearman")) %>%
        gather(responce, correl, cor_var:cor_med)%>%
        mutate(responce = factor(responce, levels = c("cor_var", "cor_med"))) %>%
      ungroup() %>% as.data.frame()


    ggplot(res2 %>% filter(n > 100), aes(x = bin_dist, y = correl, group = interaction(same_tad, responce), color = same_tad, linetype = responce)) + 
      geom_point(size = 2) + 
      geom_line() +
      theme_bw() +
      scale_linetype_discrete(name = "", labels = c("variation", "level")) +
      geom_text(data = res2 %>% filter(n > 100, responce == "cor_var"), aes(x = bin_dist, y = correl + 0.03, label = n), color = "darkred") +
      geom_hline(yintercept = 0, color = "darkred", linetype = "dashed") +
      scale_y_continuous(limits = c(-0.1, 0.45), breaks = round(seq(-0.1, 0.4, by = 0.1), 2)) +
      xlab("Quantiles by distance (close to far) ")  + 
      ylab("Correlation in variation\nbetween gene pairs") +
      scale_color_manual(values = cbPalette, name = "same TAD", labels = c("False", "True")) + 
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
            axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            legend.position = c(1, 1), legend.justification = c(1.1, 1.1),
            plot.margin = unit(c(0.25, 0.25, 1, 0.25), "cm"))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-9-1.svg)

    ggsave(filename = file.path(outdir, "figS1e_Correlations_for_neighbouring_genes.pdf"), width = 8, height = 5)

Figure S1f
----------

    path = config$data$Genetics$variance_decomposition

    variance_decompositions = read.delim(path) %>% 
      rename(Unexplained = Noise, gene_id = Gene_name) %>%
      mutate(Cis = Cis + CisGxE) %>% 
      select(-CisGxE) %>%
      filter(gene_id %in% mt$gene_id) %>%
      gather(comp, share, -gene_id) %>%
      mutate(comp = factor(comp, levels = rev(c("Cis", "Population_structure", "Environment", "Unexplained"))))


    ggplot(variance_decompositions, aes(y = share, x = comp, fill = comp)) + 
      geom_violin(alpha = 0.2) +
      geom_boxplot(width = 0.5) + 
      theme_bw() + 
      xlab("") + 
      rotate() +
    #  ylab("Number of different TF peaks at TSS\n(only genes with TSS-proximal DHSs)") + 
      ylab("Share of component in total variance \n(all time-points) ") + 
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=16), 
            axis.title.x = element_text(size=16), axis.title.y = element_text(size=18),
            legend.position = "none") 

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-10-1.svg)

    ggsave(filename = file.path(outdir, "figS1f_variance_decomposition.pdf"), width = 8, height = 4)

Figure 2. Promoter architecture is the most important predictor of expression variation
=======================================================================================

Figure 2a
---------

Top-30 important features for predicting expression variation using
Boruta feature selection. Features are ordered by their importance for
expression variation (blue) and show the corresponding importance for
level (orange). The absolute value and sign of correlation coefficient
is indicated by the triangle size and orientation, respectively. For
binary features, phi coefficient of correlation was used, otherwise
Spearman coefficient of correlation. Label colors correspond to feature
groups in (f).

    dir = file.path(project_folder, "/analysis/random_forests")
    path = file.path(dir, "significant_features_importance_and_correlations.csv")

    df = read.csv(path)
    df %<>% filter(!is.na(med_imp_var)) 
    df[is.na(df)] = 0.1 # pseudocount for non-significant features

    # remove similar features
    rl = c("promoter shape","insulation score (6-8h)", "TAD size(2-4h)", "gene conservation score","DHS time profile (prox)",
           "DHS tissue profile (prox)", "ubiquitous DHS (prox)", "number of TF peaks with motifs (prox)", "3'UTR LFC 10-12h vs. 2-4h")

    df %<>% filter(!full_name %in% rl & df$var != "modERN.Trl.E8_16.prox")

    # Top-50 features
    df %<>% arrange(-med_imp_var) 
    df_top = df[1:30, ] 

    df_top$full_name = gsub("number of|num.", "#", df_top$full_name)
    df_top$full_name = gsub("dme\\.", "", df_top$full_name)

    df_top$full_name = factor(df_top$full_name, levels = rev(df_top$full_name))

    df_top %<>% 
      gather(responce, med_imp, med_imp_var:med_imp_med) %>%
      mutate(cor = ifelse(responce == "med_imp_var", cor_var, cor_med)) 


    # set colors
    label_col = c("#4682B4", "#BDB76B", "#006400", "#E31A1C", "#8B4513", "#CC79A7", "#999999")
    df$class = factor(df$class, levels = unique(df$class))
    names(label_col) = unique(df$class)

    label_col_vect = label_col[rev(df_top$class)]


    ggplot(df_top, aes(y = full_name, x = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
      geom_point() +
      theme_bw() +
      xlab("Median feature importance") + 
      ylab("") +
      scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
      scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
      scale_size_continuous(name = "abs. cor", range = c(2, 10)) +
      scale_shape_manual(values = c(25, 24), name = "cor sign", labels = c("negative", "positive")) +
      guides(color = guide_legend(override.aes = list(size=8), order = 1),
             shape = guide_legend(override.aes = list(size=4, fill = "black"), order = 2)) +
      theme(axis.text.x = element_text(size=25), axis.text.y = element_text(size=25, color = label_col_vect, hjust=0.95,vjust=0.2), 
            axis.title.y = element_text(size=25), axis.title.x = element_text(size=30),
            legend.text=element_text(size=30), legend.title=element_text(size=30),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.justification = "top")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-11-1.svg)

    ggsave(filename = file.path(outdir, "fig2a_rf_top_features.eps"), width = 12, height = 20) # eps format is parsed better in illustrator

Figure 2b-e
-----------

Relationship between expression level and expression variation shown as
2D kernel density contours (left) and boxplots (right) for housekeeping
genes (b, Cohen’s d=0.82), genes separated by promoter shape (c, Cohen’s
d=0.97), number of embryonic conditions with a DHS (d, Cohen’s d=1.06
for top vs. bottom group), and presence of TATA-box at TSS (e, Cohen’s
d=1.41). LOESS regression lines indicated for each gene group, P-values
from Wilcoxon test.

    fun_2d = function(p, col = cbPalette) {
      
      p +
      geom_smooth(method = "loess", se = FALSE) +
      scale_x_continuous(trans='log10', breaks = c(10, 100, 1000)) +
      theme_bw() + 
      ylim(c(-1.06, 2.2)) +  
      geom_density_2d(alpha = 0.7) +
      scale_color_manual(values = col, name = "") +
      xlab("Expression level") + 
      ylab("Expression variation") + 
      guides(colour = guide_legend(override.aes = list(size=8))) +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position = c(0, 1), legend.justification = c(-0.1, 0.8),
            legend.background = element_rect(fill = "transparent"),
            plot.margin = unit(c(0.75, 0.05, 0.75, 0.75), "cm"))
      
    }


    fun_boxplot = function(p, xlab = "Group", ylab = "", comp = NA, col = cbPalette) {
      
      p +
    #  geom_violin(alpha = 0.5, width = 0.7) +
      geom_boxplot(width = 0.5, outlier.size = 0.1) +
      stat_compare_means(comparisons = comp) + 
      theme_bw() + 
      ylim(c(-1.06, 2.2)) +  
      scale_fill_manual(values = col, guide = FALSE) +
      xlab(xlab) + 
      ylab(ylab) + 
    #  stat_summary(fun.data = function(x) c(y = median(x) + 0.1, label = length(x)), geom = "text", size = 3, fontface = "bold") +
      theme(axis.text.y = element_text(size=14), 
            axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position = c(0, 1), legend.justification = c(-0.2, 1.1),
            legend.background = element_rect(fill = "transparent"),
            plot.margin = unit(c(0.75, 0.75, 0.75, 0.05), "cm"))
      
    }

    p1 = fun_2d(ggplot(mt, aes(x = median, y = resid_cv, color = shape)))
    p2 = fun_boxplot(ggplot(mt, aes(y = resid_cv, fill = shape, x = shape)), ylab = "", comp = list(c("broad", "narrow")))

    pl1 = grid.arrange(arrangeGrob(p1,p2, ncol=2, nrow=1, widths = c(2, 0.85)), top = "Promoter shape")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-12-1.svg)

    mt$tata = ifelse(mt$ohler_maj.TATA, "with TATA-box", "no TATA-box")
    mt$tata = factor(mt$tata, levels = c("no TATA-box", "with TATA-box"))

    p1 = fun_2d(ggplot(mt, aes(x = median, y = resid_cv, color = tata)))
    p2 = fun_boxplot(ggplot(mt, aes(y = resid_cv, fill = tata, x = tata)), ylab = "", comp = list(c("no TATA-box", "with TATA-box")))

    pl2 = grid.arrange(arrangeGrob(p1,p2, ncol=2, nrow=1, widths = c(2, 0.85)), top = "with TATA-box")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-12-2.svg)

    mt$hk = ifelse(mt$is_housekeeping, "housekeeping", "non-housekeeping")
    mt$hk = factor(mt$hk, levels = c("housekeeping", "non-housekeeping"))

    p1 = fun_2d(ggplot(mt, aes(x = median, y = resid_cv, color = hk)))
    p2 = fun_boxplot(ggplot(mt, aes(y = resid_cv, fill = hk, x = hk)), ylab = "", comp = list(c("housekeeping", "non-housekeeping")))

    pl3 = grid.arrange(arrangeGrob(p1,p2, ncol=2, nrow=1, widths = c(2, 0.85)), top = "Housekeeping genes")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-12-3.svg)

    # mt$cons = ifelse(mt$conserv_rank == "high", "conserved", "non-conserved")
    # mt$cons = factor(mt$cons, levels = c("conserved", "non-conserved"))
    # p1 = fun_2d(ggplot(mt, aes(x = median, y = resid_cv, color = cons)))
    # p2 = fun_boxplot(ggplot(mt, aes(y = resid_cv, fill = cons, x = cons)), ylab = "", comp = list(c("conserved", "non-conserved")))

    q_dhs_cond = quantile(mt$num_dhs_conditions.prox, c(0, 1/3, 2/3, 1))
    dhs_cond_labels = c(paste("<", round(q_dhs_cond[2], 2)), paste0("[", round(q_dhs_cond[2], 2), ", ", round(q_dhs_cond[3], 2), ")"), paste(">=", round(q_dhs_cond[3], 2)))
    mt %<>% mutate(bin_dhs_cond = cut(num_dhs_conditions.prox, q_dhs_cond, include.lowest = TRUE, dig.lab=10, labels = dhs_cond_labels))
    mt$bin_dhs_cond = factor(mt$bin_dhs_cond, levels = rev(dhs_cond_labels))

    p1 = fun_2d(ggplot(mt, aes(x = median, y = resid_cv, color = bin_dhs_cond)), col = c(cbPalette[1], "grey", cbPalette[2]))
    p2 = fun_boxplot(ggplot(mt %>% filter(bin_dhs_cond != "[4, 18)"), aes(y = resid_cv, fill = bin_dhs_cond, x = bin_dhs_cond)), ylab = "", comp = list(c("< 4", ">= 18")))

    pl4 = grid.arrange(arrangeGrob(p1,p2, ncol=2, nrow=1, widths = c(2, 0.85)), top = "Number of conditions with DHS")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-12-4.svg)

    p = grid.arrange(pl1, pl2, pl3, pl4)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-13-1.svg)

    ggsave(filename = file.path(outdir, "fig2bcde_feature_examples.eps"), p, width = 12, height = 10, device=cairo_ps)

And effect sizes for boxplots (Cohen’s d)

-   binary features

<!-- -->

    features = c("is_housekeeping", "shape", "ohler_maj.TATA")
    vapply(features, function(x) round(abs(cohen.d(mt[ , c(x, "resid_cv")], x)$cohen.d[2]), 2), numeric(1))

    ## is_housekeeping           shape  ohler_maj.TATA 
    ##            0.82            0.97            1.41

-   num conditions with DHS

<!-- -->

    tmp = mt %>% filter(bin_dhs_cond %in% c(">= 18", "< 4"))
    round(abs(cohen.d(tmp[ , c("bin_dhs_cond", "resid_cv")], "bin_dhs_cond")$cohen.d[2]), 2)

    ## [1] 1.06

Figure 2f
---------

Performance of random forest predictions (mean R^2 from 5-fold
cross-validation) for expression level (orange) and variation (blue)
trained on individual feature groups. Whiskers = standard deviation,
color code of y-axis labels matches Fig 2a.

    dir = file.path(project_folder, "/analysis/random_forests")
    path = file.path(dir, "rf_performance_by_feature_groups.csv")
    df = read.csv(path)

    df_sum = df %>% group_by(set, responce) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>%
      arrange(mean_rsq)

    var_levels =  unlist(df_sum[df_sum$responce == "variation", "set"])

    df_sum$set = factor(df_sum$set, levels = var_levels)

    df_sum$responce = factor(df_sum$responce, levels = c("variation", "level"))

    label_col_vect = rev(c("#4682B4", "#BDB76B", "#006400", "#8B4513", "#CC79A7", "#E31A1C", "#999999"))

    ggplot(df_sum, aes(x = set, y = mean_rsq, fill = responce)) + 
      geom_bar(stat = "identity", position=position_dodge(.9)) +  
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9)) +
      theme_classic() + 
      xlab("") + ylab("R^2 (5-fold cross-validation)") +
      coord_flip() + 
      scale_fill_manual(values = c("navy", "sandybrown"),  name = "explained variable",
                        guide=guide_legend(reverse=T)) + 
      theme(axis.text.y = element_text(size=25, color = label_col_vect), axis.text.x = element_text(size=20), 
            axis.title.x = element_text(size=25), axis.title.y = element_text(size=18),
            legend.text=element_text(size=25), legend.title=element_text(size=25),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.05, -0.25))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-16-1.svg)

    ggsave(filename = file.path(outdir, "fig2e_rf_feature_subsets.pdf"), width = 11, height = 8)

Supplementary Figure 2
======================

Figure S2a
----------

    # values

    path1 = file.path(project_folder, "/analysis/robustness_tests_and_validations/alternative_residual_variation_measures.csv")
    path2 = file.path(project_folder, "/analysis/robustness_tests_and_validations/VST_sd.csv")
    var1 = read.csv(path1) %>% mutate(sd = log(sd), iqr = log(iqr), mad = log(mad), cv = log(cv))
    var2 = read.csv(path2)

    var = merge(var1, var2, by = "gene_id")
    var = merge(var, mt %>% select(gene_id, resid_cv), by = "gene_id")
    var %<>% gather(metrics, value, -gene_id, -median, -median_vst)

    # model

    path1 = file.path(project_folder, "/analysis/robustness_tests_and_validations/model_performance.alternative_residual_variation_measures.csv")
    path2 = file.path(project_folder, "/analysis/robustness_tests_and_validations/model_performance.VST_comparison.csv")
    res1 = read.csv(path1)
    res2 = read.csv(path2)

    res = rbind.data.frame(res1, res2 %>% filter(metrics != "median_vst")) 


    var_alt = var %>% filter(!metrics %in% c("cv", "resid_cv", "mad", "iqr", "sd"))

    var_alt$metrics = factor(var_alt$metrics, levels = c("sd_vst", "resid_mad", "resid_iqr", "resid_sd"))

    ggplot(var_alt, aes(x = log(median), y = value)) + 
      geom_point(size = 0.2) + 
      geom_smooth(method = "loess") + 
      theme_bw() +
      xlab("Median gene expression (log-scaled)") + 
      ylab("Expression variation (different metrics)") +
      facet_wrap(~metrics, ncol = 4, scales = "free")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-17-1.svg)

    ggsave(file.path(outdir, "FigS2a_different_variation_metrics_median_dependence.pdf"), width = 10, height = 3)

Figure S2b
----------

    tmp = var %>% filter(metrics %in% c("resid_cv", "resid_sd", "resid_mad", "resid_iqr", "sd_vst")) %>% select(gene_id, metrics, value) %>% spread(metrics, value)

    # pheatmap(tmp_cor, cluster_rows = FALSE, cluster_cols = FALSE, display_numbers = TRUE, fontsize = 16, legend = FALSE,
    #          filename = file.path(outdir, "different_variation_metrics_corplot.pdf"), width = 4, height = 4,
    #          color = colorRampPalette(brewer.pal(n = 5, name = "YlOrRd"))(5))


    cor = as.matrix(round(cor(tmp[ ,2:6]), 2))
    upper_tri = get_upper_tri(cor)

    # Melt the correlation matrix
    melted_cormat <- melt(upper_tri, na.rm = TRUE)

    ggplot(melted_cormat, aes(Var2, Var1, fill = value))+
     geom_tile(color = "white")+
     scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
       midpoint = 0.5, limit = c(0,1), space = "Lab", 
        name="Pearson\nCorrelation") +
      theme_minimal()+ # minimal theme
     theme(axis.text.x = element_text(angle = 45, vjust = 1, 
        size = 12, hjust = 1))+
     coord_fixed() + 
      geom_text(aes(Var2, Var1, label = value), color = "black", size = 4) +
      theme(
        axis.title.x = element_blank(),
        axis.title.y = element_blank(),
        panel.grid.major = element_blank(),
        panel.border = element_blank(),
        panel.background = element_blank(),
        axis.ticks = element_blank(),
        legend.justification = c(1, 0),
        legend.position = c(0.6, 0.7),
        legend.direction = "horizontal")+
        guides(fill = guide_colorbar(barwidth = 7, barheight = 1,
                      title.position = "top", title.hjust = 0.5))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-18-1.svg)

    ggsave(file.path(outdir, "FigS2b_different_variation_metrics_corplot.pdf"), width = 4, height = 4)

Figure S2c
----------

    dir = file.path(project_folder, "/analysis/random_forests")

    # R^2 for expression variation on final dataset
    rf_res_path =  file.path(dir, "rf_performance.full_and_selected_features.csv")  
    res1 = read.csv(rf_res_path)
    df_sum = res1 %>% 
      filter(dataset == "selected features" & set == "expression variation") %>%
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
      select(mean_rsq, sd_rsq) %>% mutate(type = "w/o expression level")
     

    # R^2 for expression variation with expression level as one of the features
    path = file.path(dir, "rf_performance.selected_features_with_expression_level.csv")
    res2 = read.csv(path) %>% summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% mutate(type = "with expression level")

    tmp = rbind.data.frame(df_sum, res2)

    ggplot(tmp, aes(x = type, y = mean_rsq)) + 
      geom_bar(stat = "identity", position=position_dodge(.9), fill = "navy", width = 0.6) +  
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.5, size = 0.2) +
      theme_bw() + 
      xlab("")  + 
      ylab("R^2, 5-fold cross validation") +
      scale_y_continuous(limits = c(0, 0.60), breaks = seq(0, 0.5, by = 0.1)) +
     # coord_flip() + 
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=18, angle = 30, hjust = 1), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-19-1.svg)

    ggsave(filename = file.path(outdir, "figS2c_rf_performance_with_and_wo_level.pdf"), height = 6, width = 4)

Figure S2d
----------

    df_sum = res %>% 
      group_by(metrics) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
      ungroup() 


    ggplot(df_sum, aes(x = metrics, y = mean_rsq)) + 
      geom_bar(stat = "identity", position=position_dodge(.9), color = "black", fill = "cornflowerblue") +  
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9)) +
      theme_bw() + 
      xlab("")  + 
      ylab("R^2, 5-fold cross validation") +
      scale_y_continuous(limits = c(-0.05, 0.6), breaks = seq(0.1, 0.6, by = 0.1)) +
      coord_flip() + 
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=20), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = "none")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-20-1.svg)

    ggsave(filename = file.path(outdir, "figS2d_rf_performance_alternative_variation_measures.pdf"), width = 6, height = 3)

Figure S2e
----------

    dir = file.path(project_folder, "/analysis/random_forests")
    path =  file.path(dir, "rf_performance.selected_features.expression_change_bins.csv")  
    df = read.csv(path)

    df_sum = df %>% 
      group_by(bin_change, label_bin_change, med_var, sd_var) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes)) %>% 
      ungroup() %>% 
      arrange(bin_change) %>%
      mutate(label_bin_change = factor(label_bin_change, levels = label_bin_change))

    ggplot(df_sum, aes(x = label_bin_change, y = mean_rsq, group = 1)) + 
      geom_hline(yintercept = full_perf, color = "darkred", linetype = "dashed") +
      geom_point() + 
      geom_line(size = 0.4) + 
      theme_bw() +
      geom_text(aes(x = label_bin_change, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred") +
      ylim(c(0, 0.7)) + 
      xlab("Quartiles by absolute expression L2FC (10-12h vs. 6-8h)") +
      ylab("R^2 (5-fold CV)") +
      geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.3, size = 0.2) +
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16, angle = 45), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-21-1.svg)

    ggsave(filename = file.path(outdir, "figS2e_rf_performance.expr_change_bins.pdf"), width = 7, height = 5)

Figure S2f
----------

    dir = file.path(project_folder, "/analysis/robustness_tests_and_validations")
    path =  file.path(dir, "rf_perf_train_and_test_on_other_chr.csv")  
    df = read.csv(path)

    ggplot(df, aes(x = chr, y = rsq)) + geom_bar(stat = "identity", width = 0.5) + 
      coord_flip()+
      geom_hline(yintercept = median(df$rsq), color = "darkred") +
      theme_bw() +
      xlab("Predictions per chromosome (arm)") +
      ylab("Predicion R^2 from random forest")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-22-1.svg)

    ggsave(file.path(outdir, "figS2f_predict_on_other_chr.pdf"), width = 4, height = 3)

Figure 3. Expression variation in broad versus narrow promoter genes reflects trade-offs between expression robustness and regulatory plasticity.
=================================================================================================================================================

Figure 3a
---------

Genes separate into three groups based on their promoter shape index
(x-axis) and expression variation (y-axis). Each dot represents a gene;
colors indicate gene annotations: housekeeping (orange),
non-housekeeping TFs (blue), non-housekeeping with a TATA-box (red),
other (grey). Distributions of promoter shape index and expression
variation across gene groups are shown as density plots. Broad and
narrow promoter genes are separated based on shape index threshold of -1
(vertical black line) as in (Schor et al. 2017). Narrow-low and
narrow-high groups are separated based on the median expression
variation of narrow promoter genes (horizontal black line).

    mt$var_type = "other"
    mt$var_type[mt$ohler_maj.TATA == TRUE] = "TATA"
    mt$var_type[mt$is_tf == TRUE] = "TF"
    mt$var_type[mt$is_housekeeping == TRUE] = "housekeeping"
    mt$var_type = factor(mt$var_type, levels = c("housekeeping", "TF", "TATA", "other"))

    med_var_narrow = median(mt$resid_cv[mt$shape == "narrow"])

    p1 = ggplot(mt, aes(x = major_shape_ind, y = resid_cv, color = var_type)) + 
      geom_point(aes(size = var_type)) + 
      geom_vline(xintercept = -1, color = "darkblue", size = 0.5) +
      geom_segment(aes(x = -1 , y = med_var_narrow, xend = Inf, yend = med_var_narrow), color = "darkgrey", size = 0.5) +
      geom_segment(aes(x = -1 , y = -Inf, xend = -1, yend = Inf), color = "darkgrey", size = 0.5) +
      scale_size_manual(values = c(1, 1, 1.4, 1.4), guide = FALSE) +
      theme_bw() + 
      annotate("text", x = -4.3, y = -1, label = "broad", color = "darkred", size = 6) +
      annotate("text", x = 1.3, y = -1, label = "narrow-low", color = "darkred", size = 6) +
      annotate("text", x = 1.3, y = 2.2, label = "narrow-high", color = "darkred", size = 6) +
      scale_color_manual(values = c("peru", "midnightblue", "maroon", "lightgrey"), name = "gene type", 
                         labels = c("housekeeping", "TFs (non-hk)", "TATA (non-hk)", "other"))  +
      xlab("Promoter shape index") + 
      ylab("Expression variation") + 
      guides(colour = guide_legend(override.aes = list(size=8))) +
      theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=22), 
            axis.title.x = element_text(size=22), axis.title.y = element_text(size=22),
            legend.text=element_text(size=18), legend.title=element_text(size=22),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=22, hjust=0.5),
            legend.position = c(0, 1), legend.justification = c(-0.1, 1.05))

    p = ggMarginal(p1, groupColour = TRUE, groupFill = TRUE)
    p

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-23-1.svg)

    ggsave(filename = file.path(outdir, "fig3a_promoter_shape_vs_expression_variation.eps"), p, width = 9, height = 9, device=cairo_ps)

Figure 3b
---------

Performance to predict expression variation for genes split by quartiles
of promoter shape index. Horizontal lines show performance (mean R^2
from 5-fold cross-validation) on broad (orange) and narrow (blue)
promoter genes separately. Whiskers = standard deviation (from 5-fold
cross validation), number of genes per categories indicated (x-axis).

    dir = file.path(project_folder, "/analysis/random_forests")

    # overall performance
    path = file.path(dir, "rf_performance.broad_and_narrow.csv")
    df = read.csv(path)
    df_sum = df %>% group_by(response, shape) %>% summarize(rsq = median(rsq))
    rsq_broad = df_sum[df_sum$response == "variation" & df_sum$shape == "broad", "rsq"] %>% unlist()
    rsq_narrow = df_sum[df_sum$response == "variation" & df_sum$shape == "narrow", "rsq"] %>% unlist()

    # bins by shape
    path =  file.path(dir, "rf_performance.selected_features.promoter_shape_bins.csv")  
    df = read.csv(path)

    df_sum = df %>% 
      group_by(bin_shape, label_bin_shape, med_var, sd_var) %>% 
      summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes)) %>% 
      ungroup() %>% 
      arrange(bin_shape) %>%
      mutate(label_bin_shape = factor(label_bin_shape, levels = label_bin_shape))

    ggplot(df_sum, aes(x = label_bin_shape, y = mean_rsq, group = 1)) + 
      geom_hline(yintercept = rsq_broad, color = "sandybrown", linetype = "dashed", size = 1) + 
      geom_hline(yintercept = rsq_narrow, color = "darkblue", linetype = "dashed", size = 1) +
      geom_point() + 
      geom_line() + 
      theme_bw() +
      geom_text(aes(x = label_bin_shape, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred")+
      ylim(c(0, 0.6)) + 
      xlab("Quantiles by promoter shape index") +
      ylab("R^2 (5-fold cross-validation)") +
        geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.1, size = 0.2) +
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-24-1.svg)

    ggsave(filename = file.path(outdir, "fig3b.rf_performance_shape_bins.pdf"), width = 7, height = 5)

Figure 3c
---------

GO term enrichment (Biological Process) of genes stratified by promoter
shape and expression variation. Top GO terms ranked by p-value are shown
(full list in Dataset EV6). Quartiles of expression variation (1-
lowest, 4 – highest) were calculated for broad and narrow promoter genes
separately. Quantile intervals for broad and narrow promoter genes
provided in Methods.

    dir = file.path(project_folder, "/analysis/GO_enrichments")
    df = read.csv(file.path(dir, "GO_q4.shape_separate_quant.bp_pval01.final_set_genes_bg.csv"))


    # select 4 top matches per group
    df_subset = df %>% 
    #  filter(filter == "k") %>% # for manual removal of some cathegories
      group_by(shape, bin4_var) %>% 
      mutate(r = rank(pvalue)) %>%
      filter(r <= 8)

    # for selected categories - check matches in other groups
    df_subset = df[df$Description %in% df_subset$Description, ]
    df_subset %<>% arrange(shape, bin4_var, pvalue) 
    df_subset$Description = factor(df_subset$Description, levels = rev(unique(df_subset$Description)))

    pmin = min(df_subset$p.adjust)

    p = ggplot(df_subset, aes(x = factor(bin4_var), y = Description, size = GeneRatio, color = p.adjust)) +
      geom_point() +
      facet_wrap(~shape) +
      theme_bw() +
      ylab("") + 
      xlab("Quartiles by expression variation \n(separate for broad and narrow)") + 
      ylab("") + 
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
                     axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
                     legend.text=element_text(size=16), legend.title=element_text(size=16),
                     strip.text.x = element_text(size = 16), strip.text.y = element_text(size = 16))

    p

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-25-1.svg)

    ggsave(filename = file.path(outdir, "fig3c_GO_bp.final_set_genes_bg.eps"), p, width = 12, height = 14, device = cairo_ps)

    # quantile values

    mt %>% group_by(shape) %>%
      mutate(bin4_var = cut(resid_cv, quantile(resid_cv, 0:4/4), include.lowest = TRUE),
             bin4_lab = cut(resid_cv, quantile(resid_cv, 0:4/4), include.lowest = TRUE, labels = FALSE)) %>%
      select(shape, bin4_var, bin4_lab) %>% unique() %>% 
      arrange(shape, bin4_lab)

    ## Warning in mutate_impl(.data, dots): Unequal factor levels: coercing to
    ## character

    ## Warning in mutate_impl(.data, dots): binding character and factor vector,
    ## coercing into character vector

    ## Warning in mutate_impl(.data, dots): binding character and factor vector,
    ## coercing into character vector

    ## # A tibble: 8 x 3
    ## # Groups:   shape [2]
    ##   shape  bin4_var         bin4_lab
    ##   <fct>  <chr>               <int>
    ## 1 broad  [-1.06,-0.444]          1
    ## 2 broad  (-0.444,-0.266]         2
    ## 3 broad  (-0.266,-0.0754]        3
    ## 4 broad  (-0.0754,1.89]          4
    ## 5 narrow [-0.98,-0.173]          1
    ## 6 narrow (-0.173,0.0751]         2
    ## 7 narrow (0.0751,0.416]          3
    ## 8 narrow (0.416,1.99]            4

Supplementary Figure 3.
=======================

Figure S3a
----------

GO Functional Enrichment - Molecular function

    dir = file.path(project_folder, "/analysis/GO_enrichments")
    df = read.csv(file.path(dir, "GO_q4.shape_separate_quant.mf_pval01.final_set_genes_bg.csv"))


    # select 4 top matches per group
    df_subset = df %>% 
    #  filter(filter == "k") %>% # for manual removal of some cathegories
      group_by(shape, bin4_var) %>% 
      mutate(r = rank(pvalue)) %>%
      filter(r <= 8) %>%
      filter(Description != "oxidoreductase activity, acting on paired donors, with incorporation or reduction of molecular oxygen") # redundant with just oxidoreductase activity

    # for selected categories - check matches in other groups
    df_subset = df[df$Description %in% df_subset$Description, ]
    df_subset %<>% arrange(shape, bin4_var, pvalue) 
    df_subset$Description = factor(df_subset$Description, levels = rev(unique(df_subset$Description)))


    pmin = min(df_subset$p.adjust)

    p = ggplot(df_subset, aes(x = factor(bin4_var), y = Description, size = GeneRatio, color = p.adjust)) +
      geom_point() +
      facet_wrap(~shape) +
      theme_bw() +
      ylab("") + 
      xlab("Quartiles by expression variation \n(separate for broad and narrow)") + 
      ylab("") + 
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
                     axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
                     legend.text=element_text(size=16), legend.title=element_text(size=16),
                     strip.text.x = element_text(size = 16), strip.text.y = element_text(size = 16))

    p

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-27-1.svg)

    ggsave(filename = file.path(outdir, "figS3a_GO_mf.final_set_genes_bg.eps"), p, width = 12, height = 14, device = cairo_ps)

Figure S3b.
-----------

    # Important - for this plot DHSs are asigned to nearest genes (not fixed radius as in all other analysis)

    genes = load_genes()

    ## Warning: no function found corresponding to methods exports from
    ## 'GenomicAlignments' for: 'concatenateObjects'

    prom = promoters(genes, upstream = 500, downstream = 500)
    dhs_annot_path = config$processed_features$dhs_annotated
    dhs = read.delim(dhs_annot_path, sep = "\t")  
    dhs_sd = dhs %>% group_by(gene_name) %>% summarize(dhs_sd = median(sd))

    tmp = merge(mt, dhs_sd, by = "gene_name")

    tmp$var_type = "other narrow"
    tmp$var_type[tmp$ohler_maj.TATA == TRUE] = "narrow TATA"
    tmp$var_type[tmp$is_tf == TRUE] = "narrow TFs"
    tmp$var_type[tmp$shape == "broad"] = "broad"
    tmp$var_type = factor(tmp$var_type, levels = c("broad", "narrow TFs", "narrow TATA", "other narrow"))

    tmp_sum = tmp %>% group_by(var_type) %>% summarize(med_resid_cv = median(resid_cv),
                                                       mean_resid_cv = mean(resid_cv),
                                                       mean_dhs_sd = mean(dhs_sd),
                                                       q25_resid_cv = quantile(resid_cv, 0.25),
                                                       q75_resid_cv = quantile(resid_cv, 0.75),
                                                       sd_resid_cv = sd(resid_cv),
                                                       med_dhs_sd = median(dhs_sd),
                                                       q25_dhs_sd = quantile(dhs_sd, 0.25),
                                                       q75_dhs_sd = quantile(dhs_sd, 0.75),
                                                       sd_dhs_sd = sd(dhs_sd), n())


    p1 = ggplot(tmp, aes(y = dhs_sd, fill = shape, x = shape))+
      geom_violin(alpha = 0.3) +
      geom_boxplot() + 
      theme_bw() +
      stat_compare_means(method = "wilcox.test", comparisons = list(c("broad", "narrow")))  +
      xlab("Nearest TSS shape") +
      ylab("DHS standard deviation") +
      scale_fill_manual(values = cbPalette, name = "") +
      stat_summary(fun.data = function(x) c(y = -0.1, label = length(x)), geom = "text", size = 4) +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14, angle = 30), 
            axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
            legend.position="none",
            plot.margin = unit(c(0.75, 0.5, 0.5, 0.05), "cm"))


    p2 = ggplot(tmp_sum, aes(y = med_resid_cv, x = med_dhs_sd, color = var_type)) + 
      geom_point(aes(y = resid_cv, x = dhs_sd), data = tmp, size = 0.8, alpha = 0.3) + 
    #  geom_errorbar(aes(ymin = mean_resid_cv - sd_resid_cv, ymax = mean_resid_cv + sd_resid_cv), size = 0.7) + 
    #  geom_errorbarh(aes(xmin = mean_dhs_sd - sd_dhs_sd, xmax = mean_dhs_sd + sd_dhs_sd), size = 0.7) + 
      theme_bw() +
      scale_color_manual(values = c("peru", "midnightblue", "maroon", "darkgrey"), name = "gene type") + 
      ylab("Expression variation") + 
      xlab("DHS standard deviation (nearest to TSS)\n across time and tissues") + 
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=18, color = "black"), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            legend.background = element_rect(fill = "transparent"),
            legend.position = c(1, 1), legend.justification = c(1.1, 1.1))

    p2 = ggMarginal(p2, groupColour = TRUE, groupFill = TRUE, margins = "x", alpha = 0.5)


    p = grid.arrange(arrangeGrob(p1,p2, ncol=2, nrow=1, widths = c(0.85, 2)))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-28-1.svg)

    ggsave(filename = file.path(outdir, "figS3b_dhs_sd_scatter.pdf"), p, width = 8, height = 5)

Figure S3c.
-----------

    tmp = mt

    tmp$var_type = "other narrow"
    tmp$var_type[tmp$ohler_maj.TATA == TRUE] = "narrow TATA"
    tmp$var_type[tmp$is_tf == TRUE] = "narrow TFs"
    tmp$var_type[tmp$shape == "broad"] = "broad"
    tmp$var_type = factor(tmp$var_type, levels = c("broad", "narrow TFs", "narrow TATA", "other narrow"))

    tmp$conserv = ifelse(tmp$conserv_rank %in% c("high"), TRUE, FALSE)
    tmp$conserv = factor(tmp$conserv, levels = c(TRUE, FALSE))


    col = brewer.pal(name = "Paired", n = 4)

    ggplot(tmp, aes(x = conserv, y = resid_cv, fill = conserv)) + 
      geom_boxplot() + 
      stat_compare_means(comparisons = list(c("TRUE", "FALSE"))) + 
      scale_fill_manual(values = col[2:1], name = "", labels = c("conserved", "not conserved")) +
      facet_wrap(~var_type, ncol = 4) +
      theme_bw() + 
      xlab("Protein sequence conservation between fly and human") + 
      ylab("Expression variation") + 
      stat_summary(fun.data = function(x) c(y = median(x) + 0.1, label = length(x)), geom = "text", size = 5, color = "white", fontface = "bold") +
      theme(axis.text.x = element_blank(), axis.text.y = element_text(size=16), 
            axis.title.x = element_text(size=20), axis.title.y = element_text(size=20),
            legend.text=element_text(size=20), legend.title=element_text(size=18),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position="bottom")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-29-1.svg)

    ggsave(filename = file.path(outdir, "figS3c_conserved_genes_var.pdf"), width = 8, height = 7)

and Cohen’s d

    get_cohen_d_two_groups(tmp %>% filter(var_type == "broad"), group_var = "conserv", comparisons = c("TRUE", "FALSE"))

    ## [1] 0.3940551

    get_cohen_d_two_groups(tmp %>% filter(var_type == "narrow TFs"), group_var = "conserv", comparisons = c("TRUE", "FALSE"))

    ## [1] 0.1024596

    get_cohen_d_two_groups(tmp %>% filter(var_type == "narrow TATA"), group_var = "conserv", comparisons = c("TRUE", "FALSE"))

    ## [1] 0.9154757

    get_cohen_d_two_groups(tmp %>% filter(var_type == "other narrow"), group_var = "conserv", comparisons = c("TRUE", "FALSE"))

    ## [1] 0.4678157

Figure 4. Different regulatory mechanisms lead to expression robustness in genes with broad and narrow promoters.
=================================================================================================================

Figure 4a-b
-----------

Chromatin accessibility (number of conditions with DHS) (a, Cohen’s
d=0.95 for broad vs. narrow-low, d=0.79 for narrow-low vs. narrow-high),
or number of different TF peaks (b, Cohen’s d=0.29 for broad
vs. narrow-low, d=0.34 for narrow-low vs. narrow-high) overlapping
TSS-proximal DHS for genes stratified into broad, narrow-low and
narrow-high (defined in Fig 3a). P-values from Wilcoxon test. In (b)
only genes with TSS-proximal DHS peak are considered.

    med_var_narrow = median(mt[mt$shape == "narrow", "resid_cv"])

    mt$var_type = "broad"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv > med_var_narrow] = "narrow-high"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv < med_var_narrow] = "narrow-low"
    mt$var_type = factor(mt$var_type, levels = c("broad", "narrow-low", "narrow-high"))

    comp = list(c("broad", "narrow-low"), c("narrow-low", "narrow-high"))
    col_groups = c("peru", "lightblue", "maroon")

    ggplot(mt, aes(y = num_dhs_conditions.prox, x = var_type, fill = var_type)) + 
      geom_violin(alpha = 0.2) +
      geom_boxplot(width = 0.3, outlier.size = 0.1) + 
      theme_bw() + 
      scale_fill_manual(values = col_groups, guide = FALSE) +
      xlab("") + 
      rotate() +
      ylab("# conditions with DHS at TSS") + 
      stat_compare_means(comparisons = comp,
                         label.y = c(21, 23)) +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=14), axis.title.y = element_text(size=14)) +
      stat_summary(fun.data = function(x) c(y = -0.4, angle = -90, label = length(x)), geom = "text", size = 4) 

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-31-1.svg)

    ggsave(filename = file.path(outdir, "fig4a_num_dhs_conditions_var.pdf"), width = 6, height = 3)

Effect sizes (Cohen’s d)

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_dhs_conditions.prox", comparisons = c("broad", "narrow-low"))

    ## [1] -0.9468371

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_dhs_conditions.prox", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.788174

    ggplot(mt %>% filter(num_dhs_any.prox > 0), aes(y = num_tf_peaks.prox, x = var_type, fill = var_type)) + 
      geom_violin(alpha = 0.2) +
      geom_boxplot(width = 0.3, outlier.size = 0.1) + 
      theme_bw() + 
      scale_fill_manual(values = col_groups, guide = FALSE) +
      xlab("") + 
      rotate() +
    #  ylab("Number of different TF peaks at TSS\n(only genes with TSS-proximal DHSs)") + 
      ylab("# TF peaks at TSS\n(only genes with TSS-proximal DHS)") + 
      stat_compare_means(comparisons = comp) +
      #                   symnum.args = list(cutpoints = c(0, 0.0001, 0.001, 0.01, 0.05, 1), 
      #                                      symbols = c("****", "***", "**", "*", "ns"))) +
       theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=14), axis.title.y = element_text(size=14)) +
      stat_summary(fun.data = function(x) c(y = -0.4, angle = -90, label = length(x)), geom = "text", size = 4) 

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-33-1.svg)

    ggsave(filename = file.path(outdir, "fig4b_num_tf_peaks_var.pdf"), width = 6., height = 3)

Effect sizes (Cohen’s d)

    get_cohen_d_two_groups(mt %>% filter(num_dhs_any.prox > 0), group_var = "var_type", compare_var = "num_tf_peaks.prox", comparisons = c("broad", "narrow-low"))

    ## [1] -0.2857426

    get_cohen_d_two_groups(mt %>% filter(num_dhs_any.prox > 0), group_var = "var_type", compare_var = "num_tf_peaks.prox", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.3386312

Figure 4c
---------

Top: enrichment (odds ratio from Fisher’s test) of ChIP peaks for 24 TFs
in TSS-proximal DHSs of broad, narrow-low and narrow-high genes. Only
TFs with predictive importance for expression variation (based on
Boruta) were included. For each TF, Fisher’s test was performed
separately for each category vs all other. Color = log2 odds ratio from
Fisher’s exact test (two-sided), grey = non-significant comparisons
(adjusted p-value cutoff of 0.01, Benjamini-Hochberg correction on all
24x3 comparisons). Black stars on the heatmap indicate strong
enrichmenrs (odds ratio above 2). Lower panels: Presence of BEAF-32
(left) and Trl (right) ChIP-seq peaks in TSS-proximal DHS, plotted in
coordinates of promoter shape index and expression variation (same as
Fig. 3a). Each dot represents a gene (grey if TF peak is absent, blue
for Trl, orange for BEAF-32). Boxplots on the left and right sides of
the scatter plots compare expression variation (y-axis) of genes with
and without ChIP-seq peak (x-axis) for broad and narrow promoter genes
respectively. Numbers under the boxplots indicate number of genes in
each category.

    dir = file.path(project_folder, "/analysis/analysis_of_features")

    fishers_path =  file.path(dir, "fisher_tests_tf_peaks.csv")
    fishers = read.csv(fishers_path) 

    # filter by odds-ratio
    #fishers = fishers_full %>% filter(odds_ratio > 2 | odds_ratio < 0.5)

    path = file.path(project_folder, "/analysis/random_forests/significant_features_importance_and_correlations.csv")
    feature_names = read.csv(path) %>% select(feature = var, full_name)

    min_or = min(fishers$odds_ratio)
    max_or = max(fishers$odds_ratio)

    fishers %<>% select(feature, group_val, odds_ratio) %>% 
      mutate(odds_ratio = round(odds_ratio, 1)) %>%
      spread(group_val, odds_ratio) %>%
      select(feature,`narrow-high`, `narrow-low`,  broad) 

    fishers = merge(fishers, feature_names, by = "feature") %>% arrange(-broad)

    fishers$full_name = gsub(" peak \\(prox\\)", "", fishers$full_name)

    df = data.frame(log2(t(fishers[ ,2:4])))

    paletteLength <- 41
    myColor <- colorRampPalette(c("cornflowerblue", "white", "brown2"))(paletteLength)
    #myBreaks = c(0:5/5, 1.5, 2, 3, 5, 10, 30)
    myBreaks = -20:20/4

    outf = file.path(outdir, "fig4c_fisher_tests_tf_peaks_or2.pdf")
    outf = file.path(outdir, "fig4c_fisher_tests_tf_peaks.pdf")
    pheatmap(df, cluster_rows = FALSE, cluster_cols = FALSE, 
             #display_numbers = TRUE, number_format = "%.1f", 
             color=myColor, 
             breaks = myBreaks, 
             legend = TRUE,
             labels_col = fishers$full_name,
             filename = outf, width = 9, height = 2.5)

Pheatmap function only writes to file (can’t write to screen). Below are
the underlying values.

odds ratio from Fisher’s exact test:

    df = data.frame(round(t(fishers[ ,2:4]), 2))
    names(df) = fishers$full_name
    df

    ##              pzg MESR4 BEAF-32 E(bx)   Dp CG4617 gfzf YL-1 Sin3A Sry-delta
    ## narrow-high  0.1   0.1     0.1   0.1  0.2    0.1  0.1  0.1   0.1       0.2
    ## narrow-low   0.2   0.2     0.2   0.3  0.3    0.3  0.3  0.3   0.3       0.3
    ## broad       21.3  18.6    16.3  15.3 10.8   10.7  9.3  9.3   8.8       8.5
    ##             CG2116 M1BP Ets97D CG12659 crp Elba2 CG2120 Su(var)3-9 CG33213
    ## narrow-high    0.1  0.1    0.2     0.2 0.2   0.2    0.3        0.2     0.3
    ## narrow-low     0.4  0.3    0.4     0.4 0.5   0.5    0.6        0.6     0.6
    ## broad          7.9  7.9    6.5     5.8 4.3   3.6    3.4        3.4     3.1
    ##             Sry-beta ewg CG12071 Jarid2 Trl
    ## narrow-high      0.3 0.4      NA    1.3 1.8
    ## narrow-low       0.7 1.3     1.7    3.2 3.6
    ## broad            2.7 1.3     0.6    0.3 0.2

log2 odds ratio from Fisher’s exact test:

    df = data.frame(round(log2(t(fishers[ ,2:4])), 2))
    names(df) = fishers$full_name
    df

    ##               pzg MESR4 BEAF-32 E(bx)    Dp CG4617  gfzf  YL-1 Sin3A
    ## narrow-high -3.32 -3.32   -3.32 -3.32 -2.32  -3.32 -3.32 -3.32 -3.32
    ## narrow-low  -2.32 -2.32   -2.32 -1.74 -1.74  -1.74 -1.74 -1.74 -1.74
    ## broad        4.41  4.22    4.03  3.94  3.43   3.42  3.22  3.22  3.14
    ##             Sry-delta CG2116  M1BP Ets97D CG12659   crp Elba2 CG2120
    ## narrow-high     -2.32  -3.32 -3.32  -2.32   -2.32 -2.32 -2.32  -1.74
    ## narrow-low      -1.74  -1.32 -1.74  -1.32   -1.32 -1.00 -1.00  -0.74
    ## broad            3.09   2.98  2.98   2.70    2.54  2.10  1.85   1.77
    ##             Su(var)3-9 CG33213 Sry-beta   ewg CG12071 Jarid2   Trl
    ## narrow-high      -2.32   -1.74    -1.74 -1.32      NA   0.38  0.85
    ## narrow-low       -0.74   -0.74    -0.51  0.38    0.77   1.68  1.85
    ## broad             1.77    1.63     1.43  0.38   -0.74  -1.74 -2.32

BEAF-32 and GAF examples

First scatter plots:

    fun_plot_tfs = function(p, tf = "modERN.Trl.E8_16.prox", col = "midnightblue", lab = "GAF") {
      
      p + 
      geom_point(data = mt[mt[tf] == 0, ], size = 1, color = "grey", alpha = 0.5) + 
      geom_point(data =  mt[mt[tf] == 1, ], size = 1, aes(color = lab)) +
      theme_bw() + 
      scale_color_manual(values = col, name = "") +
      xlab("shape index") + 
      ylab("expression variation") +
      guides(colour = guide_legend(override.aes = list(size=8))) +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position = c(0, 1), legend.justification = c(-0.2, 1.1))
    }


    p1 = fun_plot_tfs(ggplot(mt, aes(x = major_shape_ind, y = resid_cv)), tf = "modERN.BEAF_32.E0_14.prox", col = "sandybrown", lab = "BEAF-32")
    p2 = fun_plot_tfs(ggplot(mt, aes(x = major_shape_ind, y = resid_cv)), tf = "modERN.Trl.E8_16.prox", col = "midnightblue", lab = "GAF")

    p = grid.arrange(p1, p2, ncol = 2)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-38-1.svg)

    ggsave(filename = file.path(outdir, "fig4c_tf_examples.pdf"), width = 10, height = 5)  

And the side boxplots (orange - BEAF32, blue - Trl)

    fun_boxplot = function(p, xlab = "", ylab = "", comp = NA, col = cbPalette) {
      
      p +
      geom_boxplot(width = 0.5, outlier.size = 0.1) +
      stat_compare_means(comparisons = comp) + 
      theme_bw() + 
      ylim(c(-1.2, 2.5)) +  
      scale_fill_manual(values = col, guide = FALSE) +
      xlab(xlab) + 
      ylab(ylab) + 
      stat_summary(fun.data = function(x) c(y = -1.1, label = length(x)), geom = "text", size = 3) +
      theme(axis.text.y = element_text(size=14), 
            axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=16), legend.title=element_text(size=16),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position = c(0, 1), legend.justification = c(-0.2, 1.1),
            legend.background = element_rect(fill = "transparent"),
            panel.spacing = unit(3, "lines"))
            #plot.margin = unit(c(0.75, 0.75, 0.75, 0.05), "cm"))
      
    }

    p1 = fun_boxplot(ggplot(mt %>% filter(shape == "broad"), aes(y = resid_cv, fill = factor(modERN.BEAF_32.E0_14.prox), x = factor(modERN.BEAF_32.E0_14.prox))), ylab = "", comp = list(c("1", "0")), col = c("grey", "sandybrown")) + ggtitle("broad")
    p2 = fun_boxplot(ggplot(mt %>% filter(shape == "narrow"), aes(y = resid_cv, fill = factor(modERN.BEAF_32.E0_14.prox), x = factor(modERN.BEAF_32.E0_14.prox))), ylab = "", comp = list(c("1", "0")), col = c("grey", "sandybrown")) + ggtitle("narrow")

    p3 = fun_boxplot(ggplot(mt %>% filter(shape == "broad"), aes(y = resid_cv, fill = factor(modERN.Trl.E8_16.prox), x = factor(modERN.Trl.E8_16.prox))), ylab = "", comp = list(c("1", "0")), col = c("grey", "darkblue")) + ggtitle("broad")
    p4 = fun_boxplot(ggplot(mt %>% filter(shape == "narrow"), aes(y = resid_cv, fill = factor(modERN.Trl.E8_16.prox), x = factor(modERN.Trl.E8_16.prox))), ylab = "", comp = list(c("1", "0")), col = c("grey", "darkblue")) + ggtitle("narrow")

    p = grid.arrange(p1, p2, p3, p4, ncol = 4 )         

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-39-1.svg)

    ggsave(filename = file.path(outdir, "fig4c_tf_boxplots.pdf"), p, width = 7, height = 3)

Figure4d-f
----------

Relationship between polymerase pausing index (d), number of miRNA
motifs in 3’UTR of a gene (e) and number of TSS-distal DHS peaks (f) and
expression variation for broad (orange) and narrow (blue) promoter
genes. Each dot represents a gene, lines linear regression fits,
rho=Spearman correlation coefficient.

    fun_plot_narrow_compl = function(p, cor_broad, cor_narrow, xlab = "", xlabel_loc = 2) {
      
      p +
      scale_x_continuous(trans='log2') +
      geom_point(size = 0.3, alpha = 0.3) + 
    #  geom_density2d() +
      geom_smooth(method = "lm") + 
      scale_color_manual(values = c("peru", "midnightblue"), name = "promoter", labels = c("broad", "narrow")) +
      xlab(xlab) + 
      theme_bw() +
      xlab(xlab) + 
      ylab("Expression variation") + 
      annotate("text", x = xlabel_loc, y = -1, label = paste0("R = ", sprintf("%0.2f", cor_broad)), color = "peru", size = 7) +
      annotate("text", x = xlabel_loc, y = 1.8, label = paste0("R = ", sprintf("%0.2f", cor_narrow)), color = "midnightblue", size = 7) +
      guides(colour = guide_legend(override.aes = list(size=8))) +
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=18), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            legend.position = c(1, 1), legend.justification = c(1.1, 1.05),
            legend.background = element_rect(fill = "transparent"),
            plot.margin = unit(c(1, 1, 1, 1), "cm"))
    }
      


    cors = mt %>% group_by(shape) %>% summarize(
      cor_enh = cor(resid_cv, num_dhs_any.dist, method = "spearman"), 
      cor_mirna = cor(resid_cv, num_miRNA, method = "spearman"),
      cor_pi = cor(resid_cv, PI, method = "spearman"))


    #p1 = blankPlot
    p1 = fun_plot_narrow_compl(ggplot(mt, aes(y = resid_cv, x = PI + 2, color = factor(shape))), cors[1, 4], cors[2, 4], xlab = "PolII pausing index", xlabel_loc= 4)
    p2 = fun_plot_narrow_compl(ggplot(mt, aes(y = resid_cv, x = num_miRNA + 1, color = factor(shape))), cors[1, 3], cors[2, 3], xlab = "Number of miRNA motifs", xlabel_loc = 3)
    p3 = fun_plot_narrow_compl(ggplot(mt, aes(y = resid_cv, x = num_dhs_any.dist + 1, color = factor(shape))), cors[1, 2], cors[2, 2], xlab = "Number of distal DHS", xlabel_loc = 2)

    p = grid.arrange(p1, p2, p3, ncol = 3)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-40-1.svg)

    ggsave(filename = file.path(outdir, "fig4def_pi_miRNA_enh_with_var.eps"), p, width = 17, height = 5, device = cairo_ps)

Figure 4g
---------

Gene scores by two indices constructed as the normalized rank average
of: number of embryonic conditions with DHS, number of TF peaks, number
of TF motifs (Broad regulatory index; left), and number of TSS-distal
DHS, number of miRNA motifs, Pol II pausing index (Narrow regulatory
index; right). Colors correspond to broad (orange), narrow-low (blue)
and narrow-high (red) gene groups. P-values &lt; 1e-09 for all pairwise
comparisons of the distributions.

    #med_var_narrow = median(mt[mt$shape == "narrow", "resid_cv"])
    mt$var_type = "broad"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv > med_var_narrow] = "narrow-high"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv < med_var_narrow] = "narrow-low"
    mt$var_type = factor(mt$var_type, levels = c("broad", "narrow-low", "narrow-high"))

    tmp = mt %>% select(var_type, broad_index, narrow_index) %>% gather(index, value, -var_type)

    ggplot(tmp, aes(x = value, fill = var_type, color = var_type)) + 
      geom_density(alpha = 0.4) + 
      facet_wrap(~ifelse(index == "broad_index", "Broad index", "Narrrow index")) +
      theme_bw() +
      scale_fill_manual(values =  c("peru", "deepskyblue", "maroon")) +
      scale_color_manual(values =  c("peru", "darkblue", "maroon")) +
      xlab("Index value") + 
      ylab("Density") + 
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=18), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            legend.background = element_rect(fill = "transparent"),
            panel.spacing = unit(5, "lines"))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-41-1.svg)

    ggsave(filename = file.path(outdir, "fig4g_complexity_indices.pdf"), width = 20, height = 5)

Wilcoxon tests for group comparisons

    wilcox.test(broad_index ~ var_type, mt %>% filter(var_type != "broad"))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  broad_index by var_type
    ## W = 919180, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

    wilcox.test(broad_index ~ var_type, mt %>% filter(var_type != "narrow-high"))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  broad_index by var_type
    ## W = 1491300, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

    wilcox.test(broad_index ~ var_type, mt %>% filter(var_type != "narrow-low"))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  broad_index by var_type
    ## W = 1802400, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

    wilcox.test(narrow_index ~ var_type, mt %>% filter(var_type != "broad"))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  narrow_index by var_type
    ## W = 892740, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

    wilcox.test(narrow_index ~ var_type, mt %>% filter(var_type != "narrow-high"))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  narrow_index by var_type
    ## W = 708690, p-value < 2.2e-16
    ## alternative hypothesis: true location shift is not equal to 0

    wilcox.test(narrow_index ~ var_type, mt %>% filter(var_type != "narrow-low"))

    ## 
    ##  Wilcoxon rank sum test with continuity correction
    ## 
    ## data:  narrow_index by var_type
    ## W = 1151800, p-value = 8.802e-09
    ## alternative hypothesis: true location shift is not equal to 0

Supplementary Figure 4.
=======================

Figure S4a-f
------------

    med_var_narrow = median(mt[mt$shape == "narrow", "resid_cv"])

    mt$var_type = "broad"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv > med_var_narrow] = "narrow-high"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv < med_var_narrow] = "narrow-low"
    mt$var_type = factor(mt$var_type, levels = c("broad", "narrow-low", "narrow-high"))

    comp = list(c("broad", "narrow-low"), c("narrow-low", "narrow-high"))
    col_groups = c("peru", "lightblue", "maroon")


    fun_plot = function(p, col = col_groups, y_label = "") {
      
      p +
      geom_violin(alpha = 0.2) +
      geom_boxplot(width = 0.2) + 
      theme_bw() + 
      scale_fill_manual(values = col_groups, guide = FALSE) +
      xlab("") + 
      rotate() +
      ylab(y_label) + 
      stat_compare_means(comparisons = comp) +
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=16), 
            axis.title.x = element_text(size=16), axis.title.y = element_text(size=18),
            plot.margin = unit(c(0.5, 0.5, 0.5, 0.5), "cm"))
      
    }


    # TF motifs
    p1 = fun_plot(ggplot(mt %>% filter(num_dhs_any.prox > 0), aes(y = num_tf_motifs.prox, x = var_type, fill = var_type)),
                  y_label = "# TF motifs at TSS")

    # TF peaks with motifs
    p2 = fun_plot(ggplot(mt %>% filter(num_dhs_any.prox > 0), aes(y = num_tf_peaks_with_motifs.prox , x = var_type, fill = var_type)),
                  y_label = "# TF peaks with motifs at TSS")

    # PolII pausing 
    p3 = fun_plot(ggplot(mt, aes(y = PI + 2, x = var_type, fill = var_type)) +
                   scale_y_continuous(trans = "log2")  , 
                  y_label = "PolII pausing index")

    # miRNA motifs
    p4 = fun_plot(ggplot(mt, aes(y = num_miRNA + 1, x = var_type, fill = var_type)) +
                   scale_y_continuous(trans = "log2"), 
                   y_label = "# miRNA motifs in 3'-UTR")

    # RBP motifs
    p5 = fun_plot(ggplot(mt, aes(y = num_rbp + 1, x = var_type, fill = var_type)) +
                   scale_y_continuous(trans = "log2"), 
                   y_label = "# RBP motifs in 3'-UTR")

    # distal DHS
    p6 = fun_plot(ggplot(mt, aes(y = num_dhs_any.dist + 1, x = var_type, fill = var_type)) +
                   scale_y_continuous(trans = "log2"), 
                   y_label = "# distal DHS peaks")

    p = grid.arrange(p1, p2, p3, p4, p5, p6, ncol = 2)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-43-1.svg)

    ggsave(file.path(outdir, "figS4abcdef_groups_by_features.pdf"), p, width = 14, height = 14)

and Cohen’s d statistics

    get_cohen_d_two_groups(mt  %>% filter(num_dhs_any.prox > 0), group_var = "var_type", compare_var = "num_tf_motifs.prox", comparisons = c("broad", "narrow-low"))

    ## [1] -0.6489478

    get_cohen_d_two_groups(mt  %>% filter(num_dhs_any.prox > 0), group_var = "var_type", compare_var = "num_tf_motifs.prox", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.2837736

    get_cohen_d_two_groups(mt  %>% filter(num_dhs_any.prox > 0), group_var = "var_type", compare_var = "num_tf_peaks_with_motifs.prox", comparisons = c("broad", "narrow-low"))

    ## [1] -0.2543474

    get_cohen_d_two_groups(mt  %>% filter(num_dhs_any.prox > 0), group_var = "var_type", compare_var = "num_tf_peaks_with_motifs.prox", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.3254303

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "PI", comparisons = c("broad", "narrow-low"))

    ## [1] 0.6409609

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "PI", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.3039242

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_miRNA", comparisons = c("broad", "narrow-low"))

    ## [1] 0.3084088

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_miRNA", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.4232419

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_rbp", comparisons = c("broad", "narrow-low"))

    ## [1] 0.2889119

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_rbp", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.364728

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_dhs_any.dist", comparisons = c("broad", "narrow-low"))

    ## [1] 0.3480341

    get_cohen_d_two_groups(mt, group_var = "var_type", compare_var = "num_dhs_any.dist", comparisons = c("narrow-high", "narrow-low"))

    ## [1] -0.3661693

Figure S4g
----------

    dir = file.path(project_folder, "/analysis/random_forests")
    path = file.path(dir, "significant_features_importance_and_correlations.csv")

    df = read.csv(path)
    df %<>% filter(!is.na(med_imp_narrow_var)) 
    df[is.na(df)] = 0.1 # pseudocount for non-significant features

    # remove similar features
    rl = c("promoter shape","insulation score (6-8h)", "TAD size(2-4h)", "gene conservation score","DHS time profile (prox)",
           "DHS tissue profile (prox)", "ubiquitous DHS (prox)", "number of TF peaks with motifs (prox)", "3'UTR LFC 10-12h vs. 2-4h")

    # Top-50 features
    df %<>% arrange(-med_imp_narrow_var) 
    df_top = df[1:50, ] 

    df_top$full_name = gsub("number of|num.", "#", df_top$full_name)
    df_top$full_name = gsub("dme\\.", "", df_top$full_name)

    df_top$full_name = factor(df_top$full_name, levels = rev(df_top$full_name))

    df_top %<>% 
      gather(responce, med_imp, med_imp_narrow_var:med_imp_narrow_lev) %>%
      mutate(cor = ifelse(responce == "med_imp_narrow_var", cor_var_narrow, cor_med_narrow)) 


    # set colors
    label_col = c("#4682B4", "#BDB76B", "#006400", "#E31A1C", "#8B4513", "#CC79A7", "#999999")
    df$class = factor(df$class, levels = unique(df$class))
    names(label_col) = unique(df$class)

    label_col_vect = label_col[rev(df_top$class)]


    ggplot(df_top, aes(y = full_name, x = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
      geom_point() +
      theme_bw() +
      xlab("Median feature importance") + 
      ylab("") +
      scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
      scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
      scale_size_continuous(name = "abs. cor", range = c(2, 10)) +
      scale_shape_manual(values = c(25, 24), name = "cor sign", labels = c("negative", "positive")) +
      guides(color = guide_legend(override.aes = list(size=8), order = 1),
             shape = guide_legend(override.aes = list(size=4, fill = "black"), order = 2)) +
      theme(axis.text.x = element_text(size=25), axis.text.y = element_text(size=25, color = label_col_vect, hjust=0.95,vjust=0.2), 
            axis.title.y = element_text(size=25), axis.title.x = element_text(size=30),
            legend.text=element_text(size=30), legend.title=element_text(size=30),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=1),
            legend.justification = "top")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-50-1.svg)

    ggsave(filename = file.path(outdir, "figS4g_rf_narrow_top_features_col_by_class.eps"), width = 12, height = 20) # eps format is parsed better in illustrator

Figure S4h
----------

    med_var_narrow = median(mt[mt$shape == "narrow", "resid_cv"])

    mt$var_type = "broad"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv > med_var_narrow] = "other narrow-high"
    mt$var_type[mt$shape == "narrow" & mt$resid_cv < med_var_narrow] = "other narrow-low"
    mt$var_type[mt$shape == "narrow" & mt$ohler_maj.TATA == TRUE] = "narrow TATA"
    mt$var_type[mt$shape == "narrow" & mt$is_tf == TRUE] = "narrow TF"

    tmp = mt %>% select(var_type,  broad_index, narrow_index) %>% gather(index, value, -var_type)

    ggplot(tmp, aes(x = value, fill = var_type, color = var_type)) + 
      geom_density(alpha = 0.4) +
      facet_wrap(~ifelse(index == "broad_index", "Broad index", "Narrrow index")) +
      theme_bw() +
      scale_fill_manual(values = c("peru", "maroon", "midnightblue", "coral2" , "lightblue"), name = "gene type") + 
      scale_color_manual(values = c("peru", "maroon", "midnightblue", "coral2" , "lightblue"), name = "gene type") + 
      #                    labels = c("housekeeping", "TFs (non-hk)", "TATA (non-hk)", "other"))  +  
      # scale_color_manual(values = c("peru", "midnightblue", "maroon", "lightgrey"), guide = FALSE) + 
      xlab("Index value") + 
      ylab("Density") + 
      theme(axis.text.y = element_text(size=18), axis.text.x = element_text(size=18), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
            legend.text=element_text(size=18), legend.title=element_text(size=18),
            legend.background = element_rect(fill = "transparent"),
            panel.spacing = unit(3, "lines"))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-51-1.svg)

    ggsave(filename = file.path(outdir, "figS4h_complexity_indices_tata_tf.pdf"), width = 12, height = 6)

Figure 5. Expression variation can predict signatures of differential expression upon various conditions.
=========================================================================================================

Figure 5a
---------

Expression variation of genes differentially expressed (DE) upon any
stress conditions in (Moskalev et al. 2015) compared to
non-differentially expressed genes (non-DE), Cohen’s d=0.62.

    df_stress = load_Moscalev2015_stress_responce_genes()

    mt$DE = ifelse(mt$gene_name %in% df_stress$gene_name, "DE", "non-DE")
    mt$DE = factor(mt$DE, levels = c("non-DE", "DE"))

    n_genes = mt %>% group_by(DE) %>% tally()
    n_de = n_genes[1, 2]
    n_non_de = n_genes[2, 2]

    lab = c(paste0("non-DE (N=", n_non_de, ")"), paste0("DE (N=", n_de, ")"))

    ggplot(mt, aes(x = resid_cv, fill = DE, color = DE)) + 
      geom_density(alpha = 0.5) +
      theme_bw() + 
      scale_fill_manual(values = cbPalette, name = "", labels = lab) +
      scale_color_manual(values = cbPalette, name = "", labels = lab) +
      xlab("Expression variation") + 
      ylab("Density") +
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position = c(1, 1), legend.justification = c(1.1, 1.1),
            legend.background = element_rect(fill = "transparent"),
            legend.title = element_text(size=16), legend.text = element_text(size=16))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-52-1.svg)

    ggsave(filename = file.path(outdir, "fig5a_DE_variation.pdf"),  width = 6, height = 4)

Cohen’s d

    cohen.d(mt[ , c("DE", "resid_cv")], "DE")$cohen.d[2]

    ## [1] 0.6253822

Figire 5b-c
-----------

Differences in scores by the broad and narrow indices (from Fig. 4g)
between DE and non-DE genes (from Fig. 6a) split by promoter shape:
broad index (b), narrow index (c), p-values from Wilcoxon rank test,
numbers under boxplots indicate number of genes. Cohen’s d: (b) 0.25 and
0.60, (c) 0.75 and 0.90 for broad and narrow promoter genes,
respectively (absolute values).

    comp = list(c("DE", "non-DE"))

    plot_format =   function(p, text_loc = -0.05) {
        p + 
        geom_violin(alpha = 0.5, width = 0.7) +
        geom_boxplot(width = 0.3, outlier.size = 0.1) +
          theme_bw() + 
          stat_compare_means(comparisons = comp) +
        stat_summary(fun.data = function(x) c(y = text_loc, label = length(x)), geom = "text", size = 4) +
          scale_fill_manual(values = cbPalette, guide = FALSE) +
          theme(axis.text.y = element_text(size=12), axis.text.x = element_text(size=12), 
            axis.title.x = element_text(size=12), axis.title.y = element_text(size=12),
            legend.text=element_text(size=12), legend.title=element_text(size=12),
            strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
            plot.title = element_text(color="black", face="bold", size=20, hjust=0.5),
            legend.position = c(0, 1), legend.justification = c(-0.1, 1.05),
            plot.margin = unit(c(0.3, 0.3, 0.3, 0.3), "cm")) 
      }



    p1 = ggplot(mt, aes(y = broad_index, x = DE, fill = DE)) + 
      ylab("Broad index") + facet_wrap(~shape) + xlab("") 

    p2 = ggplot(mt, aes(y = narrow_index, x = DE, fill = DE)) + 
      ylab("Narrow index") + facet_wrap(~shape) + xlab("") 


    p1 = plot_format(p1)
    p2 = plot_format(p2)


    p = grid.arrange(p1, p2, ncol = 2)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-54-1.svg)

    ggsave(filename = file.path(outdir, "fig5bc_de_stress_genes_complexity_shape_split.pdf"), p, width = 7, height = 4)

Cohen’s d:

    get_cohen_d_two_groups(mt %>% filter(shape == "broad"), group_var = "DE", compare_var = "broad_index", comparisons = c("DE", "non-DE"))

    ## [1] -0.2380952

    get_cohen_d_two_groups(mt %>% filter(shape == "narrow"), group_var = "DE", compare_var = "broad_index", comparisons = c("DE", "non-DE"))

    ## [1] -0.6073739

    get_cohen_d_two_groups(mt %>% filter(shape == "broad"), group_var = "DE", compare_var = "narrow_index", comparisons = c("DE", "non-DE"))

    ## [1] -0.7538051

    get_cohen_d_two_groups(mt %>% filter(shape == "narrow"), group_var = "DE", compare_var = "narrow_index", comparisons = c("DE", "non-DE"))

    ## [1] -0.9029289

    #get_cohen_d_two_groups(mt %>% filter(shape == "broad"), group_var = "DE", comparisons = c("DE", "non-DE"))

Figure 5d
---------

ROC-cures for predicting DE (from Fig. 6a) with random forest models
trained on expression variation (top-30% variable vs. bottom-30%
variable) in all genes (light blue) or narrow promoter genes (dark
blue). Models were trained and tested on non-overlapping subsets of
genes in 10 random sampling rounds (all plotted). Median AUC values from
10 sampling rounds.

    dir = file.path(project_folder, "/analysis/de_predictions")
    pred_all = read.csv(file.path(dir, "transfer_learning_performance.csv"))

    # median AUC across 10 iteration for models trained on narrow or all genes
    auc_sum = pred_all %>% group_by(learner) %>% summarize(auc = median(AUC))

    rocl_labels = c(paste0("all genes, AUC = ", round(auc_sum[1, 2], 2)),
                    paste0("narrow only, AUC = ", round(auc_sum[2, 2], 2)))

    ggplot(pred_all, aes(x = fpr, y = tpr, color = learner, group = interaction(i, learner))) +
      geom_line(size = 0.3) + 
      geom_abline(intercept = 0, slope = 1, linetype = "dashed") +
      theme_bw() +
      xlab("False positive rate") +
      ylab("True positive rate") +
      scale_color_manual(values = c("#B0C4DE", "#4682B4"), name = "Training dataset:", labels = rocl_labels) +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=14), axis.title.y = element_text(size=14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.title = element_text(size=12), legend.text = element_text(size=12),
            legend.position = c(1, 0), legend.justification = c(1.02, -0.2))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-56-1.svg)

    ggsave(filename = file.path(outdir, "fig5d_transfer_roc_curves.pdf"), width = 4, height = 4)

**Figure 5e**. Difference in expression variation for DE vs. non-DE
genes in 53 datasets with different genetic perturbations (Dataset EV8).
Cohen’s d and p-value from Wilcoxon rank test for comparing DE
vs. non-DE genes are plotted on the axis, each dot represents one
dataset. Vertical dashed lines indicate empirical cut-offs for small,
medium and large effect size proposed in (Cohen 1988).

    path = "/g/furlong/project/62_expression_variation//analysis/de_predictions/ExpressionAtlas_and_lab_meso/ExpressionAtlas_and_lab_meso_de_genes.csv"
    de_annot = read.csv(path)
    sel_ids1 = which(de_annot$cond_type == "genetic perturbation" & de_annot$Filter < 1)
    sel_ids2 = which(de_annot$cond_type == "environment/infection" & de_annot$Filter < 1)
    de_annot_sel = de_annot %>% filter(cond_type == "genetic perturbation" & Filter < 1 & cond_type == "genetic perturbation")

    path = "/g/furlong/project/62_expression_variation//analysis/de_predictions/ExpressionAtlas_and_lab_meso/DE_statistics_by_genes.csv"
    df = read.csv(path)
    df$n_de_genet = rowSums(df[ , sel_ids1])
    df$n_de_envir = rowSums(df[ , sel_ids2])
    df = merge(df, mt, by = "gene_id")

    ggplot(de_annot_sel, 
           aes(x = cohen_d, y = -log10(wilcox_pval))) + 
      geom_point(size = 3, color = "darkblue", alpha = 0.7) +
    #  scale_shape_manual(values = c(21, 24), name = "Source") +
      theme_bw() + 
      xlab("Cohen's d") +
      ylab("P-value (-log10)") +
      geom_vline(xintercept = 0.2, color = "darkred", linetype = "dashed", size = 0.3) +
      geom_vline(xintercept = 0.5, color = "darkred", linetype = "dashed", size = 0.3) +
      geom_vline(xintercept = 0.8, color = "darkred", linetype = "dashed", size = 0.3) +
      annotate(x = 0.23, y = 90, geom = "text", label = "small", angle = 90, hjust = 0, color = "darkred") +
      annotate(x = 0.53, y = 90, geom = "text", label = "medium", angle = 90, hjust = 0, color = "darkred") +
      annotate(x = 0.83, y = 90, geom = "text", label = "large", angle = 90, hjust = 0, color = "darkred")

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-57-1.svg)

    ggsave(filename = file.path(outdir, "fig5e_DE_genetic_cohenD.eps"), device = cairo_pdf,  width = 3.5, height = 4)

Figure 5f
---------

Expression variation (y-axis) of genes found as DE in different number
of genetic perturbation studies (groups on the x-axis), p-values from
Wilcoxon rank test, numbers under boxplots indicate number of genes.

    df %<>% mutate(bin_de_genet = cut(n_de_genet, c(-1, 0, 5, 10, 15, max(df$n_de_genet)), 
                                      include.lowest = TRUE, dig.lab=10)) 

    comparisons = list(c("[-1,0]", "(0,5]"), c("(5,10]", "(0,5]"), c("(5,10]", "(10,15]"), c("(15,28]", "(10,15]"))

    ggplot(df, aes(x = bin_de_genet, y = resid_cv)) +
      geom_boxplot(width = 0.5, outlier.size = 0.01) +
      stat_compare_means(comparisons = comparisons) +
      theme_bw() +
      xlab("Number of experiments where gene is DE") + 
      ylab("Expression variation") + 
      stat_summary(fun.data = function(x) c(y = -1.1, label = length(x)), geom = "text", size = 3) +
      theme(axis.title.x = element_text(size=14), axis.text.x = element_text(size=12, angle = 30, hjust = 1),
            axis.title.y = element_text(size=14), axis.text.y = element_text(size=12))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-58-1.svg)

    ggsave(filename = file.path(outdir, "Fig6f_DE_genetic_num_datasets.pdf"),  width = 2.5, height = 4)

Not used - share of broad/narrow genes:

    df_sum = df %>% group_by(bin_de_genet) %>% mutate(N = n()) %>%
      group_by(bin_de_genet, shape) %>% summarize(n = n()) %>%
      group_by(bin_de_genet) %>% 
      mutate(N = sum(n), 
             share = round(n / N, 2))


    ggplot(df_sum, aes(x = bin_de_genet, fill = shape, y = share)) + 
      geom_bar(stat = "identity") + 
      theme_bw() + 
      scale_fill_manual(values = cbPalette, name = "promoter shape") +
      xlab("") + 
      ylab("Share of genes") +
      xlab("Number of experiments where gene is DE") + 
      theme(axis.text.y = element_text(size=12), axis.text.x = element_text(size=12,  hjust = 1), 
            axis.title.x = element_text(size=15), axis.title.y = element_text(size=15)) 

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-59-1.svg)

    df %>% 
      group_by(n_de_genet > 10) %>% mutate(N = n()) %>% ungroup() %>%
      group_by(n_de_genet > 10, shape) %>% 
      summarize(n = n(), n / mean(N))

    ## # A tibble: 4 x 4
    ## # Groups:   n_de_genet > 10 [?]
    ##   `n_de_genet > 10` shape      n `n/mean(N)`
    ##   <lgl>             <fct>  <int>       <dbl>
    ## 1 FALSE             broad   1704       0.535
    ## 2 FALSE             narrow  1481       0.465
    ## 3 TRUE              broad     94       0.106
    ## 4 TRUE              narrow   795       0.894

Figure 5g-h
-----------

Same as (b-c) for comparison between genes that were found as DE in more
than 10 studies with genetic perturbation vs. genes DE in 0-10 studies.
Cohen’s d: (g) 0.86 and 0.41, (h) 0.14 and 0.43 for broad and narrow
promoter genes, respectively (absolute values).

    df %<>% mutate(group = ifelse(n_de_genet > 10, ">10", "0-10"))
    df$group = factor(df$group, levels = c("0-10", ">10"))
    comp = list(c(">10", "0-10"))

    p1 = ggplot(df, aes(y = broad_index, x = group, fill = group)) + 
      ylab("Broad index") + facet_wrap(~shape) + xlab("# DE datasets")
    p2 = ggplot(df, aes(y = narrow_index, x = group, fill = group)) + 
      ylab("Narrow index") + facet_wrap(~shape) + xlab("# DE datasets")

    p5 = plot_format(p1)
    p6 = plot_format(p2)

    p = grid.arrange(p5, p6, ncol = 2)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-61-1.svg)

    ggsave(filename = file.path(outdir, "fig5gh_de_genet_genes_complexity_shape_split.pdf"), p, width = 7, height = 4)

Cohen’s d:

    get_cohen_d_two_groups(df %>% filter(shape == "broad"), group_var = "group", compare_var = "broad_index", comparisons = c(">10", "0-10"))

    ## [1] -0.8647311

    get_cohen_d_two_groups(df %>% filter(shape == "narrow"), group_var = "group", compare_var = "broad_index", comparisons = c(">10", "0-10"))

    ## [1] -0.4104488

    get_cohen_d_two_groups(df %>% filter(shape == "broad"), group_var = "group", compare_var = "narrow_index", comparisons = c(">10", "0-10"))

    ## [1] 0.1839686

    get_cohen_d_two_groups(df %>% filter(shape == "narrow"), group_var = "group", compare_var = "narrow_index", comparisons = c(">10", "0-10"))

    ## [1] -0.4410968

Figure 5i
---------

Same as (d) for comparison between genes that were found as DE in more
than 10 studies with genetic perturbation vs. genes DE in 0-10 studies.

    dir = file.path(project_folder, "/analysis/de_predictions")
    pred_all = read.csv(file.path(dir, "transfer_learning_performance.expr_atlas_and_lab_genet.csv"))

    # median AUC across 10 iteration for models trained on narrow or all genes
    auc_sum = pred_all %>% group_by(learner) %>% summarize(auc = median(AUC))

    rocl_labels = c(paste0("all genes, AUC = ", round(auc_sum[1, 2], 2)),
                    paste0("narrow only, AUC = ", round(auc_sum[2, 2], 2)))

    ggplot(pred_all, aes(x = fpr, y = tpr, color = learner, group = interaction(i, learner))) +
      geom_line(size = 0.3) + 
      geom_abline(intercept = 0, slope = 1, linetype = "dashed") +
      theme_bw() +
      xlab("False positive rate") +
      ylab("True positive rate") +
      scale_color_manual(values = c("#B0C4DE", "#4682B4"), name = "Training dataset:", labels = rocl_labels) +
      theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
            axis.title.x = element_text(size=14), axis.title.y = element_text(size=14),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.title = element_text(size=12), legend.text = element_text(size=12),
            legend.position = c(1, 0), legend.justification = c(1.02, -0.2))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-63-1.svg)

    ggsave(filename = file.path(outdir, "fig5i_transfer_genet_roc_curves.pdf"), width = 4, height = 4)

Supplementary Figgure 5
=======================

Figure S5a
----------

    # Same dataset as loaded by load_Moscalev2015_stress_responce_genes function, but also retrieving LFC
    stress_path = config$validation_datasets$Moskalev2015$data_dir
    de_files = list.files(stress_path, pattern = "csv")

    # Get DE genes
    df_stress = lapply(de_files, function(f) { de_res = read.csv(file.path(stress_path, f)) %>% select(-FBtr);
                                        names(de_res) = c("gene_name", "fdr", "lfc")
                                        # single transcript per gene (max change)
                                        de_res %>% group_by(gene_name) %>%
                                          mutate(max_lfc = max(abs(lfc))) %>%
                                          filter(abs(lfc) == max_lfc) %>%
                                          select(-max_lfc) %>%
                                          mutate(dataset = gsub(".csv", "", f), 
                                                          type = substr(f, 1, 3)) %>% 
                                          filter(gene_name != "" & gene_name != "</td") %>% unique()   
                                        }) %>% bind_rows




    df_stress_sum = df_stress %>% 
      mutate(change = ifelse(lfc > 0, "up", "down")) %>% 
      select(gene_name, change) %>% unique() %>%
      group_by(gene_name) %>%
      summarize(change = paste0(sort(unique(change)), collapse = "-"))

    # 4931 genes down, 559 up, 330 up-down
    #df_stress_sum %>% group_by(change) %>% tally()
    df = merge(df_stress_sum, mt, by = "gene_name", all.y = T)

    df$change[is.na(df$change)] = "non-DE"

    df$change = factor(df$change, levels = c("non-DE", "down", "up", "down-up"))

    labels = df %>% group_by(change) %>% summarize(n = n()) %>% mutate(label = paste0(change, " (N=", n, ")")) %>%
      select(label) %>% unlist(use.names = F)

    ggplot(df, aes(x = resid_cv, fill = change, color = change)) + 
      geom_density(alpha = 0.2) +
      theme_bw() + 
      scale_fill_brewer(palette = "Set2", name = "", labels = labels) +
      scale_color_brewer(palette = "Set2", name = "", labels = labels) +
      #scale_color_manual(values = cbPalette, name = "") +
      xlab("Expression variation") + 
      ylab("Density") +
      theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
            axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.position = c(1, 1), legend.justification = c(1.1, 1.1),
            legend.background = element_rect(fill = "transparent"),
            legend.title = element_text(size=16), legend.text = element_text(size=16))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-64-1.svg)

    ggsave(filename = file.path(outdir, "figS5a_DE_variation_change.pdf"),  width = 6, height = 4)

Figure S5b
----------

    df_stress = load_Moscalev2015_stress_responce_genes()

    df = merge(df_stress, mt %>% select(-DE), by = "gene_name", all.y = TRUE) %>%
                select(gene_name, resid_cv, type, DE) %>%
      unique() %>%
      mutate(type = ifelse(is.na(DE), "non-DE", type),
             DE = ifelse(is.na(DE), "non-DE", "DE"))

    df$type = factor(df$type, levels = c("non-DE", "temperature", "radiation", "starvation", "fungi"))

    med = median(df[df$DE == "non-DE", "resid_cv"])

    comp = list(c( "temperature","non-DE"),
                c( "radiation","non-DE"),
                c( "starvation","non-DE"),
                c("fungi", "non-DE"))

    ggplot(df, aes(x = type, y = resid_cv, fill = DE)) + 
      geom_violin(alpha = 0.5) +
      geom_boxplot(width = 0.5, outlier.size = 0.1) + 
      stat_summary(fun.data = function(x) c(y = - 1.1, label = length(x)), 
                   geom = "text", size = 3) + 
      theme_bw() + 
      geom_hline(yintercept = med, color = "darkred", linetype = "dashed") + 
      scale_fill_manual(values = cbPalette[2:1], name = "") +
      ylab("Expression variation") + xlab("") +
      stat_compare_means(method = "wilcox.test", comparisons = comp) +
      theme(axis.text.y = element_text(size=12), axis.text.x = element_text(size=12, angle = 30, hjust = 1), 
            axis.title.x = element_text(size=12), axis.title.y = element_text(size=12),
            plot.title = element_text(color="black", face="bold", size=18, hjust=0.5),
            legend.title = element_text(size=8), legend.text = element_text(size=8))

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-65-1.svg)

    ggsave(filename = file.path(outdir, "figS6b_stress_conditions_DE_variation.pdf"),  width = 6, height = 4)

Figure S5c-f
------------

    p1 = ggplot(mt, aes(y = num_dhs_conditions.prox, x = DE, fill = DE)) + 
      ylab("Number of conditions with DHS at TSS") 

    p2 = ggplot(mt, aes(y = num_dhs_any.dist + 1, x = DE, fill = DE)) + 
        scale_y_continuous(trans = "log2") +
      ylab("Number of TSS-distal DHSs") 

    p3 = ggplot(mt, aes(y = PI + 2, x = DE, fill = DE)) + 
      scale_y_continuous(trans = "log2") +
      ylab("PolII pausing index") 

    p4 = ggplot(mt, aes(y = num_miRNA + 1, x = DE, fill = DE)) + 
      scale_y_continuous(trans = "log2") +
      ylab("Number of miRNA motifs") 


    p1 = plot_format(p1, text_loc = -0.1)
    p2 = plot_format(p2, text_loc = -0.1)
    p3 = plot_format(p3, text_loc = -0.1)
    p4 = plot_format(p4, text_loc = -0.1)

    p = grid.arrange(p1, p2, p3, p4, ncol = 4)

    ## Warning: Computation failed in `stat_signif()`:
    ## missing value where TRUE/FALSE needed

    ## Warning: Computation failed in `stat_signif()`:
    ## missing value where TRUE/FALSE needed

    ## Warning: Computation failed in `stat_signif()`:
    ## missing value where TRUE/FALSE needed

    ## Warning: Computation failed in `stat_signif()`:
    ## missing value where TRUE/FALSE needed

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-66-1.svg)

    ggsave(filename = file.path(outdir, "figS5ghij_de_genet_features.pdf"), p, width = 12, height = 4)

Figure S5g-j
------------

    path = "/g/furlong/project/62_expression_variation//analysis/de_predictions/ExpressionAtlas_and_lab_meso/ExpressionAtlas_and_lab_meso_de_genes.csv"
    de_annot = read.csv(path)
    sel_ids1 = which(de_annot$cond_type == "genetic perturbation" & de_annot$Filter < 1)
    sel_ids2 = which(de_annot$cond_type == "environment/infection" & de_annot$Filter < 1)
    de_annot_sel = de_annot %>% filter(cond_type == "genetic perturbation" & Filter < 1 & cond_type == "genetic perturbation")

    path = "/g/furlong/project/62_expression_variation//analysis/de_predictions/ExpressionAtlas_and_lab_meso/DE_statistics_by_genes.csv"
    df = read.csv(path)
    df$n_de_genet = rowSums(df[ , sel_ids1])
    df$n_de_envir = rowSums(df[ , sel_ids2])
    df = merge(df, mt, by = "gene_id")

    df %<>% mutate(bin_de_genet = cut(n_de_genet, c(-1, 0, 5, 10, 15, max(df$n_de_genet)), 
                                      include.lowest = TRUE, dig.lab=10)) 

    df %<>% mutate(group = ifelse(n_de_genet > 10, ">10", "0-10"))
    df$group = factor(df$group, levels = c("0-10", ">10"))
    comp = list(c(">10", "0-10"))

    p1 = ggplot(df, aes(y = num_dhs_conditions.prox, x = group, fill = group)) + 
      ylab("# conditions with DHS at TSS") + facet_wrap(~shape) + xlab("# DE datasets")
    p2 = ggplot(df, aes(y = num_dhs_any.dist +1 , x = group, fill = group))  + 
      ylab("# TSS-distal DHSs") + scale_y_continuous(trans = "log2")+ facet_wrap(~shape) + xlab("# DE datasets")
    p3 = ggplot(df, aes(y = PI + 2, x = group, fill = group))  + 
      ylab("PolII pausing index") + scale_y_continuous(trans = "log2")+ facet_wrap(~shape) + xlab("# DE datasets")
    p4 = ggplot(df, aes(y = num_miRNA + 1, x = group, fill = group))  + 
      ylab("# miRNA motifs")  + scale_y_continuous(trans = "log2")+ facet_wrap(~shape) + xlab("# DE datasets")

    p1 = plot_format(p1, text_loc = -0.1)
    p2 = plot_format(p2, text_loc = -0.1)
    p3 = plot_format(p3, text_loc = -0.1)
    p4 = plot_format(p4, text_loc = -0.1)

    p = grid.arrange(p1, p2, p3, p4, ncol = 4)

![](Figures_Drosophila_files/figure-markdown_strict/unnamed-chunk-67-1.svg)

    ggsave(filename = file.path(outdir, "figS5ghij_de_genet_features.pdf"), p, width = 12, height = 4)

Cohen’s d:

    get_cohen_d_two_groups(df %>% filter(shape == "broad"), group_var = "group", compare_var = "num_dhs_conditions.prox", comparisons = c(">10", "0-10"))

    ## [1] -1.021806

    get_cohen_d_two_groups(df %>% filter(shape == "narrow"), group_var = "group", compare_var = "num_dhs_conditions.prox", comparisons = c(">10", "0-10"))

    ## [1] -0.4270814

    get_cohen_d_two_groups(df %>% filter(shape == "broad"), group_var = "group", compare_var = "num_dhs_any.dist", comparisons = c(">10", "0-10"))

    ## [1] 0.1395235

    get_cohen_d_two_groups(df %>% filter(shape == "narrow"), group_var = "group", compare_var = "num_dhs_any.dist", comparisons = c(">10", "0-10"))

    ## [1] -0.2837527

    get_cohen_d_two_groups(df %>% filter(shape == "broad"), group_var = "group", compare_var = "PI", comparisons = c(">10", "0-10"))

    ## [1] 0.1437636

    get_cohen_d_two_groups(df %>% filter(shape == "narrow"), group_var = "group", compare_var = "PI", comparisons = c(">10", "0-10"))

    ## [1] -0.07203014

    get_cohen_d_two_groups(df %>% filter(shape == "broad"), group_var = "group", compare_var = "num_miRNA", comparisons = c(">10", "0-10"))

    ## [1] 0.01516701

    get_cohen_d_two_groups(df %>% filter(shape == "narrow"), group_var = "group", compare_var = "num_miRNA", comparisons = c(">10", "0-10"))

    ## [1] -0.4734061

Session Info
============

    sessionInfo()

    ## R version 3.5.1 (2018-07-02)
    ## Platform: x86_64-pc-linux-gnu (64-bit)
    ## Running under: CentOS Linux 7 (Core)
    ## 
    ## Matrix products: default
    ## BLAS/LAPACK: /g/easybuild/x86_64/CentOS/7/haswell/software/OpenBLAS/0.2.20-GCC-6.4.0-2.28/lib/libopenblas_haswellp-r0.2.20.so
    ## 
    ## locale:
    ##  [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
    ##  [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
    ##  [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
    ##  [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
    ##  [9] LC_ADDRESS=C               LC_TELEPHONE=C            
    ## [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       
    ## 
    ## attached base packages:
    ## [1] parallel  stats4    stats     graphics  grDevices utils     datasets 
    ## [8] methods   base     
    ## 
    ## other attached packages:
    ##  [1] rtracklayer_1.40.4   GenomicRanges_1.34.0 GenomeInfoDb_1.18.2 
    ##  [4] IRanges_2.16.0       S4Vectors_0.20.1     BiocGenerics_0.28.0 
    ##  [7] bindrcpp_0.2.2       yaml_2.1.19          pheatmap_1.0.10     
    ## [10] psych_1.8.4          rcompanion_2.2.2     ggExtra_0.9         
    ## [13] gridExtra_2.3        RColorBrewer_1.1-2   ggpubr_0.1.7        
    ## [16] data.table_1.11.4    magrittr_1.5         forcats_0.3.0       
    ## [19] stringr_1.3.1        dplyr_0.7.5          purrr_0.2.5         
    ## [22] readr_1.1.1          tidyr_0.8.1          tibble_1.4.2        
    ## [25] ggplot2_3.0.0        tidyverse_1.2.1     
    ## 
    ## loaded via a namespace (and not attached):
    ##  [1] TH.data_1.0-8               colorspace_1.3-2           
    ##  [3] ggsignif_0.4.0              modeltools_0.2-21          
    ##  [5] rprojroot_1.3-2             XVector_0.22.0             
    ##  [7] rstudioapi_0.7              manipulate_1.0.1           
    ##  [9] mvtnorm_1.0-8               lubridate_1.7.4            
    ## [11] coin_1.2-2                  xml2_1.2.0                 
    ## [13] codetools_0.2-15            splines_3.5.1              
    ## [15] mnormt_1.5-5                knitr_1.20                 
    ## [17] jsonlite_1.5                Rsamtools_1.32.2           
    ## [19] broom_0.4.4                 shiny_1.1.0                
    ## [21] compiler_3.5.1              httr_1.3.1                 
    ## [23] backports_1.1.2             assertthat_0.2.1           
    ## [25] Matrix_1.2-14               lazyeval_0.2.1             
    ## [27] cli_1.0.0                   later_0.7.3                
    ## [29] htmltools_0.3.6             tools_3.5.1                
    ## [31] gtable_0.2.0                glue_1.2.0                 
    ## [33] GenomeInfoDbData_1.2.0      reshape2_1.4.3             
    ## [35] Rcpp_0.12.17                Biobase_2.42.0             
    ## [37] cellranger_1.1.0            Biostrings_2.48.0          
    ## [39] nlme_3.1-137                lmtest_0.9-36              
    ## [41] rvest_0.3.2                 mime_0.5                   
    ## [43] miniUI_0.1.1.1              XML_3.98-1.11              
    ## [45] MASS_7.3-50                 zlibbioc_1.28.0            
    ## [47] zoo_1.8-2                   scales_0.5.0               
    ## [49] hms_0.4.2                   promises_1.0.1             
    ## [51] SummarizedExperiment_1.12.0 sandwich_2.4-0             
    ## [53] expm_0.999-2                EMT_1.1                    
    ## [55] stringi_1.2.3               nortest_1.0-4              
    ## [57] boot_1.3-20                 BiocParallel_1.16.6        
    ## [59] matrixStats_0.53.1          rlang_0.2.1                
    ## [61] pkgconfig_2.0.1             bitops_1.0-6               
    ## [63] evaluate_0.10.1             lattice_0.20-35            
    ## [65] bindr_0.1.1                 GenomicAlignments_1.16.0   
    ## [67] labeling_0.3                tidyselect_0.2.4           
    ## [69] plyr_1.8.4                  R6_2.2.2                   
    ## [71] DescTools_0.99.28           multcompView_0.1-7         
    ## [73] multcomp_1.4-8              DelayedArray_0.8.0         
    ## [75] pillar_1.2.3                haven_1.1.1                
    ## [77] foreign_0.8-70              withr_2.1.2                
    ## [79] survival_2.42-3             RCurl_1.95-4.10            
    ## [81] modelr_0.1.2                crayon_1.3.4               
    ## [83] utf8_1.1.4                  rmarkdown_1.10             
    ## [85] grid_3.5.1                  readxl_1.1.0               
    ## [87] digest_0.6.15               xtable_1.8-2               
    ## [89] httpuv_1.4.4.1              munsell_0.5.0
