suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(ggrepel))
suppressPackageStartupMessages(library(RColorBrewer))
suppressPackageStartupMessages(library(gridExtra))
suppressPackageStartupMessages(library(ggExtra))
suppressPackageStartupMessages(library(pheatmap))
options(stringsAsFactors = FALSE)
source("/g/furlong/project/62_expression_variation/src/shared/utils.R")
config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir
human_path1 = file.path(project_folder, "/analysis/human_data")
human_path2 = "/g/scb2/zaugg/shaeiri/variation_prediction/table/final"

project_folder = config$project_parameters$project_dir

outdir = file.path(project_folder, "/Manuscript/Figures")

df1 = read.csv(file.path(human_path2, "all_variations2.csv"))
df2 = read.csv(file.path(human_path2, "share.csv"))
df3 = read.csv(file.path(human_path2, "prior.csv"))

######
df = read.delim2("/scratch/akamal/Neha_Project/DE_result_unverified.txt", sep = " ")


df = merge(df, df3, by.x = "geneSymbol", by.y = "Gene_Name", all.y = TRUE)
