suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(GenomicRanges))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(ranger))
suppressPackageStartupMessages(library(mlr))
suppressPackageStartupMessages(library(Boruta))
options(stringsAsFactors = FALSE)

source("/g/furlong/project/62_expression_variation/src/shared/utils.R")

config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir
outdir = file.path(project_folder, "/analysis/robustness_tests_and_validations/")
make_recursive_dir(outdir)


master_table = load_master_table()

# full dataset, predict variation

######## RF selected features ############################################

### final gene set

mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, t = "2_4h")

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/2_4h/predict_resid_cv_2_4h/results/1.rds")
imp_var1 = names(boruta_get_importance(boruta_var))

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/2_4h/predict_median_2_4h/results/1.rds")
imp_med1 = names(boruta_get_importance(boruta_med))

mt1 = mt %>% select(resid_cv, one_of(imp_var1))
mt2 = mt %>% select(median, one_of(imp_med1))

mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, t = "6_8h")

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/6_8h/predict_resid_cv_6_8h/results/1.rds")
imp_var2 = names(boruta_get_importance(boruta_var))

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/6_8h/predict_median_6_8h/results/1.rds")
imp_med2 = names(boruta_get_importance(boruta_med))

mt3 = mt %>% select(resid_cv, one_of(imp_var2))
mt4 = mt %>% select(median, one_of(imp_med2))

### with decr expr

mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, t = "2_4h", remove_decr_expr = FALSE)

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/2_4h/predict_resid_cv_2_4h_with_decr_expr/results/1.rds")
imp_var3 = names(boruta_get_importance(boruta_var))

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/2_4h/predict_median_2_4h_with_decr_expr/results/1.rds")
imp_med3 = names(boruta_get_importance(boruta_med))

mt5 = mt %>% select(resid_cv, one_of(imp_var3))
mt6 = mt %>% select(median, one_of(imp_med3))


mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, t = "6_8h", remove_decr_expr = FALSE)

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/6_8h/predict_resid_cv_6_8h_with_decr_expr/results/1.rds")
imp_var4 = names(boruta_get_importance(boruta_var))

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/6_8h/predict_median_6_8h_with_decr_expr/results/1.rds")
imp_med4 = names(boruta_get_importance(boruta_med))


mt7 = mt %>% select(resid_cv, one_of(imp_var4))
mt8 = mt %>% select(median, one_of(imp_med4))


mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, remove_decr_expr = FALSE)

boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/10_12h/predict_resid_cv_10_12h_with_decr_expr/results/1.rds")
imp_var5 = names(boruta_get_importance(boruta_var))

boruta_med = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/additional_validations/10_12h/predict_median_10_12h_with_decr_expr/results/1.rds")
imp_med5 = names(boruta_get_importance(boruta_med))

mt9 = mt %>% select(resid_cv, one_of(imp_var5))
mt10 = mt %>% select(median, one_of(imp_med5))


lrn = makeLearner("regr.ranger")

task1 = makeRegrTask(data = mt1, target = "resid_cv")
task2 = makeRegrTask(data = mt2, target = "median")
task3 = makeRegrTask(data = mt3, target = "resid_cv")
task4 = makeRegrTask(data = mt4, target = "median")
task5 = makeRegrTask(data = mt5, target = "resid_cv")
task6 = makeRegrTask(data = mt6, target = "median")
task7 = makeRegrTask(data = mt7, target = "resid_cv")
task8 = makeRegrTask(data = mt8, target = "median")
task9 = makeRegrTask(data = mt9, target = "resid_cv")
task10 = makeRegrTask(data = mt10, target = "median")


rdesc.outer = makeResampleDesc("CV", iters = 5)
ms = list(rsq, rmse, mse)

bmr_sel = benchmark(lrn, tasks = list(task1, task2, task3, task4, task5, task6, task7, task8, task9, task10), resampling = rdesc.outer, measures = ms, show.info = TRUE)

df_sel = data.frame(bmr_sel)
df_sel$exp = gsub("mt", "", df_sel$task.id)
df_sel %<>% arrange(as.numeric(exp))

df_sel$response = rep(rep(c("variation", "level"), each = 5), 5)

df_sel$time = c(rep(rep(c("2_4h", "6_8h"), each = 10), 2), rep(c("10_12h"), 10))

df_sel$dataset = c(rep("final dataset", 20), rep("with decreased expression", 30))


write.csv(df_sel, file.path(outdir, "rf_performance.other_time_points_and_filtering.csv"), row.names = FALSE)



