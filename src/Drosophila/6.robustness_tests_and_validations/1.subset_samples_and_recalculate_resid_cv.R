suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(gridExtra))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(data.table))
options(stringsAsFactors = FALSE)

source("/g/furlong/project/62_expression_variation/src/shared/utils.R")
config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir
outdir = file.path(project_folder, "analysis/robustness_tests_and_validations")
make_recursive_dir(outdir)

master_table = load_master_table()
mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, remove_names = FALSE)


expr_path = config$data$TagSeq$quantified_gene_expression$processed$no_replicates
expr = fread(expr_path, sep = ",")


# all samples 
lines = unique(expr$line_id)
# final subset of genes
expr_sel = expr %>% filter(gene_id %in% mt$gene_id & time == "10_12h")


######## split dataset in halves ###########################

# Randomly split dataset in two halves, calculate variation on each half, take correlation   

cor_df = data.frame(matrix(ncol = 3))
names(cor_df) = c("cor_pair", "cor_full1", "cor_full2")


for (i in 1:100) {
    
  # split samples in two halves
  n = floor(length(lines) / 2)
  
  rl1 = sample(lines, n)
  rl2 = lines[!lines %in% rl1][1:n]
  
  df1 = expr_sel %>% filter(line_id %in% rl1)
  df2 = expr_sel %>% filter(line_id %in% rl2)
  
  df_sum1 = get_resid_cv(df1)
  df_sum2 = get_resid_cv(df2)
  
  tmp = merge(df_sum1, df_sum2, by = "gene_id")
  
  cor1 = cor(tmp$resid_cv.x, tmp$resid_cv.y)
  
  tmp = merge(tmp, mt, by = "gene_id")
  
  cor2 = cor(tmp$resid_cv.x, tmp$resid_cv)
  cor3 = cor(tmp$resid_cv.y, tmp$resid_cv)
  
  cor_df[i, ] = c(cor1, cor2, cor3)
  
  print(c(cor1, cor2, cor3))

}

write.csv(cor_df, file.path(outdir, "/dataset_split_in_halves.correlations.csv"), row.names = FALSE)


######## Subset samples ###########################


cor_df = data.frame(matrix(ncol = 3))
names(cor_df) = c("num_samples", "iter", "cor")

j = 1

for(num_select in c(5, seq(10, 70, by = 10), 74)) {
  
  for (i in 1:100) {
    
    # select random samples to remove
    sel_samples = sample(lines, num_select)
    
    # remove selected samples 
    df = expr_sel %>% filter(line_id %in% sel_samples)
    
    # get variation
    df_sum = get_resid_cv(df)
    
    # find correlation with full dataset
    tmp = merge(df_sum, mt, by = "gene_id")
    cor = cor(tmp$resid_cv.x, tmp$resid_cv.y)
    
    # store results
    cor_df[j, ] = c(num_select, i, cor)
    print(c(num_select, i, cor))
    
    j = j + 1
    
  }
}

write.csv(cor_df, file.path(outdir, "/subset_samples.correlations.csv"), row.names = FALSE)

