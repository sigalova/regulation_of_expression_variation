---
title: "plots and data analysis"
output: html_document
---


```{r}
suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(RColorBrewer))
options(stringsAsFactors = FALSE)
```

```{r}
source("/g/furlong/project/62_expression_variation/src/shared/utils.R")
config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir

outdir = file.path(project_folder, "/analysis/robustness_tests_and_validations")

master_table = load_master_table()
master_table$time = factor(master_table$time, levels = times)
mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, remove_names = FALSE)
mt = add_complexity_indixes(mt)
```

# Subsetting samples

```{r}
path = file.path(outdir, "/subset_samples.correlations.csv")
cor_df = read.csv(path)

ggplot(cor_df, aes(x = factor(num_samples), y = cor)) + 
  geom_boxplot(size = 0.2) + 
  scale_y_continuous(breaks = seq(0.5, 1, by = 0.1), limits = c(0.5, 1), name = "Correlation with full dataset\n(75 samples)") + 
  theme_bw() + 
  geom_hline(yintercept = 0.9, color = "darkred", linetype = "dashed", size = 0.2) + 
  xlab("number of samples") +
  theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
        axis.title.x = element_text(size=14), axis.title.y = element_text(size=14))

ggsave(filename = file.path(outdir, "Subsetting_samples.pdf"), width = 10, height = 4)

```


### Splitting samples in halves


```{r, fig.width=8, fig.height=3}

path = file.path(outdir, "/dataset_split_in_halves.correlations.csv")
cor_df = read.csv(path)

cor_df %<>% gather(type, cor)

df_sum = cor_df %>% 
  group_by(type) %>% 
  summarize(mean_cor = mean(cor), sd_cor = sd(cor)) %>% 
  ungroup()

df_sum$labels = c("first half with full", "second half with full", "between halves of the dataset")
df_sum$labels = factor(df_sum$labels, levels = df_sum$labels)

ggplot(df_sum, aes(x = labels, y = mean_cor)) + 
  geom_bar(stat = "identity", position=position_dodge(.9)) +  
  geom_errorbar(aes(ymin = mean_cor - sd_cor, ymax = mean_cor + sd_cor), position=position_dodge(.9)) +
  theme_bw() +
  scale_y_continuous(limits = c(0, 1), breaks = seq(0.1, 1, by = 0.1)) +
  coord_flip() +
  xlab("")  + 
  ylab("Pearson correlation") +
 # scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=20), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))


ggsave(filename = file.path(outdir, "Splitting_samples_in_halves_v2.pdf"), width = 10, height = 5)
```


# Alternative variation measures

```{r}

# values

path1 = file.path(project_folder, "/analysis/robustness_tests_and_validations/alternative_residual_variation_measures.csv")
path2 = file.path(project_folder, "/analysis/robustness_tests_and_validations/VST_sd.csv")
var1 = read.csv(path1) %>% mutate(sd = log(sd), iqr = log(iqr), mad = log(mad), cv = log(cv))
var2 = read.csv(path2)

var = merge(var1, var2, by = "gene_id")
var = merge(var, mt %>% select(gene_id, resid_cv), by = "gene_id")
var %<>% gather(metrics, value, -gene_id, -median, -median_vst)

# model

path1 = file.path(project_folder, "/analysis/robustness_tests_and_validations/model_performance.alternative_residual_variation_measures.csv")
path2 = file.path(project_folder, "/analysis/robustness_tests_and_validations/model_performance.VST_comparison.csv")
res1 = read.csv(path1)
res2 = read.csv(path2)

res = rbind.data.frame(res1, res2 %>% filter(metrics != "median_vst")) 

```


### Dependence on the mean

```{r}

var_alt = var %>% filter(!metrics %in% c("cv", "resid_cv"))

var_alt$metrics = factor(var_alt$metrics, levels = c("mad", "iqr", "sd", "sd_vst", "resid_mad", "resid_iqr", "resid_sd"))

ggplot(var_alt, aes(x = log(median), y = value)) + 
  geom_point(size = 0.2) + 
  geom_smooth(method = "loess") + 
  theme_bw() +
  xlab("Median gene expression (log-scaled)") + 
  ylab("Expression variation (different metrics)") +
  facet_wrap(~metrics, ncol = 4, scales = "free")

ggsave(file.path(outdir, "different_variation_metrics_median_dependence.pdf"), width = 10, height = 5)
png(file.path(outdir, "different_variation_metrics_median_dependence.png"), width = 16, height = 8)
```

### Correlation 

```{r}
tmp = var %>% filter(metrics %in% c("resid_cv", "resid_sd", "resid_mad", "resid_iqr", "sd_vst")) %>% select(gene_id, metrics, value) %>% spread(metrics, value)

# pheatmap(tmp_cor, cluster_rows = FALSE, cluster_cols = FALSE, display_numbers = TRUE, fontsize = 16, legend = FALSE,
#          filename = file.path(outdir, "different_variation_metrics_corplot.pdf"), width = 4, height = 4,
#          color = colorRampPalette(brewer.pal(n = 5, name = "YlOrRd"))(5))


cor = as.matrix(round(cor(tmp[ ,2:6]), 2))
upper_tri = get_upper_tri(cor)

# Melt the correlation matrix
melted_cormat <- melt(upper_tri, na.rm = TRUE)

ggplot(melted_cormat, aes(Var2, Var1, fill = value))+
 geom_tile(color = "white")+
 scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
   midpoint = 0.5, limit = c(0,1), space = "Lab", 
    name="Pearson\nCorrelation") +
  theme_minimal()+ # minimal theme
 theme(axis.text.x = element_text(angle = 45, vjust = 1, 
    size = 12, hjust = 1))+
 coord_fixed() + 
  geom_text(aes(Var2, Var1, label = value), color = "black", size = 4) +
  theme(
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    panel.grid.major = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank(),
    axis.ticks = element_blank(),
    legend.justification = c(1, 0),
    legend.position = c(0.6, 0.7),
    legend.direction = "horizontal")+
    guides(fill = guide_colorbar(barwidth = 7, barheight = 1,
                  title.position = "top", title.hjust = 0.5))

ggsave(file.path(outdir, "different_variation_metrics_corplot.pdf"), width = 4, height = 4)


```

```{r, out.width = '100%'}
knitr::include_graphics(file.path(outdir, "different_variation_metrics_corplot.pdf"))
```

### Model performance

```{r}

df_sum = res %>% 
  group_by(metrics) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
  ungroup() 


ggplot(df_sum, aes(x = metrics, y = mean_rsq, fill = metrics)) + 
  geom_bar(stat = "identity", position=position_dodge(.9), color = "black") +  
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9)) +
  theme_bw() + 
  xlab("")  + 
  ylab("R^2, 5-fold cross validation") +
  scale_y_continuous(limits = c(-0.05, 0.6), breaks = seq(0.1, 0.6, by = 0.1)) +
  coord_flip() + 
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=20), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = "none")

ggsave(filename = file.path(outdir, "rf_performance_alternative_variation_measures.pdf"), width = 6, height = 3)
```

# Replicates

### RAL-375, 6-8h

```{r}
# correlation among replicates
path = file.path(outdir, "/RAL375_replicates_68h.vst_transformed_counts.csv")
df = read.csv(path)
df %<>% spread(line_id, norm_read_count)
cor = round(cor(df[ , 2:6]), 2)
```


```{r}
# correlation with final variation measure

path = file.path(outdir, "/RAL375_replicates_68h.variation_metrics.csv")
df = read.csv(path)
names(df)[3:6] = paste0(names(df)[3:6], "_rep")
df = merge(mt, df, by = "gene_id")

cor_val = with(df, round(cor(sd_vst, resid_cv, method = "spearman"), 2))

ggplot(df, aes(resid_cv, sd_vst)) + 
  geom_point(size = 0.3) + 
  annotate("text", x = -0.2, y = 1.2, label = paste0("Cor = ", cor_val, " (Spearman)"), size = 6) +
  geom_smooth(data = df, aes(resid_cv, sd_vst), inherit.aes = FALSE, method = "loess", se = FALSE) + 
  theme_bw() +
  ylab("expression variation (RAL-375 replicates, 6-8h)") + 
  xlab("expression variation (all lines, 10-12h)") +
  guides(colour = guide_legend(override.aes = list(size=8))) +
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
        axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=22, hjust=0.5),
        legend.position = c(0, 1), legend.justification = c(-0.1, 1.05))

ggsave(filename = file.path(outdir, "5replicates_ral375.correlation_with_full.pdf"),  width = 7, height = 7)


```

# Comparing time-points

```{r}
df = master_table %>% filter(gene_id %in% mt$gene_id) %>% select(gene_id, time, resid_cv_decrExprFilt_filtNA) %>% spread(time, resid_cv_decrExprFilt_filtNA) %>% na.omit()

cor = as.matrix(round(cor(df[ ,2:4]), 2))
upper_tri = get_upper_tri(cor)

# Melt the correlation matrix
melted_cormat <- melt(upper_tri, na.rm = TRUE)

ggplot(melted_cormat, aes(Var2, Var1, fill = value))+
 geom_tile(color = "white")+
 scale_fill_gradient2(low = "blue", high = "red", mid = "white", 
   midpoint = 0.5, limit = c(0,1), space = "Lab", 
    name="Pearson\nCorrelation") +
  theme_minimal()+ # minimal theme
 theme(axis.text.x = element_text(angle = 45, vjust = 1, 
    size = 12, hjust = 1))+
 coord_fixed() + 
  geom_text(aes(Var2, Var1, label = value), color = "black", size = 4) +
  theme(
    axis.title.x = element_blank(),
    axis.title.y = element_blank(),
    panel.grid.major = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank(),
    axis.ticks = element_blank(),
    legend.justification = c(1, 0),
    legend.position = c(0.6, 0.7),
    legend.direction = "horizontal")+
    guides(fill = guide_colorbar(barwidth = 7, barheight = 1,
                  title.position = "top", title.hjust = 0.5))

ggsave(file.path(outdir, "comparing_timepoints_corplot.pdf"), width = 4, height = 4)
```


```{r, out.width = '100%'}
knitr::include_graphics(file.path(outdir, "comparing_timepoints_corplot.pdf"))
```

# R^2 across time-points (with and without removing genes that decrease in expression)

```{r}

f1 = file.path(project_folder, "/analysis/robustness_tests_and_validations/rf_performance.other_time_points_and_filtering.csv")
f2 = file.path(project_folder, "/analysis/random_forests/rf_performance.full_and_selected_features.csv")  
res1 = read.csv(f1) %>% select(response, rsq, time, dataset)
res2 = read.csv(f2) %>% filter(set %in% c("expression variation", "expression level") & dataset == "selected features") %>% select(response, rsq) %>% 
  mutate(time = "10_12h", dataset =  "final dataset")

res = rbind.data.frame(res1, res2)

df_sum = res %>% 
  group_by(time, response, dataset) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
  ungroup() %>%
  mutate(time = factor(time, levels = rev(times))) %>%
  arrange(dataset, time, response)
  

ggplot(df_sum, aes(x = time, y = mean_rsq, fill = response)) + 
  geom_bar(stat = "identity", position=position_dodge(.9)) +  
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.7, size = 0.2) +
  theme_bw() + 
  facet_wrap(~dataset) +
  scale_alpha_manual(values = c(0.5, 1), guide = FALSE) + 
  xlab("")  + 
  ylab("R^2, 5-fold cross validation") +
 # scale_y_continuous(limits = c(-0.05, 0.55), breaks = seq(0.1, 0.5, by = 0.1)) +
  coord_flip() + 
  scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
  theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=18), 
        axis.title.x = element_text(size=22), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(project_folder, "/analysis/robustness_tests_and_validations/rf_performance.other_time_points_and_filtering.pdf"), width = 8, height = 4.5)




```

## Same for narrow

```{r}

f = file.path(project_folder, "/analysis/robustness_tests_and_validations/rf_performance_narrow.other_time_points_and_filtering.csv")
res = read.csv(f) %>% select(response, rsq, time, dataset)

df_sum = res %>% 
  group_by(time, response, dataset) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
  ungroup() %>%
  mutate(time = factor(time, levels = rev(times))) %>%
  arrange(dataset, time, response)
  

ggplot(df_sum, aes(x = time, y = mean_rsq, fill = response)) + 
  geom_bar(stat = "identity", position=position_dodge(.9)) +  
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.7, size = 0.2) +
  theme_bw() + 
  facet_wrap(~dataset) +
  scale_alpha_manual(values = c(0.5, 1), guide = FALSE) + 
  xlab("")  + 
  ylab("R^2, 5-fold cross validation") +
 # scale_y_continuous(limits = c(-0.05, 0.55), breaks = seq(0.1, 0.5, by = 0.1)) +
  coord_flip() + 
  scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
  theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=18), 
        axis.title.x = element_text(size=22), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(project_folder, "/analysis/robustness_tests_and_validations/rf_performance_narrow.other_time_points_and_filtering.pdf"), width = 8, height = 4.5)


```

