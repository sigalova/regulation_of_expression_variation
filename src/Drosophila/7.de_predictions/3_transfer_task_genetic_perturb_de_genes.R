suppressMessages(library(tidyverse))
suppressMessages(library(magrittr))
suppressMessages(library(DESeq2))
suppressMessages(library(data.table))
suppressMessages(library(gridExtra))
suppressMessages(library(ggrepel))
suppressMessages(library(ggpubr))
suppressMessages(library(mlr))
options(stringsAsFactors = FALSE)
source("/g/furlong/project/62_expression_variation/src/shared/utils.R")
config = load_config()

project_folder = config$project_parameters$project_dir

outdir = file.path(project_folder, "/analysis/de_predictions")
make_recursive_dir(outdir)

master_table = load_master_table()
mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = FALSE, remove_names = FALSE)

### DE genes - lab and expression atlas

path = "/g/furlong/project/62_expression_variation//analysis/de_predictions/ExpressionAtlas_and_lab_meso/DE_statistics_by_genes.csv"
df = read.csv(path)
df$n_de_genet = rowSums(df[ , sel_ids1])
df$n_de_envir = rowSums(df[ , sel_ids2])

mt = merge(df, mt, by = "gene_id")

# call DE if detected in more than 10 studies
mt$DE = mt$n_de_genet > 10

# Signisifcant features
boruta_var = readRDS(config$feature_selection$resid_cv)
imp_var = names(boruta_get_importance(boruta_var))


############# Non-overlapping test and train sets ##########################

# total number of DE genes
n_de = sum(mt$DE)

# trained models
pred_all = data.frame(matrix(ncol = 6, nrow = 0))
names(pred_all) = c("learner", "fpr", "tpr", "threshold", "i", "AUC")


for (i in 1:10) {
  
  # select half of DE genes for testing
  test_de = mt[mt$DE, "gene_name"]
  test_de = sample(test_de, n_de/2)
  
  # select same number of non-de genes for testing 
  test_non_de = mt[!mt$DE, "gene_name"]
  test_non_de = sample(test_non_de, n_de/2)
  
  # test dataset
  test_set = c(test_de, test_non_de)
  mt_test = mt %>% filter(gene_name %in% test_set) %>%
    select(DE, one_of(imp_var)) %>% mutate(bin_var = factor(ifelse(DE == FALSE, -1, 1))) %>% select(-DE)
  
  # prepare dataset for training (exclude test set genes, all or only narrow promoter genes)
  mt_train_narrow = mt %>% filter(shape == "narrow" & ! gene_name %in% test_set) %>% 
    select(resid_cv, one_of(imp_var)) %>%
    mutate(bin_var = cut(resid_cv, quantile(resid_cv, 0:3/3), include.lowest = TRUE, labels = c(-1, 0, 1))) %>% 
    filter(bin_var %in% c(-1, 1)) %>% select(-resid_cv) %>% mutate(bin_var = factor(bin_var))
  
  mt_train = mt %>% filter(! gene_name %in% test_set) %>% 
    select(resid_cv, one_of(imp_var)) %>%
    mutate(bin_var = cut(resid_cv, quantile(resid_cv, 0:3/3), include.lowest = TRUE, labels = c(-1, 0, 1))) %>% 
    filter(bin_var %in% c(-1, 1)) %>% select(-resid_cv) %>% mutate(bin_var = factor(bin_var))
  
  # train models
  
  lrn = makeLearner("classif.ranger", predict.type = "prob")
  ms = list(auc, mmce, acc)
  
  task_all = makeClassifTask(data = mt_train, target = "bin_var")
  task_narrow = makeClassifTask(data = mt_train_narrow, target = "bin_var")
  
  mod_all = train(lrn, task_all)
  mod_narrow = train(lrn, task_narrow)
  
  # prediction
  
  task_pred1 = makeClassifTask(data = mt_test, target = "bin_var")
  pred1 = predict(mod_narrow, task_pred1)
  
  task_pred2 = makeClassifTask(data = mt_test, target = "bin_var")
  pred2 = predict(mod_all, task_pred2)
  
  # performance
  AUC1 = round(performance(pred1, measures = auc), 2)
  AUC2 = round(performance(pred2, measures = auc), 2)
  
  print(AUC1)
  print(AUC2)
  
  # ROC curve data
  rocl = generateThreshVsPerfData(list(narrow =pred1, all = pred2), list(fpr, tpr), aggregate = TRUE)
  
  # write data
  df = rocl$data
  df$i = i
  df$AUC = rep(c(AUC1, AUC2), each = 100)
  
  pred_all = rbind.data.frame(pred_all, df)
  
}


write.csv(pred_all, file.path(outdir, "transfer_learning_performance.expr_atlas_and_lab_genet.csv"), row.names = FALSE)

