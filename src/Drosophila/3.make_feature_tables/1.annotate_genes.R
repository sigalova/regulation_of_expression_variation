####################################################################################
######################### Setup ####################################################
####################################################################################

suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(rtracklayer))
suppressPackageStartupMessages(library(GenomicRanges))
suppressPackageStartupMessages(library(data.table))
options(stringsAsFactors = FALSE)

source("/g/furlong/project/62_expression_variation/src/shared/utils.R")

config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
name2id = load_name2id() # list of FBIDs and corresponding gene symbols from flybase v6.13
project_folder = config$project_parameters$project_dir

####################################################################################
######################### Local parameters #########################################
####################################################################################

# Radius from TSS to calculate gene density
prox_dens_thres = 1000 # distance to tss to consider dhs 'TSS-proximal'
dist_dens_thres = 20000 # distance to tss to calculate gene density

# GC to calculate: 'gene' or 'gene_exon_transcript'  
gc_opt = "gene" # only GC-content for the whole gene, the other option is not active now

# UTR length computing: 'absolute' or 'relative' 3'UTR length
utr3_type = "relative" # 3'UTR length devided by gene length

# formatting
logicals2int = TRUE # 0/1 encoding for logicals

# Either include all genes or only those for which all features are available. In first case: NAs for missing values
include_all_genes = TRUE

####################################################################################
######################### load additional data #####################################
####################################################################################

# GFF v6.13 with gene annotations
genes = load_genes() 

# In-house insulation score, WE 2-4h
#ins_score = load_ins_score(time = "6_8h") # old version
ins_score_2_4h = read_IS_raw("HiC_WE_2-4h_combined")
ins_score_6_8h = read_IS_raw("HiC_WE_6-8h_combined")

# In-house TADs
tad_borders_2_4h = read_boundaries("HiC_WE_2-4h_combined")
tads_2_4h = boundaries_to_domains(tad_borders_2_4h)

# High resolution TADs from Ramirez..Manke paper
tads_hight_res = load_tads(prefix = "Ramirez") 


####################################################################################
######################### Annotate genes ###########################################
####################################################################################

# Median expression and residual variation per time-point
# var_summary = load_var_summary(convert_id2name = FALSE) 
# Expression change
# expr_change = get_expression_change(var_summary)

#######################################
############ GENETICS #################
#######################################

# Variance decomposition - genetics (from Mattia)
var_decomp = load_variance_decomposition(remove_noise = FALSE)

# Cannavo_Table_S6_gene-QTL.csv
eQTL_genes = load_eQTL_genes()

# shape QTLS  (Schor et al)
# tss_QTL_genes = load_tss_QTL_genes()

# TSS vatiant index
tss_variants = load_tss_variants()

# 3'UTR variant index: number_of_variants * average_allele_frequency / vriants_peak_length - based on 3'TAg-seq data
utr3_variants = load_utr3_variants()


##############################################
## Gene properties and biological function ###
##############################################


# Gene length, chromosome, number of transcripts and exons
genes_prop = data.frame(gene_name = genes$gene_name,
                        gene_id = genes$gene_id,
                        start = start(genes),
                        end = end(genes),
                        strand = strand(genes),
                        chr = as.character(seqnames(genes)), 
                        length_nt = width(genes))

# Dummy variables for chromosomes, chr2L as reference level
genes_prop %<>% mutate(value = 1) %>% spread(chr, value) %>% mutate(chr2L = NULL)
genes_prop[is.na(genes_prop)] = 0

# Number of transcripts and exons
n_transcripts = associate_transcrits_with_genes(count = TRUE)
n_exons = associate_exons_with_genes(count = TRUE)

# If missing info on number transcripts or exons - asign to 1 
genes_prop = merge_df_list(list(genes_prop, n_transcripts, n_exons), merge_by = "gene_name", 
                           all_x = TRUE, fill_na_with = 1)


# GC-content - for different gene parts or gene body only
if (gc_opt == "gene") {
  
  gene_gc_cont = load_gc_cont_gene_body()
  
} else if (g_opt == "gene_exon_transcript") {
  
  gb_gc = load_gc_cont_gene_body()
  exons_gc = load_gc_cont_exons()
  introns_gc = load_gc_cont_introns()
  # conbine and fill missing values with gene body values
  gene_gc_cont = merge_df_list(list(gb_gc, exons_gc, introns_gc), merge_by = "gene_name", all_x = TRUE)
  gene_gc_cont[is.na(gene_gc_cont[ , "exon_gc"]) ,"exon_gc"] = gene_gc_cont[is.na(gene_gc_cont[ , "exon_gc"]) ,"gene_gc"]
  gene_gc_cont[is.na(gene_gc_cont[ , "intron_gc"]) ,"intron_gc"] = gene_gc_cont[is.na(gene_gc_cont[ , "intron_gc"]) ,"gene_gc"]
  
} else {
  
  stop("Wrong value for GC-content estimation")
  
}


# gene densities
genes_tss = promoters(genes, upstream = 1, downstream = 1)
genes_prox = promoters(genes, upstream = prox_dens_thres, downstream = prox_dens_thres)
genes_dist = promoters(genes, upstream = dist_dens_thres, downstream = dist_dens_thres)

dens_prox = countOverlaps(genes_prox, genes_tss)
dens_dist = countOverlaps(genes_dist, genes_tss)

gene_density = data.frame(gene_name = genes$gene_name, num_genes_prox = dens_prox, num_genes_dist = dens_dist)


# TFs
tfs_full_list = load_tf_list()

# Housekeeping genes
hk_genes = load_housekeeping_genes(genes)

# BDGP expression pattern
ubiq_genes = load_ubiq_genes()

# Data from Yad (unfertilized eggs 2-4 and 6-8h)
maternal_genes = load_maternal_in_house_genes()

# conservation with human
conserv_human = load_human_orthologs()

# pausing from Saunders
PI_saunders = load_pausing_index_saunders()

# Excluded data:

# List of genes paused in mesoderm (received from Vlad, to check the sources)
# paused_meso_genes = load_paused_genes_meso()
# Cell cycle genes
#cell_cycle_genes = load_cell_cycle_genes()
# Stress responce GO term (list of genes retrieved from flybase)
#stress_responce_genes = load_stress_responce_genes()

##############################################
########## TSS proximal ######################
##############################################

### SEQUENCE MOTIFS ###

# Ohler 2006 TSS motifs - one per gene or one per transcipt (scanned with FIMO, -100+50bp of TSS from GFF annotation)
tss_motifs_maj = load_tss_motifs(path = "config$data$Motif_occurencies$tss$Ohler_motifs$by_gene$uniform_bg", prefix = "ohler_maj.") 
tss_motifs_alt = load_tss_motifs(path = "config$data$Motif_occurencies$tss$Ohler_motifs$by_transcript$uniform_bg", prefix = "ohler_alt.") 
tss_motifs = merge(tss_motifs_maj, tss_motifs_alt, by = "gene_name")  
# alternative TSS == 1 if there is no motif on major TSS (8 motifs in total, 2:9 - major, 10:17 - alternative)
tss_motifs[ , 10:17] = ifelse(tss_motifs[, 10:17] > tss_motifs[, 2:9], 1, 0) 

# promoter shape. Values calculated: 1. shape: broad, narrow, minor_broad (if minor_broad == TRUE); 2. prsence of any broad TSS; 3. number of expressed TSS; 4. shape index of the most expressed TSS. 
cage_shape = get_gene_promoter_shape_stat()  

# GC-content of core promoter (-100/+50 of major TSS)
tss_gc_cont = load_gc_cont_tss()


##############################################
##############  3'UTR   ######################
##############################################

# 3'UTR length per time-point => take average
utr3_lengths = lapply(times, function(t) load_utr3_lengths(t = t))
utr3_lengths = Reduce(rbind.data.frame, utr3_lengths)

if(utr3_type == "relative") {
  utr3_lengths = merge(utr3_lengths, genes_prop, by = "gene_name")
  utr3_lengths %<>% mutate(utr3_length = utr3_length / length_nt) %>% select(gene_name, utr3_length, time)
}

utr3_lengths_av = utr3_lengths %>% group_by(gene_name) %>% summarize(utr3_length = mean(utr3_length))


# change in 3'UTR length
utr3_change = utr3_lengths %>% spread(time, utr3_length) %>% 
  mutate(utr3_l2fc_10vs2 = log2(`10_12h`/ `2_4h`),
         utr3_l2fc_10vs6 = log2(`10_12h`/ `6_8h`),
         utr3_l2fc_6vs2 = log2(`6_8h`/ `2_4h`)) %>% 
  select(gene_name, utr3_l2fc_10vs2, utr3_l2fc_10vs6, utr3_l2fc_6vs2)


# Pumilo and Smaug targets => ideally, to be extended. Tried with dataset form Stoiber et al 2015 - don't improve performance, confounded with expression level
rbp_targets = load_rbp_targets() 

# 3'UTR motifs from CISBP-RNA, MIRBASE and RBPDB - uniform background
cisbp_rna = load_utr3_motifs(path = "config$data$Motif_occurencies$utr3$CISBP_RNA$uniform_bg", prefix = "cisbp_rna.")
mirbase = load_utr3_motifs(path = "config$data$Motif_occurencies$utr3$MIRBASE$uniform_bg", prefix = "mirbase.")
#rbpdb = load_utr3_motifs(path = "config$data$Motif_occurencies$utr3$RBPDB$uniform_bg", prefix = "rbpdb.")

# count number of motifs
cisbp_rna$num_rbp = rowSums(cisbp_rna[ , grep("^cisbp_rna\\.", names(cisbp_rna))])
mirbase$num_miRNA = rowSums(mirbase[ , grep("^mirbase\\.", names(mirbase))])

#######################################
#### Chromatin ########################
#######################################

# nearest value of insulation score to gene's TSS - 2-4h and 6-8h
ins2gene_2_4h = get_nearest_score(genes, ins_score_2_4h, outputFeatureColumn = "ins_score.2_4h", targetFeatureName = "value")
ins2gene_6_8h = get_nearest_score(genes, ins_score_6_8h, outputFeatureColumn = "ins_score.6_8h", targetFeatureName = "value")

# distance to the nearest TAD border from gene's TSS - in-house and high-resolution from Ramirez et al
dist2tad1 = get_distance_to_border(genes, tads_2_4h, label = "dist_to_tad_border.2_4h") 
dist2tad2 = get_distance_to_border(genes, tads_hight_res, label = "dist_to_tad_border.ramirez") 

# asignment to TADs and TAD size - in-house and high-resolution from Ramirez et al
genes2tads1 = asign_genes_to_domains(genes, tads_2_4h, domain_id_column = "tad_id.2_4h") # asign genes to TADs by TSS coordinate
genes2tads1$tad_size.2_4h = width(genes2tads1)
genes2tads1 = data.frame(mcols(genes2tads1))

genes2tads2 = asign_genes_to_domains(genes, tads_hight_res, domain_id_column = "tad_id.ramirez") # asign genes to TADs by TSS coordinate
genes2tads2$tad_size.ramirez = width(genes2tads2)
genes2tads2 = data.frame(mcols(genes2tads2))

#domain_annot = data.frame(gene_name = genes2tads$gene_name, tad_id = genes2tads$tad_id, tad_size = width(genes2tads)) # for each gene - size of the corresponding TAD

####################################################################################
######################### Merge data ###############################################
####################################################################################

# 1. merge time specific data (merge by two columns: gene_name and time), using
#genes_df = merge(var_summary, utr3_lengths, by = c("gene_name", "time"), all.x = include_all_expressed_genes)

genes_df = merge_df_list(list(genes_prop, 
                              gene_gc_cont, 
                              tss_gc_cont,
                              gene_density,
                              utr3_lengths_av,
                              utr3_change, 
                              eQTL_genes, 
                              var_decomp, 
                              utr3_variants, 
                   #           tss_variants,
                              maternal_genes, 
                              hk_genes, 
                              ubiq_genes, 
                              rbp_targets, 
                              tfs_full_list, 
                              PI_saunders,
                              cage_shape, 
                              conserv_human,
                              ins2gene_2_4h, 
                              ins2gene_6_8h, 
                              dist2tad1, 
                              dist2tad2, 
                              genes2tads1, 
                              genes2tads2, 
                              tss_motifs, 
                              cisbp_rna, 
                              mirbase),
                             merge_by = "gene_name",
                             all_x = include_all_genes)

if(logicals2int) { genes_df = convert_logicals_to_numerics_in_df(genes_df)}

# info on missing features
full_info_genes = genes_df %>% na.omit() %>% select(gene_name) %>% unlist()
info_df = name2id
info_df$with_missing_features = 1
info_df$with_missing_features[info_df$gene_name %in% full_info_genes] = 0

####################################################################################
######################### Write Master Table #######################################
####################################################################################

outdir = file.path(project_folder, "data/feature_tables")
make_recursive_dir(outdir)
outf = file.path(outdir, "genes_v6.13_annotated.txt")
write.table(genes_df, outf, row.names = FALSE, quote = FALSE, sep = "\t")

# and info table
outf = file.path(outdir, "missing_data_info.txt")
write.table(info_df, outf, row.names = FALSE, quote = FALSE, sep = "\t")
