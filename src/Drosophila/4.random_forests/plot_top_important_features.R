suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(rtracklayer))
suppressPackageStartupMessages(library(data.table))
source("/g/furlong/project/62_expression_variation/src/pipelines/utils.R")

config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir
outdir = file.path(project_folder, "analysis/random_forests")

df_path = file.path(project_folder, "/analysis/random_forests/selected_features_importance_and_correlations.names_corrected.csv")
df = read.csv(df_path)
outdir = file.path(project_folder, "paper_plots")


df_meta_path = file.path(project_folder, "/analysis/random_forest_pipeline/features_meta.csv")
df_meta = read.csv(df_meta_path)

df = merge(df, df_meta, by.x = "var", by.y = "feature")

# features with correlation above 0.9 - only two pairs:
# shape and major_shape_ind
# keeping one member from each pair with higher importance, e.g. ins_score.2_4h & major_shape_ind

#df %<>% filter(!var %in% c("shape", "ins_score.6_8h"))
# df %<>% filter(!var %in% c("ins_score.6_8h"))

df %<>% filter(!var %in% c("shape", "ins_score.6_8h", "conserv_rank")) %>% arrange(-med_imp_var)

# top features to plot
n = 30
df_top = df[1:n, ]

# df_top$var = gsub("^modERN.|ohler_maj.", "", df_top$var)
# df_top$var = gsub("major_shape_ind", "shape index", df_top$var)
# df_top$var = gsub("length_nt", "gene length", df_top$var)
# df_top$var = gsub("num_dhs_any", "num. DHS", df_top$var)
# df_top$var = gsub(".prox", " (prox.)", df_top$var)
# df_top$var = gsub(".dist", " (dist.)", df_top$var)

df_top$var_corrected = factor(df_top$var_corrected, levels = df_top$var_corrected)

df_top %<>% 
  gather(responce, med_imp, med_imp_var:med_imp_med) %>%
  mutate(cor = ifelse(responce == "med_imp_var", cor_var, cor_med))


label_col = brewer.pal(6, "Set2")
label_col_vect = label_col[factor(df_top$feature_class)]

ggplot(df_top, aes(x = var_corrected, y = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() + 
  ylab("median feature importance") + xlab("") +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "explained variable", labels = c("level", "variation")) + 
  scale_size_continuous(name = "abs. correlation\nwith explained variable") + 
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "explained variable", labels = c("level", "variation")) + 
  scale_shape_manual(values = c(25, 24), name = "correlation with \nexplained variable", labels = c("negative", "positive")) +
  theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=22, angle = 90, color = label_col_vect), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
        legend.text=element_text(size=18), legend.title=element_text(size=18),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1))


ggsave(filename = file.path(outdir, "fig2_top30_features.pdf"), width = 18, height = 9)

n = 20
df_top = df[1:n, ]
df_top$var_corrected = factor(df_top$var_corrected, levels = rev(df_top$var_corrected))
df_top %<>% 
  gather(responce, med_imp, med_imp_var:med_imp_med) %>%
  mutate(cor = ifelse(responce == "med_imp_var", cor_var, cor_med))

ggplot(df_top, aes(y = var_corrected, x = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() + 
  xlab("median feature importance") + ylab("") +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "explained variable", labels = c("level", "variation")) + 
  scale_size_continuous(name = "abs. correlation\nwith explained variable") + 
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "explained variable", labels = c("level", "variation")) + 
  scale_shape_manual(values = c(25, 24), name = "correlation with \nexplained variable", labels = c("negative", "positive")) +
  theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=22), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=18),
        legend.text=element_text(size=18), legend.title=element_text(size=18),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1))

ggsave(filename = file.path(outdir, "fig2_top15_features.pdf"), width = 10, height = 12)
