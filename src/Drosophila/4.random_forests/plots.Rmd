---
title: "paper_plots"
output: html_document
---

```{r}
suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(RColorBrewer))
suppressPackageStartupMessages(library(ggrepel))
options(stringsAsFactors = FALSE)
```

```{r}
source("/g/furlong/project/62_expression_variation/src/shared/utils.R")
config = load_config() # json file with all paths to data
times = load_times() # time-points used for analysis
project_folder = config$project_parameters$project_dir

outdir = file.path(project_folder, "/analysis/random_forests")
make_recursive_dir(outdir)

master_table = load_master_table()
mt = preprocess_master_table(master_table, remove_median = FALSE, log_transf_median = TRUE, remove_names = FALSE)
mt = add_complexity_indixes(mt)
```

# RANDOM FOREST PERFORMANCE

## Selected features

```{r, fig.width=6, fig.height=3}

# R^2 for expression variation on full dataset
rf_res_path =  file.path(outdir, "rf_performance.full_and_selected_features.csv")  
res = read.csv(rf_res_path)
res_var = res %>% filter(dataset == "selected features")

df_sum = res_var %>% 
  group_by(set, response, dataset) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
  ungroup() 
df_sum$set = factor(df_sum$set, levels = c("permuted variation", "permuted level", "expression variation", "expression level"))

ggplot(df_sum, aes(x = set, y = mean_rsq, fill = response)) + 
  geom_bar(stat = "identity", position=position_dodge(.9)) +  
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.7, size = 0.2) +
  theme_bw() + 
  scale_alpha_manual(values = c(0.5, 1), guide = FALSE) + 
  xlab("")  + 
  ylab("R^2, 5-fold cross validation") +
  scale_y_continuous(limits = c(-0.05, 0.55), breaks = seq(0.1, 0.5, by = 0.1)) +
  coord_flip() + 
  scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
  theme(axis.text.y = element_text(size=22), axis.text.x = element_text(size=18), 
        axis.title.x = element_text(size=22), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(outdir, "rf_performance.pdf"), width = 6.5, height = 4.5)

```

## Full and selected features

```{r}
# R^2 for expression variation on full dataset
rf_res_path =  file.path(outdir, "rf_performance.full_and_selected_features.csv")  
res = read.csv(rf_res_path)

df_sum = res %>% 
  group_by(set, response, dataset) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>% 
  ungroup() 

df_sum$set = factor(df_sum$set, levels = c("permuted variation", "permuted level", "expression variation", "expression level"))
df_sum$dataset = factor(df_sum$dataset, levels = c("selected features", "all features"))

# for plot below
full_perf = df_sum %>% filter(set == "expression variation" & dataset == "selected features") %>% select(mean_rsq) %>% unlist()

ggplot(df_sum, aes(x = set, y = mean_rsq, fill = response, alpha = dataset)) + 
  geom_bar(stat = "identity", position=position_dodge(.9)) +  
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9)) +
  theme_classic() + 
  scale_alpha_manual(values = c(0.5, 1), guide = guide_legend(reverse = TRUE)) + 
  xlab("")  + 
  ylab("R^2, 5-fold cross validation") +
  scale_y_continuous(limits = c(-0.05, 0.6), breaks = seq(0.1, 0.6, by = 0.1)) +
  coord_flip() + 
  scale_fill_manual(values = c("sandybrown", "navy"), guide = FALSE) + 
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=15), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(outdir, "rf_performance_full_and_selected_features.pdf"), width = 6, height = 4)

```


# EXPRESSION LEVEL THRESHOLDS

```{r}

path =  file.path(outdir, "rf_performance.selected_features.expression_level_thresholds.csv")  
df = read.csv(path)


df_sum = df %>% 
  group_by(task.id) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes), min_expr = mean(min_expr)) %>% 
  ungroup()

#full_perf = df_sum %>% filter(min_expr == 0) %>% select(mean_rsq) %>% unlist()

ggplot(df_sum %>% filter(min_expr > 0), aes(x = min_expr, y = mean_rsq)) + 
  geom_hline(yintercept = full_perf, color = "darkred", linetype = "dashed") +
  geom_point() + 
  geom_line() + 
  theme_bw() +
  geom_text(aes(x = min_expr, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred") +
  ylim(c(0, 0.65)) + 
  xlab("Minimum gene expression level") +
  ylab("R^2 (5-fold CV)") +
  scale_x_continuous(trans='log2', breaks = c(5, 10, 50, 200, 500)) +
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9)) +
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=20), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(outdir, "rf_performance.expr_level_thresholds.pdf"), width = 7, height = 5)


```


# EXPRESSION LEVEL BINS

```{r}

path =  file.path(outdir, "rf_performance.selected_features.expression_level_bins.csv")  
df = read.csv(path)


df_sum = df %>% 
  group_by(bin_median, label_bin_median) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes)) %>% 
  ungroup() %>% 
  arrange(bin_median) %>%
  mutate(label_bin_median = factor(label_bin_median, levels = label_bin_median))

ggplot(df_sum, aes(x = label_bin_median, y = mean_rsq, group = 1)) + 
  geom_hline(yintercept = full_perf, color = "darkred", linetype = "dashed") +
  geom_point(width = 0.5) + 
  geom_line(width = 0.4) + 
  theme_bw() +
  geom_text(aes(x = label_bin_median, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred") +
  ylim(c(0.2, 0.68)) + 
  xlab("Quantiles by gene expression level") +
  ylab("R^2 (5-fold CV)") +
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.3, size = 0.2) +
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=18, angle = 45, hjust=0.95), 
        axis.title.x = element_text(size=20), axis.title.y = element_text(size=18),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(outdir, "rf_performance.expr_level_bins.pdf"), width = 10, height = 4)


```


# EXPRESSION CHANGE THRESHOLDS

```{r}

path =  file.path(outdir, "rf_performance.selected_features.expression_change_thresholds.csv")  
df = read.csv(path)


df_sum = df %>% 
  group_by(bin_abs_change, label_bin_abs_change) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes)) %>% 
  ungroup() %>% 
  arrange(bin_abs_change) %>%
  mutate(label_bin_abs_change = factor(label_bin_abs_change, levels = label_bin_abs_change))


ggplot(df_sum, aes(x = max_abs_change, y = mean_rsq)) + 
  geom_hline(yintercept = full_perf, color = "darkred", linetype = "dashed") +
  geom_point(width = 0.5) + 
  geom_line(width = 0.4) + 
  theme_bw() +
  geom_text(aes(x = max_abs_change, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred") +
  ylim(c(0, 0.65)) + 
  scale_x_continuous(trans = "log2", breaks = round(df_sum$max_abs_change, 1)) +
  xlab("Maximum absolute expression L2FC (10-12h vs. 6-8h)") +
  ylab("R^2 (5-fold CV)") +
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.3, size = 0.2) +
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(outdir, "rf_performance.expr_change_thresholds.pdf"), width = 7, height = 5)


```

# EXPRESSION CHANGE BINS

```{r}

path =  file.path(outdir, "rf_performance.selected_features.expression_change_bins.csv")  
df = read.csv(path)

df_sum = df %>% 
  group_by(bin_change, label_bin_change, med_var, sd_var) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes)) %>% 
  ungroup() %>% 
  arrange(bin_change) %>%
  mutate(label_bin_change = factor(label_bin_change, levels = label_bin_change))

ggplot(df_sum, aes(x = label_bin_change, y = mean_rsq, group = 1)) + 
  geom_hline(yintercept = full_perf, color = "darkred", linetype = "dashed") +
  geom_point() + 
  geom_line(width = 0.4) + 
  theme_bw() +
  geom_text(aes(x = label_bin_change, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred") +
  ylim(c(0, 0.7)) + 
  xlab("Quartiles by absolute expression L2FC (10-12h vs. 6-8h)") +
  ylab("R^2 (5-fold CV)") +
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.3, size = 0.2) +
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16, angle = 45), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))


ggsave(filename = file.path(outdir, "rf_performance.expr_change_bins.pdf"), width = 7, height = 5)


```


# TOP IMPORTANT FEATURES

TSS features highlighted  

```{r, fig.width=15, fig.height=10}
path = file.path(outdir, "significant_features_importance_and_correlations.csv")
df = read.csv(path)
df %<>% filter(!is.na(med_imp_var)) 
df %<>% filter(!var %in% c("conserv_rank", "ins_score.6_8h") )
df[is.na(df)] = 0.1 # pseudocount for non-significant features

# Top-30 features
df %<>% arrange(-med_imp_var) 
df_top = df[1:30, ]

df_top$full_name = factor(df_top$full_name, levels = df_top$name_corrected)


df_top %<>% 
  gather(responce, med_imp, med_imp_var:med_imp_med) %>%
  mutate(cor = ifelse(responce == "med_imp_var", cor_var, cor_med))

label_col_vect = ifelse(df_top$class == "TSS", "darkred", "black")

ggplot(df_top, aes(x = full_name, y = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() +
  ylab("Median feature importance") + 
  xlab("") +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "Explained variable", labels = c("level", "variation")) + 
  scale_size_continuous(name = "Abs. correlation\nwith explained variable") +
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "Explained variable", labels = c("level", "variation")) + 
  scale_shape_manual(values = c(25, 24), name = "Correlation with \nexplained variable", labels = c("negative", "positive")) +
  guides(colour = guide_legend(override.aes = list(size=8)),
         shape = guide_legend(override.aes = list(size=4, fill = "black"))) +
  theme(axis.text.y = element_text(size=20), axis.text.x = element_text(size=20, angle = 90, color = label_col_vect), 
        axis.title.x = element_text(size=20), axis.title.y = element_text(size=24),
        legend.text=element_text(size=20), legend.title=element_text(size=22),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1))

ggsave(filename = file.path(outdir, "rf_top_features.pdf"), width = 20, height = 10)

```

Color labels by feature classes


```{r, fig.width=15, fig.height=10}
path = file.path(outdir, "significant_features_importance_and_correlations.csv")

df = read.csv(path)
df %<>% filter(!is.na(med_imp_var)) 
df[is.na(df)] = 0.1 # pseudocount for non-significant features

# remove some redundant features
#df %<>% filter(!var %in% c("conserv_rank", "ins_score.6_8h") )
#df = df[!duplicated(df$full_name), ]


# Top-30 features
df %<>% arrange(-med_imp_var) 
df_top = df[1:30, ]

df_top$full_name = factor(df_top$full_name, levels = df_top$full_name)

df_top %<>% 
  arrange(-med_imp_var) %>%
  gather(responce, med_imp, med_imp_var:med_imp_med) %>%
  mutate(cor = ifelse(responce == "med_imp_var", cor_var, cor_med)) 

# set colors

#label_col = brewer.pal(length(unique(df$class)), "Paired")
#label_col = c("#1F78B4", "#E31A1C", "#33A02C", "#B2DF8A", "#A6CEE3", "#FB9A99", "#FDBF6F")
#label_col = c("#32488d", "#E69F00", "#006400", "#E31A1C", "#8B4513", "#CC79A7", "#999999")
label_col = c("#4682B4", "#BDB76B", "#006400", "#E31A1C", "#8B4513", "#CC79A7", "#999999")
#cbPalette[c(5, 1, 3, 2, 6:8)]
df$class = factor(df$class, levels = unique(df$class))
names(label_col) = unique(df$class)

label_col_vect = label_col[df_top$class]

ggplot(df_top, aes(x = full_name, y = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() +
  ylab("Median feature importance") + 
  xlab("") +
#  coord_flip() +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
  scale_size_continuous(name = "abs. cor") +
  scale_shape_manual(values = c(25, 24), name = "cor sign", labels = c("negative", "positive")) +
  guides(color = guide_legend(override.aes = list(size=8), order = 1),
         shape = guide_legend(override.aes = list(size=4, fill = "black"), order = 2)) +
  theme(axis.text.y = element_text(size=25), axis.text.x = element_text(size=25, angle = 90, color = label_col_vect, hjust=0.95,vjust=0.2), 
        axis.title.y = element_text(size=25), axis.title.x = element_text(size=25),
        legend.text=element_text(size=30), legend.title=element_text(size=30),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(0.95, 0.6))

ggsave(filename = file.path(outdir, "rf_top_features_col_by_class.pdf"), width = 25, height = 15)

```

# Fig 2A

```{r, fig.width=10, fig.height=15}
path = file.path(outdir, "significant_features_importance_and_correlations.csv")

df = read.csv(path)
df %<>% filter(!is.na(med_imp_var)) 
df[is.na(df)] = 0.1 # pseudocount for non-significant features

# remove some redundant features
#df %<>% filter(!var %in% c("conserv_rank", "ins_score.6_8h") )
#df = df[!duplicated(df$full_name), ]

# remove similar features

rl = c("promoter shape","insulation score (6-8h)", "TAD size(2-4h)", "gene conservation score","DHS time profile (prox)",
       "DHS tissue profile (prox)", "ubiquitous DHS (prox)", "number of TF peaks with motifs (prox)", "3'UTR LFC 10-12h vs. 2-4h")

df %<>% filter(!full_name %in% rl & df$var != "modERN.Trl.E8_16.prox")

# Top-50 features
df %<>% arrange(-med_imp_var) 
df_top = df[1:30, ] 

df_top$full_name = gsub("number of|num.", "#", df_top$full_name)
df_top$full_name = gsub("dme\\.", "", df_top$full_name)

df_top$full_name = factor(df_top$full_name, levels = rev(df_top$full_name))

df_top %<>% 
  gather(responce, med_imp, med_imp_var:med_imp_med) %>%
  mutate(cor = ifelse(responce == "med_imp_var", cor_var, cor_med)) 


# set colors

#label_col = brewer.pal(length(unique(df$class)), "Paired")
#label_col = c("#1F78B4", "#E31A1C", "#33A02C", "#B2DF8A", "#A6CEE3", "#FB9A99", "#FDBF6F")
#label_col = c("#32488d", "#E69F00", "#006400", "#E31A1C", "#8B4513", "#CC79A7", "#999999")
label_col = c("#4682B4", "#BDB76B", "#006400", "#E31A1C", "#8B4513", "#CC79A7", "#999999")
#cbPalette[c(5, 1, 3, 2, 6:8)]
df$class = factor(df$class, levels = unique(df$class))
names(label_col) = unique(df$class)

label_col_vect = label_col[rev(df_top$class)]


ggplot(df_top, aes(y = full_name, x = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() +
  xlab("Median feature importance") + 
  ylab("") +
#  coord_flip()  +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
  scale_size_continuous(name = "abs. cor", range = c(2, 10)) +
  scale_shape_manual(values = c(25, 24), name = "cor sign", labels = c("negative", "positive")) +
  guides(color = guide_legend(override.aes = list(size=8), order = 1),
         shape = guide_legend(override.aes = list(size=4, fill = "black"), order = 2)) +
  theme(axis.text.x = element_text(size=25), axis.text.y = element_text(size=25, color = label_col_vect, hjust=0.95,vjust=0.2), 
        axis.title.y = element_text(size=25), axis.title.x = element_text(size=30),
        legend.text=element_text(size=30), legend.title=element_text(size=30),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.justification = "top")
#        legend.position = c(0.95, 0.6))

ggsave(filename = file.path(outdir, "rf_top_features_col_by_class.vertical.eps"), width = 12, height = 20)

```



```{r}
df_top$cor_sign = ifelse(df_top$cor_var > 0, "Positively correlated with variation", "Negatively correlated with variation") 

df_top_pos = df_top %>% filter(cor_var >= 0)
df_top_neg = df_top %>% filter(cor_var < 0)

label_col_vect_pos = label_col[df_top_pos$class]
label_col_vect_neg = label_col[df_top_neg$class]

max_imp = max(df_top$med_imp)

p1 = ggplot(df_top_pos, aes(x = full_name, y = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() +
  ylab("Median feature importance") + 
  xlab("") +
  ylim(c(0, ceiling(max_imp))) +
  facet_wrap(~cor_sign) +
  #  coord_flip() +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
  scale_size_continuous(name = "abs. cor") +
  scale_shape_manual(values = c(25, 24), name = "cor sign", labels = c("negative", "positive")) +
  guides(color = guide_legend(override.aes = list(size=8), order = 1),
         shape = guide_legend(override.aes = list(size=4, fill = "black"), order = 2)) +
  theme(axis.text.y = element_text(size=25), axis.text.x = element_text(size=25, angle = 90, color = label_col_vect_pos, hjust=0.95,vjust=0.2), 
        axis.title.y = element_text(size=25), axis.title.x = element_text(size=25),
        legend.text=element_text(size=30), legend.title=element_text(size=30),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=0.5))  

ggsave(filename = file.path(outdir, "rf_top_features_col_by_class_pos_cor.pdf"), p1, width = 8, height = 10)  

p2 = ggplot(df_top_neg, aes(x = full_name, y = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() +
#  ylab("") +
  facet_wrap(~cor_sign) +  
  ylab("Median feature importance") + 
  ylim(c(0, ceiling(max_imp))) +
  xlab("") +
  #  coord_flip() +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
  scale_size_continuous(name = "abs. cor") +
  scale_shape_manual(values = c(25, 24), name = "cor sign", labels = c("negative", "positive")) +
  guides(color = guide_legend(override.aes = list(size=8), order = 1),
         shape = guide_legend(override.aes = list(size=4, fill = "black"), order = 2)) +
  theme(axis.text.y = element_text(size=25), axis.text.x = element_text(size=25, angle = 90, color = label_col_vect_neg, hjust=0.95,vjust=0.2), 
        axis.title.y = element_text(size=25), axis.title.x = element_text(size=25),
        legend.text=element_text(size=30), legend.title=element_text(size=30),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=0.5))  

#p = ggarrange(p1, p2, widths = c(0.5,2), heights = c(0.5, 1))

ggsave(filename = file.path(outdir, "rf_top_features_col_by_class_neg_cor.pdf"), p2, width = 24, height = 12)  
```


# Fig 2B

## RF BY FEATURE CLASSES

```{r}
path = file.path(outdir, "rf_performance_by_feature_groups.csv")
df = read.csv(path)

df_sum = df %>% group_by(set, responce) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq)) %>%
  arrange(mean_rsq)

var_levels =  unlist(df_sum[df_sum$responce == "variation", "set"])

df_sum$set = factor(df_sum$set, levels = var_levels)

df_sum$responce = factor(df_sum$responce, levels = c("variation", "level"))

#label_col_vect = rev(c("#32488d", "#E69F00", "#006400", "#8B4513",  "#E31A1C", "#CC79A7", "#999999"))
label_col_vect = rev(c("#4682B4", "#BDB76B", "#006400", "#8B4513", "#CC79A7", "#E31A1C", "#999999"))

#label_col_vect = label_col[rev(unique(df_sum$set))]

ggplot(df_sum, aes(x = set, y = mean_rsq, fill = responce)) + 
  geom_bar(stat = "identity", position=position_dodge(.9)) +  
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9)) +
  theme_classic() + 
  xlab("") + ylab("R^2 (5-fold cross-validation)") +
  coord_flip() + 
  scale_fill_manual(values = c("navy", "sandybrown"),  name = "explained variable",
                    guide=guide_legend(reverse=T)) + 
  theme(axis.text.y = element_text(size=25, color = label_col_vect), axis.text.x = element_text(size=20), 
        axis.title.x = element_text(size=25), axis.title.y = element_text(size=18),
        legend.text=element_text(size=25), legend.title=element_text(size=25),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.05, -0.25))

ggsave(filename = file.path(outdir, "rf_feature_subsets.pdf"), width = 11, height = 8)
```

# PROMOTER SHAPE THRESHOLDS

```{r}

path =  file.path(outdir, "rf_performance.selected_features.promoter_shape_thresholds.csv")  
df = read.csv(path)


df_sum = df %>% 
  group_by(task.id) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes), min_shape_ind = mean(min_shape_ind)) %>% 
  ungroup()

full_perf = df_sum %>% filter(min_shape_ind == -1) %>% select(mean_rsq) %>% unlist()

ggplot(df_sum, aes(x = min_shape_ind, y = mean_rsq)) + 
  geom_hline(yintercept = full_perf, color = "darkred", linetype = "dashed") +
  geom_point() + 
  geom_line() + 
  theme_bw() +
  geom_text(aes(x = min_shape_ind, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred") +
  ylim(c(0, 0.65)) + 
  xlab("Minimum promoter shape index") +
  ylab("R^2 (5-fold CV)") +
  geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9)) +
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=20), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))

ggsave(filename = file.path(outdir, "rf_performance.shape_index_thresholds.pdf"), width = 7, height = 5)


```

# EXPRESSION CHANGE BINS

```{r}

path =  file.path(outdir, "rf_performance.selected_features.promoter_shape_bins.csv")  
df = read.csv(path)

df_sum = df %>% 
  group_by(bin_shape, label_bin_shape, med_var, sd_var) %>% 
  summarize(mean_rsq = mean(rsq), sd_rsq = sd(rsq), num_genes = mean(num_genes)) %>% 
  ungroup() %>% 
  arrange(bin_shape) %>%
  mutate(label_bin_shape = factor(label_bin_shape, levels = label_bin_shape))

ggplot(df_sum, aes(x = label_bin_shape, y = mean_rsq, group = 1)) + 
  geom_point() + 
  geom_line() + 
  theme_bw() +
  geom_text(aes(x = label_bin_shape, y = mean_rsq + sd_rsq + 0.03, label = num_genes), size = 6, color = "darkred")+
  ylim(c(0, 0.6)) + 
  xlab("Quantiles by promoter shape index") +
  ylab("R^2 (5-fold CV)") +
    geom_errorbar(aes(ymin = mean_rsq - sd_rsq, ymax = mean_rsq + sd_rsq), position=position_dodge(.9), width = 0.3, size = 0.2) +
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16, angle = 45), 
        axis.title.x = element_text(size=18), axis.title.y = element_text(size=22),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1),
        legend.position = c(1, 0), legend.justification = c(1.1, -0.4))


ggsave(filename = file.path(outdir, "rf_performance.shape_bins.pdf"), width = 7, height = 5)


```



# Predicting promoter shape and variation in narrow promoters


```{r}
class_col = c("#4682B4", "#BDB76B", "#006400", "#8B4513", "#E31A1C", "#CC79A7", "#999999")
names(class_col) = c("TSS", "Gene body", "Gene type", "Gene context", "Distal regulators", "3'UTR", "Genetics")
```


```{r}
path = file.path(project_folder, "/analysis/random_forests/significant_features_importance_and_correlations.csv")
df = read.csv(path)

# Top-50 features
df %<>% arrange(-med_imp_narrow_var) 
df_top = df[1:50, ]

df_top$full_name = factor(df_top$full_name, levels = df_top$full_name)

df_top %<>% 
arrange(-med_imp_narrow_var) %>%
  gather(responce, med_imp, c(med_imp_narrow_var, med_imp_narrow_lev)) %>%
  mutate(cor = ifelse(responce == "med_imp_narrow_var", cor_var_narrow, cor_med_narrow)) 
df_top[is.na(df_top)] = 0
# set colors

label_col_vect = label_col[df_top$class]

p = ggplot(df_top, aes(x = full_name, y = med_imp, color = responce, fill = responce, size = abs(cor), shape = factor(sign(cor)))) + 
  geom_point() +
  theme_bw() +
  ylab("Median feature importance") + 
  xlab("") +
#  coord_flip() +
  scale_color_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation")) + 
  scale_fill_manual(values = c("sandybrown", "midnightblue"), name = "variable", labels = c("level", "variation"), guide = FALSE) + 
  scale_size_continuous(name = "abs. cor") +
  scale_shape_manual(values = c(25, 24), name = "cor sign", labels = c("negative", "positive")) +
  guides(color = guide_legend(override.aes = list(size=8), order = 1),
         shape = guide_legend(override.aes = list(size=4, fill = "black"), order = 2)) +
  theme(axis.text.y = element_text(size=25), axis.text.x = element_text(size=25, angle = 90, color = label_col_vect, hjust=0.95,vjust=0.2), 
        axis.title.y = element_text(size=25), axis.title.x = element_text(size=25),
        legend.text=element_text(size=30), legend.title=element_text(size=30),
        strip.text.x = element_text(size = 18), strip.text.y = element_text(size = 18),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1))

ggsave(filename = file.path(outdir, "rf_narrow_top_features_col_by_class.eps"), p, width = 26, height = 12)
```




```{r, fig.width=12, fig.height=12}

path = file.path(project_folder, "/analysis/random_forests/significant_features_importance_and_correlations.csv")
selected_features = read.csv(path)
df = selected_features 
#%>% filter(class %in% c("TSS", "3'UTR", "Distal regulators"))
# #df %<>% filter(!var %in% c("conserv_rank", "ins_score.6_8h") )
# 
# boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/predict_shape_ind/results/1.rds")
# imp_shape = boruta_get_importance(boruta_var)
# imp_shape = data.frame(var = names(imp_shape), med_imp_shape = imp_shape) 
# 
# boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/narrow.predict_resid_cv/results/1.rds")
# imp_var_narrow = boruta_get_importance(boruta_var)
# imp_var_narrow = data.frame(var = names(imp_var_narrow), med_imp_narrow_var = imp_var_narrow) 
# 
# boruta_var = readRDS("/g/furlong/project/62_expression_variation/analysis/feature_selection_with_boruta/broad.predict_resid_cv/results/1.rds")
# imp_var_broad = boruta_get_importance(boruta_var)
# imp_var_broad = data.frame(var = names(imp_var_broad), med_imp_broad_var = imp_var_broad) 
# 
# df = merge_df_list(list(selected_features, imp_shape, imp_var_narrow, imp_var_broad), all_x = TRUE, merge_by = "var")

df[is.na(df)] = 0
df %<>% mutate(delta_narrow = med_imp_narrow_var - med_imp_broad_var) %>% filter(!var %in% c("shape", "major_shape_ind")) %>%
  arrange(-med_imp_var) 

# for similar features (Trl at 2 time-points) - select the stronger one
df %<>% group_by(full_name) %>% mutate(max_imp = max(med_imp_var)) %>% filter(med_imp_var == max_imp)
#df$class = factor(df$class, levels = c("TSS", "3'UTR", "Distal regulators"))
#df_top = df[1:50, ]

df %<>% arrange(-med_imp_narrow_var)
df1 = df %>% filter(med_imp_narrow_var > 0)
df1 = df[1:50, ]

ggplot(df1, aes(x = med_imp_shape, y = med_imp_narrow_var, group = 1, label = full_name, size = med_imp_var)) + 
  geom_point(color = "black", alpha = 0.7) + 
  geom_text_repel(aes(color = class), size = 4) +
  scale_color_manual(values = cbPalette, name = "Feature class") +
  theme_bw() +
  scale_size_continuous(name = "Feature importance\n(full model)") +
  xlab("Importance for promoter shape prediction") +
  ylab("Importance for expression variation prediction\nin narrow promtoer genes") +
  theme(axis.text.y = element_text(size=14), axis.text.x = element_text(size=14), 
        axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 14), strip.text.y = element_text(size = 14),
        plot.title = element_text(color="black", face="bold", size=18, hjust=1))


ggsave(filename = file.path(outdir, "rf_performance_shape_vs_narrow_var_top30.pdf"), width = 12, height = 8)
  

```

