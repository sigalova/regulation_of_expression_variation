################################################################################
######################## SETUP #################################################
################################################################################

from snakemake.utils import min_version
import socket
import os
import yaml

# Enforce a minimum Snakemake version because of various features
min_version("4.7")

onstart:
    print("\n\n##########################################\n# STARTING EXPRESSION VARIATION PIPELINE #\n##########################################\n\n")
    print ("Running workflow for the following fly samples:\n" + '\n'.join(tagseq_samples))

onsuccess:
    print("\n\n###############################\n# WORKFLOW FINISHED, NO ERROR #\n###############################\n\n")

ruleDisplayMessage = """\n\n####### START EXECUTING RULE ##########\n"""

# Load modules
#if "cluster" in socket.gethostname():
#    shell.prefix("module load GCCcore/7.3.0 ncurses;")


"""
    Full collection of Taq-seq samples (254 files) are in the folder:
    /g/furlong/project/41_integration_CAGE_polyA/solexa/fastq/Tagseq/
    Some of tehm were discarded, same of them belong to 5 DGRP lines that have incomplete time points.

    List of the 228 "good" files used in the subsequent analysis:
    /g/furlong/project/41_integration_CAGE_polyA/solexa/fastq/Tagseq/studied_samples.txt
    /g/furlong/project/41_integration_CAGE_polyA/src/sh/LIMIX/studied_samples_Tagseq.txt

    Trimmomatic was called with parameters "HEADCROP:7 CROP:43".
    Initially different samples have different read lenghtRead, here length will uniformely be of 43 bp.

    HTSEQ-count was used to assign reads to the polyadenylation peaks. They are defined in the gff:
    v6.05:/g/furlong/project/41_integration_CAGE_polyA/data/peaks/Tagseq/peaks_paraclu_alltimepoints_peaks_to_genes_mappable_bases.gff
    v6.13:/g/furlong/project/55_polyA_sites_resource/data/peaks/reproducing_polyA_paper_peaks/peaks.coverage.drop.filtered.gene.allpeaks.1bp.resolution.gff

    These two GFF files differ a bit in format, so HTSEQ_COUNT rule would require slightly different input parameters:
    v6.05: gff_type = "-t Tagseq_peak"
    v6.13: gff_type = "-t polyA_site -i Parent"

"""


################################################################################
######################## VARIABLES #############################################
################################################################################

### READ PIPELINE CONFIG ###
PROJECT_DIR = config["par_general"]["root_dir"]
CODE_DIR    = config["par_general"]["outdir"]
SCRATCH_DIR = config["par_general"]["scratch_dir"]

# Directories to store final data and results (on tier 1)
DATA_DIR          = PROJECT_DIR + "/data/"
LOG_BENCHMARK_DIR = CODE_DIR + "/Logs_and_Benchmarks"
CONFIG_DIR        = PROJECT_DIR + "/config"

# Directories to store temporarypreprocessing data (on scratch)
FASTQ_DIR                        = SCRATCH_DIR + "/1.Fastq_processing"
ALIGNMENT_DIR                    = SCRATCH_DIR + "/2.Alignment"
EXPRESSION_QUANTIFICATION_DIR    = SCRATCH_DIR + "/3.Expression_quantification"
EXPRESSION_AGGREGATION_DIR       = DATA_DIR + "/1.tagseq_with_replicates"


### READ GLOBAL CONFIG FILE ###

# load yaml file with data project_paths
yaml_data = CONFIG_DIR + "/config_data.yaml"
with open(yaml_data, 'r') as stream:
    data = yaml.load(stream)

# Path to fastq files
TAQSEQ_INPUT_DIR = data["data"]["tagseq"]["samples_dir"]

# software
trimmomatic = data["software"]["trimmomatic"]
bwa = data["software"]["bwa"]
samtools = data["software"]["samtools"]

# Variables
list_of_samples = data["data"]["tagseq"]["list_of_samples"]["with_replicates"]
genome_build = data["project_parameters"]["genome"]["build"]
annotation_version = data["project_parameters"]["genome"]["version"]
genome_fasta = data["data"]["genome"]["fasta"]
polyA_mapping = data["data"]["tagseq"]["polyA_mapping"][annotation_version]
#remove_samples = config["samples"]["rm_samples_with_batch_effects"]

# read list of samples
with open(list_of_samples, 'r') as myfile:
    tagseq_samples = myfile.read().replace('.fastq.gz', '').replace('\n', ' ').split()

print(annotation_version)

################################################################################
######################## START OF THE PIPELINE #################################
################################################################################
localrules: ALL, AGGREGATE_EXPRESSION

rule ALL:
    input:
    #    FASTQ_TRIMMED = expand(FASTQ_DIR + "/{SAMPLE}.trimmed.fastq", SAMPLE = tagseq_samples),
    #    BAM = expand(ALIGNMENT_DIR + "/{SAMPLE}.bam", SAMPLE = tagseq_samples),
    #    HTSEQ_COUNT = expand(EXPRESSION_QUANTIFICATION_DIR + "/{SAMPLE}.simple_read_count.txt", SAMPLE = tagseq_samples),
        RAW_EXPRESSION = EXPRESSION_AGGREGATION_DIR + "/aggregated_gene_expression.raw_counts.all_timepoints." + annotation_version + ".csv",

################################################################################
################ GENERATE EXPRESSION DATA FROM 3'-TAGSEQ #######################
################################################################################

rule TRIMMOMATIC:
    input:
        fastq = TAQSEQ_INPUT_DIR + "/{sample}.fastq.gz"
    output:
        fastq_trimmed = temp(FASTQ_DIR + "/{sample}.trimmed.fastq")
    params:
        trimmomatic = config["executables"]["trimmomatic"],
        trimmomatic_1 = config["trimmomatic"]["part_1"],
        trimmomatic_2 = config["trimmomatic"]["part_2"]
    threads: 8
    message: "{ruleDisplayMessage}Trimming fastq files for sample {wildcards.sample}..."
    log: LOG_BENCHMARK_DIR + "/{sample}.trimming.log"
    shell: """
           java -jar {trimmomatic} {params.trimmomatic_1}  {input.fastq} {output.fastq_trimmed} {params.trimmomatic_2} 2> {log}
           """

# Assuming index is already in the directory with the reference sequence
rule BWA_ALIGN:
    input:
        fastq = rules.TRIMMOMATIC.output.fastq_trimmed,
        ref = genome_fasta
    output:
        sai = temp(ALIGNMENT_DIR + "/{sample}.sai"),
        sam = temp(ALIGNMENT_DIR + "/{sample}.sam"),
    params:
        aln = config["bwa"]["aln"],
        samse = config["bwa"]["samse"]
    threads: 8
    message: "{ruleDisplayMessage}Alignment for sample {wildcards.sample}..."
    log: LOG_BENCHMARK_DIR + "/{sample}.aln.log"
    shell: """
           {bwa} aln {params.aln} {input.ref} {input.fastq} > {output.sai} 2> {log};
           {bwa} samse {params.samse} {input.ref} {output.sai} {input.fastq} > {output.sam};
           """

rule SAMTOOLS:
    input:
        sam = rules.BWA_ALIGN.output.sam
    output:
        bam = ALIGNMENT_DIR + "/{sample}.bam",
        txt = ALIGNMENT_DIR + "/{sample}.txt"
    params:
        view = config["samtools"]["view"]
    threads: 1
    message: "{ruleDisplayMessage}Samtools for sample {wildcards.sample}..."
    shell: """
           {samtools} flagstat {input.sam} > {output.txt};
           {samtools} view {params.view} {input.sam} > {output.bam}
           """

# Sort polyA sites in the mapping file by chromosome and start coordinate
rule SORT_MAPPING_FILE:
    input: polyA_mapping
    output: EXPRESSION_QUANTIFICATION_DIR + "/polyA_mapping_sorted." + annotation_version + ".gff"
    threads: 1
    message: "{ruleDisplayMessage}Sort GFF file with polyA coordinates..."
    shell: "cat {input} | cut -f1-9 | sort -k1,1 -k4,4g > {output}"


rule HTSEQ_COUNT:
    input:
        bam = rules.SAMTOOLS.output.bam,
        mappable = rules.SORT_MAPPING_FILE.output
    output:
        expression = EXPRESSION_QUANTIFICATION_DIR + "/{sample}.simple_read_count.txt"
    params:
        general = config["htseq_count"]["general"],
        gff_type = config["htseq_count"]["gff_type"][annotation_version]
    threads: 1
    message: "{ruleDisplayMessage}Peaks quantification for sample {wildcards.sample}..."
    log: LOG_BENCHMARK_DIR + "/{sample}.htseq_count.log"
    shell: """
           module load HTSeq/0.9.1-foss-2016b-Python-2.7.12
           htseq-count {params.general} {params.gff_type} {input.bam} {input.mappable} |  \
           grep -v  "^[__.]" > {output}
           """


rule AGGREGATE_EXPRESSION:
    input: expand(EXPRESSION_QUANTIFICATION_DIR + "/{SAMPLE}.simple_read_count.txt", SAMPLE = tagseq_samples)
    output: EXPRESSION_AGGREGATION_DIR + "/aggregated_gene_expression.raw_counts.all_timepoints." + annotation_version + ".csv"
    params:
        data_dir = EXPRESSION_QUANTIFICATION_DIR,
        aggregate_expression = CODE_DIR  + "/executables/aggregate_expression_data.R"
    threads: 1
    message: "{ruleDisplayMessage}Aggregate expression data: all samples, all time-points..."
    shell: """ Rscript {params.aggregate_expression} {params.data_dir} {output} """
