#!/bin/bash

# run the script from the current directory to ensure it finds config file, change if needed
config=$(ls ../../config/config.yml)
projectdir=$(cat $config | yq .global.projectdir | sed -e 's/^"//' -e 's/"$//')

# path to genome fasta
genome_fasta=$(cat $config | yq .data.genome.dm6.fasta | sed -e 's/^"//' -e 's/"$//')

# output directory
outdir=${projectdir}/genome/by_chromosome/
mkdir -p $outdir

awk -F '>' '/^>/ {F = $outdir/$2".fasta"} {print > F}' $genome_fasta
