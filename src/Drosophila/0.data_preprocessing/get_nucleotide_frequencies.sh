#!/bin/bash

# run the script from the current directory to ensure it finds config file

config=$(ls ../../config/config_data.yaml)

# Read config file with yq tool (depends on jq)
# the last part of the code removes quotes

project_dir=$(cat $config | yq .project_parameters.project_dir | sed -e 's/^"//' -e 's/"$//')
outdir=${project_dir}/data/nucleotide_frequencies

#  Generate error when parent directories don't exist, and create the directory if it doesn't exist
[ -d $outdir ] || mkdir $outdir

genome_fasta=$(cat $config | yq .data.Genome.fasta | sed -e 's/^"//' -e 's/"$//')
genes_gff=$(cat $config | yq .data.Genome.gff_genes | sed -e 's/^"//' -e 's/"$//')
exons_gff=$(cat $config | yq .data.Genome.gff_exons | sed -e 's/^"//' -e 's/"$//')
introns_gff=$(cat $config | yq .data.Genome.gff_introns | sed -e 's/^"//' -e 's/"$//')

bedtools=$(cat $config | yq .software.bedtools | sed -e 's/^"//' -e 's/"$//')

# generate files with nucleotide nucleotide_frequencies
$bedtools nuc -fi $genome_fasta -bed $genes_gff > ${outdir}/nuc_freq_genes.gff
$bedtools nuc -fi $genome_fasta -bed $exons_gff > ${outdir}/nuc_freq_exons.gff
$bedtools nuc -fi $genome_fasta -bed $introns_gff > ${outdir}/nuc_freq_introns.gff
