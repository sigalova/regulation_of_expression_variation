assert_that(exists("Alek.dir"))
source(paste0(Alek.dir, "/src/R/plot_map_binned_functions.R"))


get_TADs <- function(files)
{
  d <- NULL
  for (i in seq_along(files))
  {
    f <- files[[i]]
    source <- names(files)[i]
    if (is.null(source) || source == "")
      source <- sub('^(.*/)?([^/]+).bed$', '\\2',f)

    gr <- rtracklayer::import(f)
    gr <- gr[grepl('^chr[234XY][LR]?$',seqnames(gr)), ]
    d <- data.frame(chrom = seqnames(gr), 
            start = start(gr), 
            end = end(gr), 
            source = source,
            color = if (grepl("_colors_", f) || grepl("Sexton_2012",f)) sapply(strsplit(gr$name, "_"), nth, 1) else NA,
            even = factor(rep(c("odd", "even"), length(gr))[1:length(gr)])) %>%
      arrange(chrom, start) %>%
      rbind(d, .)
  }
  d %>% arrange(chrom, start) %>% as.data.table
}


### TADs
message("[global] loading TAD calls...")
all_TADs <- get_TADs(list(
  # "Chromatin colors" = paste0(Alek.dir, "/data/hic_annotations/dm6/Filion_2010_colors_", genome, ".bed.gz"),
  # "Ramirez et al. 2017" =
  " " =
    paste0(Alek.dir, "/data/external_TAD_calls/Ramirez_2017_Kc167_domains_", genome, ".bed")
))


plt_TADcalls <- function(wh)
{
  tad.wh <- all_TADs[  all_TADs$chrom == as.character(seqnames(wh))[1]
             & all_TADs$start < end(wh)
             & all_TADs$end > start(wh),]
  tad.wh[, source := factor(tad.wh$source)]
  p20 <- ggplot(tad.wh[is.na(color)]) +
    aes(x = start, xend = end, y = source, yend = source, alpha=even) + 
    geom_segment(size=3.5) +
    guides(alpha = 'none') + 
    scale_y_discrete(drop = F) +
    scale_alpha_manual(values = c(0.7, 0.4))

  col <- list(BLACK = "#000000", BLUE = "#0072B2", GREEN = "#009E73", RED = "#D55E00", YELLOW = "#F0E442") 
  for (name in names(col))
    p20 <- p20 + geom_segment(data = tad.wh[color == name], size = 3.5, alpha = 0.7, color = col[[name]])

  tad.wh.Sexton <- tad.wh[!is.na(color) & !(color %in% names(col))]
  tad.wh.Sexton[, color := factor(color, c("Null", "Active", "PcG", "HP1_centromeric"))]
  levels(tad.wh.Sexton$color)[levels(tad.wh.Sexton$color) == "HP1_centromeric"] <- "HP1/Centromere"
  p20 <- p20 +
    geom_segment(aes(color = color), data = tad.wh.Sexton, size = 3.5, alpha = 0.7) +
    scale_color_manual(values = c("gray80", "firebrick3", "deepskyblue3", "darkolivegreen2"), drop = F)

  p21 <- p20 +
    theme(legend.key.size = unit(10, "pt")) +
    ylab(NULL) +
    guides(color = guide_legend(title = NULL))
  return(p21)
}
