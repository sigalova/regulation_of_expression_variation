#!/usr/bin/bash

groups_type="broad  narrow-low narrow-high"
gene_file="/g/furlong/project/62_expression_variation/data/genes_grouped.txt"
genes_grouped=${gene_file::-4}_grouped.bed

for gene_type in $groups_type
    do
      echo $gene_type
      # write in bed format, add group name at the end
      grep -F "$gene_type" $gene_file | awk -v OFS='\t' '{print $1, $2, $3, $4, $5, $6}' >> $genes_grouped
      echo "#"${gene_type} >> $genes_grouped
    done


computeMatrix=/g/funcgen/bin/computeMatrix-3.1.3
plotProfile=/g/funcgen/bin/plotProfile-3.1.3

bw_path=/g/furlong/sigalova/data/sequence_conservation_score/dm6.phyloP27way.bw
odir=/g/furlong/project/62_expression_variation/analysis/sequence_conservation



$computeMatrix scale-regions  -S $bw_path -R $genes_grouped -out ${odir}/gene_body_grouped_phyloP.tab.gz

$plotProfile  ${odir}/gene_body_grouped_phyloP.tab.gz -out ${odir}/gene_body_grouped_phyloP.png --legendLocation "upper-right" --averageType "median"  --plotType se --plotHeight 8 --plotWidth 13
