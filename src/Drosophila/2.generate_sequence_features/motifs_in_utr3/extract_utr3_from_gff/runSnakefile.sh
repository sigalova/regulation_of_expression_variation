#!/bin/bash

########################
# PATHS AND PARAMETERS #
########################

# All parameters and paths are defined here:
. "./params.sh"



########################
# RUN AUTOMATED SCRIPT #
########################
. "/g/furlong/project/62_expression_variation/src/shared/runSnakemakeWrapper.sh"
# "/g/scb/zaugg/zaugg_shared/scripts/Christian/src/Snakemake/runSnakemakeWrapper.sh"
