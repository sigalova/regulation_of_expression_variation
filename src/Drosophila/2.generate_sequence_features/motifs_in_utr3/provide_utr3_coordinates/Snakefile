
################################################################################
####################### Setup ##################################################
################################################################################

from snakemake.utils import min_version
import os
import socket
import yaml

# Enforce a minimum Snakemake version because of various features
min_version("4.7")

# Load modules - if not in the /g/funcgen/bin wrappers + GCCcore/5.4.0 and ncurses (needed for some technical reasons => Christian) + ? Java/1.8.0_112
if "cluster" in socket.gethostname():
    shell.prefix("module load GCCcore/5.4.0 ncurses;")


onstart:
    print("################### Starting the analysis....#######################\n\n")

onsuccess:
#   print("\n\n#####################\nProcessed bam files written to:\n{" + "\n".join("{}: {}".format(k, v) for k, v in chip_seq_processed_bam_files.items()) + "\n\n")
    print("\n\n################### Workflow finished, no error #######################\n\n")


# Make the output nicer and easier to follow
ruleDisplayMessage = """\n\n
                        ########################\n
                        # START EXECUTING RULE #\n
                        ########################\n"""


# Maximum number of cores per rule
threadsMax = 16

################################################################################
####################### Data and parameters ####################################
################################################################################


PROJECT_DIR = config["par_general"]["root_dir"]
PIPELINE_DIR = config["par_general"]["outdir"] # directory with this code

EXECUTABLES = PROJECT_DIR + "/src/shared/snakemake"
OUTDIR = PROJECT_DIR + "/data/sequence_features/utr3/utr3_coordinates_provided"
LOG_DIR = PIPELINE_DIR + "/logs"
CONFIG_DIR = PROJECT_DIR + "/config" # project-wide config with paths to data

# load yaml file with data project_paths
yaml_data = CONFIG_DIR + "/config_data.yaml"
with open(yaml_data, 'r') as stream:
    data = yaml.load(stream)

# software
BEDTOOLS = data["software"]["bedtools"]
FIMO = data["software"]["fimo"]
GET_BACKGROUND = data["software"]["fasta-get-markov"]

# data
utr3_annot = data["data"]["Genome"]["utr3_coordinates"]
genome_fasta = data["data"]["Genome"]["fasta"]
cisbp_rna = data["data"]["Motif_databases"]["CISBP_RNA"]
mirbase = data["data"]["Motif_databases"]["MIRBASE"]
rbpdb = data["data"]["Motif_databases"]["RBPDB"]

################################################################################
####################### Pipeline ###############################################
################################################################################

rule all:
    input:
        OUTDIR + "/utr3.v6.13_chromUCSF_by_gene.fasta",
        OUTDIR + "/FIMO/fimo_cisbp_rna_order0",
        OUTDIR + "/FIMO/fimo_mirbase_order0",
        OUTDIR + "/FIMO/fimo_rbpdb_order0"


rule get_fasta:
    input: utr3_annot
    output: OUTDIR + "/utr3.v6.13_chromUCSF_by_gene.fasta"
    params:
        getfasta_params = config["bedtools"]["getfasta_param"]
    message: "{ruleDisplayMessage}Get 3'UTR fasta..."
    log: LOG_DIR + "/utr3_fasta.log"
    threads: 1
    shell: "{BEDTOOLS} getfasta -fi {genome_fasta} -bed {input} -fo {output} {params.getfasta_params} &>> {log}"


rule fasta_get_markov:
    input: rules.get_fasta.output
    output: temp(OUTDIR + "/utr3_backgrounds.txt"),
    message: "{ruleDisplayMessage}Calculate genomic background..."
    threads: 1
    shell: "{GET_BACKGROUND} {input} {output} -m 0"


rule fimo_cisbp_rna:
    input:
        fasta = rules.get_fasta.output,
        bg_file = rules.fasta_get_markov.output
    output:
        o1 = OUTDIR + "/FIMO/fimo_cisbp_rna_order0",
        o2 = OUTDIR + "/FIMO/fimo_cisbp_rna_uniform"
    message: "{ruleDisplayMessage}Run fimo for CISBP-RNA motifs..."
    threads: 1
    shell:
        """
        {FIMO} --bgfile {input.bg_file} -oc {output.o1} -norc {cisbp_rna} {input.fasta};
        {FIMO} --bgfile --uniform-- -oc {output.o2} -norc {cisbp_rna} {input.fasta}
        """


rule fimo_mirbase:
    input:
        fasta = rules.get_fasta.output,
        bg_file = rules.fasta_get_markov.output
    output:
        o1 = OUTDIR + "/FIMO/fimo_mirbase_order0",
        o2 = OUTDIR + "/FIMO/fimo_mirbase_uniform"
    message: "{ruleDisplayMessage}Run fimo for MIRBASE motifs..."
    threads: 1
    shell:
        """
        {FIMO} --bgfile {input.bg_file} -oc {output.o1} -norc {mirbase} {input.fasta};
        {FIMO} --bgfile --uniform-- -oc {output.o2} -norc {mirbase} {input.fasta}
        """

rule fimo_rbpdb:
    input:
        fasta = rules.get_fasta.output,
        bg_file = rules.fasta_get_markov.output
    output:
        o1 = OUTDIR + "/FIMO/fimo_rbpdb_order0",
        o2 = OUTDIR + "/FIMO/fimo_rbpdb_uniform"
    message: "{ruleDisplayMessage}Run fimo for RBPDB motifs..."
    threads: 1
    shell:
        """
        {FIMO} --bgfile {input.bg_file} -oc {output.o1} -norc {rbpdb} {input.fasta};
        {FIMO} --bgfile --uniform-- -oc {output.o2} -norc {rbpdb} {input.fasta}
        """
