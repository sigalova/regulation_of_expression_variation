library(gridExtra)
library(ggplot2)
library(RColorBrewer)
library(MASS)
library(plyr)
library(stringr)
library(tidyr)
library(dplyr)
library(readr)
library(purrr)

# functions
se <- function(x) { sd(x)/sqrt(length(x)) }
give.n <- function(x){
      return(c(y = median(x)*1.05, label = length(x)))
}
mean.n <- function(x){
      return(c(y = median(x)*0.97, label = round(mean(x),2)))
}

data_summary <- function(x) {
      m <- mean(x)
      ymin <- m-sd(x)
      ymax <- m+sd(x)
      return(c(y=m,ymin=ymin,ymax=ymax))
}

data_summary_rep <- function(x) {
      m <- mean(x)
      ymin <- m-sd(x)
      ymax <- m+sd(x)
      return(c(ymin=ymin,ymax=ymax))
}


# set the working directory. All the data files and the construct info should be here.
setwd ("~/Documents/EMBL/protocols/promoter reporter/cell sorter/GFP_mCherry_03-06-2016/")
# get file names
all.files<-dir(pattern = "*Transfected.csv")

# extract sample info from filename (filed should be named: cellLine_construtID_replicate_transfected.csv (constructID example: 59 vs 58 vs 59mut)
kw <- as.data.frame(str_split (all.files,pattern = "[_]"))
kw <- t(as.matrix(kw))[,2:3] # here you only keep fields 2 and 3, loosing cell line identifier. If you are using different cell lines in the same experiment, you might want to keep this field
kw <- data.frame(cbind(kw,as.character(1:nrow(kw))), row.names = NULL)
colnames(kw) <- c("ID","Replicate","Sample")

# load data and add sample info
all.data <- map_df(all.files, read_csv, .id = "Sample")
all.data$Sample <- as.factor(all.data$Sample)
colnames(all.data)[2:3]<-c("sfGFP","mCherry")
all.data <- left_join(all.data,kw, by = "Sample")

# load construct information (see readme)
constructs_info <- read_csv("constructs.csv")
all.data <- left_join(all.data,constructs_info, by = "ID")
all.data$Promoter <- as.factor(all.data$Promoter)

# correct values with mock transfected samples, using the average of the bottom 0.05% as baseline
mock <- read.csv("S2_mock_Singlets.csv")
zero.GFP <- mean(subset (mock, X488.530_30.A <= quantile(X488.530_30.A,0.0005))$X488.530_30.A)
zero.Cherry <- mean(subset (mock, X561.610_20.A <= quantile(X561.610_20.A,0.0005))$X561.610_20.A)
# calculate ratios
all.data <- mutate(all.data, Ratio = (all.data$sfGFP-zero.GFP)/(all.data$mCherry-zero.Cherry))
all.data <- mutate(all.data, log2Ratio = log2(Ratio))


# Order factors. This is to define the order for plotting afterwards, and depends on the particular constructs and genotypes that you have in each experiment
#all.data$Promoter <- ordered (all.data$Promoter, levels=c("pasha","utr-pasha","wech","utr-wech","CG31436","CG16965","GNBP3","CG5724","CG16896","Rplll128","CG11210","CG16753","HSP","neg"))
all.data$Allele <- ordered (all.data$Allele, levels=c("Ref","Alt","Refmut","Altmut","Refmut2","Refmut12","Altmut2","Altmut12","-"))
all.data$Allele.fr <- ordered (all.data$Allele.fr, levels=c("Maj","Min","Majmut","Minmut","Majmut2","Majmut12","Minmut2","Minmut12","-"))
prom <- levels(all.data$Promoter)
#all.data$Treatment <- ordered (all.data$Treatment, levels=c("","Dox"))


# plot distributions of values for each channel independently
pdf("controlplots.pdf")
GFP.box= ggplot(all.data, aes(x=Allele, y=sfGFP))
GFP.density= ggplot(all.data, aes(x=sfGFP))
GFP.box + geom_boxplot(aes(fill = Allele)) + scale_y_log10() + facet_grid(Promoter ~ .)
GFP.box + geom_boxplot(aes(fill = Allele, linetype=Replicate)) + scale_y_log10() + facet_grid(Promoter ~ .)
GFP.density + geom_density (aes(color=Allele)) + scale_x_log10() + facet_grid (Promoter ~ .)
#same for cherry
GFP.box= ggplot(all.data, aes(x=Allele, y=mCherry))
GFP.density= ggplot(all.data, aes(x=mCherry))
GFP.box + geom_boxplot(aes(fill = Allele)) + scale_y_log10() + facet_grid(Promoter ~ .)
GFP.box + geom_boxplot(aes(fill = Allele, linetype=Replicate)) + scale_y_log10() + facet_grid(Promoter ~ .)
GFP.density + geom_density (aes(color=Allele)) + scale_x_log10() + facet_grid (Promoter ~ .)
GFP.density + geom_density (aes(color=Allele))  + coord_cartesian(xlim=c(0,5000))+ facet_grid (Promoter ~ .)
dev.off()


# pre-filter data: you can opt to filter out cells expressing low levels of the reference fluorophore (mCherry). 
# Store your aboslute threshold in th
th <- 0
all.data.filtered <- subset (all.data, mCherry > th)
# It may be wiser to filter out a fixed percetage (ie. bottom 10%) instead of using a fixed threshold
# Save the threshold quantile in fq
fq <- 0.1


# calculate summaries (mean, SD, median, MAD, n)
split_data<- group_by(all.data.filtered,Promoter,Allele,Allele.fr,Replicate)
split_data<- filter(split_data, mCherry > quantile(mCherry, probs=fq))
# summaries for linear ratios
summ_repl <- summarize (split_data, Mean_exp = mean(Ratio), SD_exp = sd(Ratio), Median_exp = median (Ratio), MAD_exp = mad(Ratio), Num = length(Ratio))
split_summ_repl <- group_by(summ_repl, Promoter, Allele,Allele.fr)
summ <- summarize (split_summ_repl,center=mean(Mean_exp),spr=sd(Mean_exp),noise_center=mean(SD_exp/Mean_exp),noise_spr=sd(SD_exp/Mean_exp), Num = sum(Num))
summ_2 <-summarize (split_summ_repl,center=mean(Median_exp),spr=sd(Median_exp),noise_center=mean(MAD_exp),noise_spr=sd(MAD_exp), Num = sum(Num))
# summaries for log2 transf. ratios
summ_repl_log2 <- summarize (split_data, Mean_exp = mean(log2Ratio,na.rm=TRUE), SD_exp = sd(log2Ratio,na.rm=TRUE), Median_exp = median (log2Ratio,na.rm=TRUE), MAD_exp = mad(log2Ratio,na.rm=TRUE), Num = length(log2Ratio))
split_summ_repl_log2 <- group_by(summ_repl_log2, Promoter, Allele,Allele.fr)
summ_log2 <- summarize (split_summ_repl_log2,center=mean(Mean_exp),spr=sd(Mean_exp),noise_center=mean(SD_exp),noise_spr=sd(SD_exp), Num = sum(Num))
summ_2_log2 <-summarize (split_summ_repl_log2,center=mean(Median_exp),spr=sd(Median_exp),noise_center=mean(MAD_exp),noise_spr=sd(MAD_exp), Num = sum(Num))
# summaries for ln transf. ratios
summ_repl_log <- summarize (split_data, Mean_exp = mean(log(Ratio),na.rm=TRUE), SD_exp = sd(log(Ratio),na.rm=TRUE), Median_exp = median (log(Ratio),na.rm=TRUE), MAD_exp = mad(log(Ratio),na.rm=TRUE), Num = length(Ratio))
split_summ_repl_log <- group_by(summ_repl_log, Promoter, Allele,Allele.fr)
summ_log <- summarize (split_summ_repl_log, center=mean(Mean_exp), spr=se(Mean_exp), noise_center=mean(SD_exp), noise_spr=se(SD_exp), Num = sum(Num))
summ_2_log <-summarize (split_summ_repl_log,center=mean(Median_exp), spr=se(Median_exp), noise_center=mean(MAD_exp), noise_spr=se(MAD_exp), Num = sum(Num))
Maj.val <- subset (summ_log, Allele.fr=="Maj", select=c(Promoter,center,noise_center))
Maj.val_2 <- subset (summ_2_log, Allele.fr=="Maj", select=c(Promoter,center,noise_center))


# exploratory plots
# plot ratios for all data
pdf("allplots_all.pdf")
all.box= ggplot(all.data.filtered, aes(x=Allele, y=Ratio))
all.box + geom_boxplot(aes(fill = Allele)) + scale_y_log10() + coord_cartesian (ylim=c(0.0001,100))+ facet_grid (Promoter ~ .) 
all.box + geom_violin(aes(fill = Allele)) + scale_y_log10()  + coord_cartesian (ylim=c(0.0001,100)) + facet_grid (Promoter ~ .) 
all.box + geom_boxplot(aes(fill = Allele, linetype=Replicate)) + scale_y_log10() + coord_cartesian (ylim=c(0.0001,100))+ facet_grid (Promoter ~ .) 
all.box + geom_violin(aes(fill = Allele, linetype=Replicate)) + scale_y_log10()  + coord_cartesian (ylim=c(0.0001,100)) + facet_grid (Promoter ~ .)
all.density= ggplot(all.data.filtered, aes(x=Ratio))
all.density + geom_density (aes(color=Allele)) + scale_x_log10()  + coord_cartesian (xlim=c(0.0001,100)) + facet_grid (Promoter ~ .)
all.density + geom_density (aes(color=Allele, linetype=Replicate)) + scale_x_log10()  + coord_cartesian (xlim=c(0.0001,100)) + facet_grid (Promoter ~ .)
dev.off()

# plot gene-by-gene plots with independent scales and notches
pdf("genebygene_all_wviolin.pdf")
for (i in 1:length(prom)) {
      plotin<-subset(all.data.filtered, Promoter==prom[i])
      repl_plotin<-subset(summ_repl, Promoter==prom[i])
      sum_plotin<-subset(summ, Promoter==prom[i])
      gene.plot.dens=ggplot(plotin, aes(x=Ratio))
      print (gene.plot.dens + geom_density (aes(color=Allele)) + scale_x_log10() + facet_grid (. ~ Promoter))
      gene.boxplot= ggplot(plotin, aes(x=Allele, y=Ratio))
      print (gene.boxplot + geom_violin(aes(fill = Allele)) + scale_y_log10() + facet_grid (. ~ Promoter)) 
      print (gene.boxplot + geom_boxplot(aes(fill = Allele)) + scale_y_log10()  + facet_grid (. ~ Promoter) +
                   stat_summary(fun.data = give.n, geom = "text", fun.y = median))
      print (gene.boxplot + geom_boxplot(aes(fill = Allele,linetype=Replicate)) + scale_y_log10()  + facet_grid (. ~ Promoter) +
                   stat_summary(fun.data = give.n, geom = "text", fun.y = median))
      print (gene.boxplot + geom_boxplot(aes(fill = Allele), notch=TRUE) + scale_y_log10()  + theme_bw() + labs(title = prom[i]) + scale_fill_brewer(palette = "Set1"))
      print (gene.boxplot + geom_violin(aes(fill = Allele)) + scale_y_log10() + facet_grid (. ~ Promoter) + stat_summary(fun.data=data_summary, aes(linetype=Replicate), position = position_dodge(width=0.1)) + theme_bw() + scale_fill_brewer(palette = "Set1"))
      print (gene.boxplot + geom_violin(aes(fill = Allele)) + scale_y_log10() + facet_grid (. ~ Promoter) + stat_summary(data=repl_plotin, aes (x=Allele, y=Mean_exp), fun.data=data_summary) + theme_bw() + scale_fill_brewer(palette = "Set1"))
     
      print (gene.boxplot + geom_violin(aes(fill = Allele)) + scale_y_log10() + facet_grid (. ~ Promoter) + stat_summary(data=repl_plotin, aes (x=Allele, y=Mean_exp), fun.data=data_summary,geom="pointrange", size=0.4) + theme_bw() + scale_fill_brewer(palette = "Set1"))
      SD.bar <- ggplot (sum_plotin, aes(x=Allele, y=center, color=Allele))
      limits <- aes(ymax = center + spr, ymin=center - spr)
      print(SD.bar + geom_errorbar(stat="identity", limits, width=0.25) + theme_bw() + labs(title = prom[i]) + expand_limits(y=0) + scale_colour_brewer(palette = "Set1"))
      
      
}
dev.off()


# plots for showing to other people
prom<-prom[!(prom=="HSP" | prom=="SV40")]  # get rid of control promoters


pdf("genebygene_forpaper_allelefr_log2_all.pdf")
for (i in 1:length(prom)) {
      plotin<-subset(all.data.filtered, Promoter==prom[i])
      repl_plotin<-subset(summ_repl_log, Promoter==prom[i])
      sum_plotin<-subset(summ_log, Promoter==prom[i])
      sum2_plotin<-subset(summ_2_log, Promoter==prom[i])
      plotin$log2Ratio <- plotin$log2Ratio - sum2_plotin$center[sum2_plotin$Allele.fr=="Maj"]
      sum_plotin$center <- sum_plotin$center - sum2_plotin$center[sum2_plotin$Allele.fr=="Maj"]
      sum2_plotin$center <- sum2_plotin$center - sum2_plotin$center[sum2_plotin$Allele.fr=="Maj"]
      gene.boxplot= ggplot(plotin, aes(x=Allele.fr, y=log2Ratio))
      print(gene.boxplot + geom_violin(aes(fill = Allele.fr)) + labs(title = prom[i], x = "Promoter variant", y = "Expression (log2 Fold-change)", fill="Allele") 
            + geom_errorbar(data=sum2_plotin, aes(x=Allele.fr, y=center, ymax=center, ymin=center), width = 0.55, linetype = "dashed") 
            + geom_errorbar(data=sum_plotin, aes(x=Allele.fr, y=center, ymax = center + spr, ymin=center - spr), stat="identity", width=0.4, size=0.5, color="grey") 
            + theme_bw() + scale_fill_brewer(palette = "Set1") + scale_y_continuous(breaks = pretty(plotin$log2Ratio,7), minor_breaks = NULL) + theme(title=element_text(size=20),axis.text=element_text(size=12),axis.title=element_text(size=16))) 
}      
dev.off()


pdf("genebygene_forpaper_-bottom10_log2_all.pdf")
for (i in 1:length(prom)) {
      plotin<-subset(split_data, Promoter==prom[i])
      repl_plotin<-subset(summ_repl_log, Promoter==prom[i])
      sum_plotin<-subset(summ_log, Promoter==prom[i])
      sum2_plotin<-subset(summ_2_log, Promoter==prom[i])
      plotin$log2Ratio <- plotin$log2Ratio - sum2_plotin$center[sum2_plotin$Allele.fr=="Maj"]
      sum_plotin$center <- sum_plotin$center - sum2_plotin$center[sum2_plotin$Allele.fr=="Maj"]
      sum2_plotin$center <- sum2_plotin$center - sum2_plotin$center[sum2_plotin$Allele.fr=="Maj"]
      gene.boxplot= ggplot(plotin, aes(x=Allele.fr, y=log2Ratio))
      print(gene.boxplot + geom_violin(aes(fill = Allele.fr)) + labs(title = prom[i], x = "Promoter variant", y = "Expression (log2 Fold-change)", fill="Allele") 
            + geom_errorbar(data=sum2_plotin, aes(x=Allele.fr, y=center, ymax=center, ymin=center), width = 0.55, linetype = "dashed") 
            + geom_errorbar(data=sum_plotin, aes(x=Allele.fr, y=center, ymax = center + spr, ymin=center - spr), stat="identity", width=0.4, size=0.5, color="grey") 
            + theme_bw() + scale_fill_brewer(palette = "Set1") + scale_y_continuous(breaks = pretty(plotin$log2Ratio,7), minor_breaks = NULL) + theme(title=element_text(size=20),axis.text=element_text(size=12),axis.title=element_text(size=16))) 
}      
dev.off()

# plot noise
pdf("exp_noise_median_MAD_all.pdf")
for (i in 1:length(prom)) {
      sum2_plotin<-subset(summ_2_log, Promoter==prom[i])
      
      # percentage relative to Maj
      sum2_plotin$center<- 100*(exp(sum2_plotin$center - Maj.val_2$center[Maj.val_2$Promoter==prom[i]])-1)+100
      sum2_plotin$noise_center<- 100*(exp(sum2_plotin$noise_center - Maj.val_2$noise_center[Maj.val_2$Promoter==prom[i]])-1)+100
      x1 <- sum2_plotin$center[sum2_plotin$Allele.fr=="Maj"]
      x2 <- sum2_plotin$center[sum2_plotin$Allele.fr=="Majmut"]
      x3 <- sum2_plotin$center[sum2_plotin$Allele.fr=="Min"]
      y1 <- sum2_plotin$noise_center[sum2_plotin$Allele.fr=="Maj"]
      y2 <- sum2_plotin$noise_center[sum2_plotin$Allele.fr=="Majmut"]
      y3 <- sum2_plotin$noise_center[sum2_plotin$Allele.fr=="Min"]
      limitsx<- aes(xmax= center + 100*(exp(spr)-1), xmin=center - 100*(exp(spr)-1))
      limits <- aes(ymax = noise_center + 100*(exp(noise_spr)-1), ymin=noise_center - 100*(exp(noise_spr)-1))
      noise_plot <- ggplot(sum2_plotin, aes(x=center,y=noise_center,color=Allele.fr))
      noise_plot <- noise_plot + geom_point(stat="identity", size=3) + geom_errorbar(limits, width=0) + geom_errorbarh(limitsx, height=0) + labs(title = prom[i], x = "Expression level", y = "Expression noise", color="Allele") + theme_bw() + scale_color_brewer(palette = "Set1") + theme(title=element_text(size=20),axis.text=element_text(size=12),axis.title=element_text(size=16))
      print (noise_plot + geom_segment (aes(x=x1,y=y1,xend=x2,yend=y2), color='lightgreen', arrow = arrow(length = unit(0.05, "npc"))) +  geom_segment (aes(x=x2,y=y2,xend=x3,yend=y3), color='lightblue', arrow = arrow(length = unit(0.05, "npc"))))
}
dev.off()
            




            