# this is a copy of Christian's wrapper for snakemake on 04.02.2019
# original file location: /g/scb/zaugg/zaugg_shared/scripts/Christian/src/Snakemake/runSnakemakeWrapper.sh

# Changes:
# - set my path to conda: /g/furlong/sigalova/software/conda/bin/conda

# parameters that are currently fixed
customJobStatusScript=false

#######################
# AUTOMATIC FROM HERE #
#######################

if [ ! -n "$configFile" ]; then
  outputDir="output"
else
  # grep output directory automatically
  outputDir=`grep -P "\\"?outdir\\"?\s*:(.+)" $configFile | cut -d":" -f2 | sed 's/[\"]//g' | sed  -e 's/^[ \t]*//' | sed  -e 's/[,]*//g'`

  if [ ! -n "$outputDir" ]; then
    echo "Error: Could not find \"outdir\" parameter in the config file $configFile to automatically set the output directory correctly."
    exit 1
  fi

fi

if [ ! -n  "$shadowDir" ] ; then
  shadowDir=""
fi


if [ ! -n  "$skipPDFWorkflow" ] ; then
  skipPDFWorkflow=false
fi

if [ ! -n  "$skipSummaryAndDAG" ] ; then
  skipSummaryAndDAG=true
fi

if [ ! -n  "$mailType" ] ; then
  mailType="None"
fi

if [ ! -n  "$ignoreZeroSizedFiles" ] ; then
  ignoreZeroSizedFiles=true
fi

if [ ! -n  "$rerunIncomplete" ] ; then
  rerunIncomplete=true
fi

if [ ! -n  "$nolock" ] ; then
  nolock=true
fi

if [ ! -n  "$useSLURM" ] ; then
  useSLURM=false
fi


if [ ! -n  "$condaDir" ] ; then
  condaDir="/g/furlong/sigalova/software/conda"
fi


now="$(date +'%Y-%m-%d_%H-%M-%S')"

# Create a subdirectory where the stuff goes
logDirBasename="LOGS_AND_BENCHMARKS"
reportsDirBasename="LOGS_AND_BENCHMARKS"
outputDirLog="${outputDir}/$logDirBasename"
outputDirReports="${outputDir}/$reportsDirBasename"
inputDirBasename="0.Input/$now"
inputDir="${outputDir}/$inputDirBasename"
logParameters="${inputDir}/SnakemakeParams.log"
fileDAG="$outputDirReports/workflow.dag"
workflowGraphPDF="$outputDirReports/workflow.pdf"
workflowGraphSVG="$outputDirReports/workflow.svg"
stats="$outputDirLog/snakemake.stats"
stats2="$outputDirLog/snakemake.summaryDetailed"


if [ ! -d "$outputDirLog" ]; then
  mkdir -p $outputDirLog
  if [ $? -ne 0 ] ; then
    echo "Error: Could not create directory $outputDirLog."
    #exit 1
  fi
fi


if [ ! -d "$outputDirReports" ]; then
  mkdir -p $outputDirReports
  if [ $? -ne 0 ] ; then
    echo "Error: Could not create directory $outputDirLog."
    #exit 1
  fi
fi

if [ ! -d "$inputDir" ]; then
  mkdir -p $inputDir
  if [ $? -ne 0 ] ; then
    echo "Error: Could not create directory $outputDirLog."
    #exit 1
  fi
fi



echo "Automated parameter report, generated $now"              | tee    $logParameters
echo ""                                                        | tee -a $logParameters

echo "##############"                                          | tee -a $logParameters
echo "# PARAMETERS #"                                          | tee -a $logParameters
echo "##############"                                          | tee -a $logParameters

echo " DIRECTORIES AND INPUT"                                  | tee -a $logParameters
echo "  configFile                   = $configFile"            | tee -a $logParameters
echo "  snakefile                    = $snakefile"             | tee -a $logParameters

echo " PERFORMANCE OPTIONS"                                    | tee -a $logParameters
echo "  nCores                       = $nCores"                | tee -a $logParameters
echo "  nolock                       = $nolock"                | tee -a $logParameters
echo "  shadowDir                    = $shadowDir"             | tee -a $logParameters
echo "  skipSummaryAndDAG            = $skipSummaryAndDAG"     | tee -a $logParameters
echo "  dryRun                       = $dryRun"                | tee -a $logParameters

echo " FILES OPTIONS"
echo "  ignoreTemp                   = $ignoreTemp"            | tee -a $logParameters
echo "  touchOutputFiles             = $touchOutputFiles"      | tee -a $logParameters
echo "  ignoreZeroSizedFiles         = $ignoreZeroSizedFiles"  | tee -a $logParameters


echo " RULE OPTIONS"                                           | tee -a $logParameters
echo "  rerunIncomplete              = $rerunIncomplete"       | tee -a $logParameters
echo "  forceRerunAll                = $forceRerunAll"         | tee -a $logParameters
echo "  allowedRules                 = $allowedRules"          | tee -a $logParameters
echo "  runSpecificRule              = $runSpecificRule"       | tee -a $logParameters
echo "  runAlsoDownstreamRules       = $runAlsoDownstreamRules"| tee -a $logParameters
echo "  abortAfterFirstError         = $abortAfterFirstError"  | tee -a $logParameters

echo " CONDA AND SINGULARITY OPTIONS"                          | tee -a $logParameters
echo "  useConda                     = $useConda"              | tee -a $logParameters
echo "  conda-prefix                 = $condaDir"              | tee -a $logParameters
echo "  useSingularity               = $useSingularity"        | tee -a $logParameters
echo "  singularityPrefix            = $singularityPrefix"     | tee -a $logParameters
echo "  singularityArgs              = $singularityArgs"       | tee -a $logParameters

echo " DEVELOPMENT OPTIONS"                                    | tee -a $logParameters
echo "  useVerbose                   = $useVerbose"            | tee -a $logParameters

echo "  otherOptions                 = $otherOptions"          | tee -a $logParameters
echo "  runCustomCommand             = $runCustomCommand"      | tee -a $logParameters
echo "  workflowGraphFileType        = $workflowGraphFileType" | tee -a $logParameters

echo " CLUSTER OPTIONS"                                        | tee -a $logParameters
echo "  submitToCluster              = $submitToCluster"       | tee -a $logParameters
echo "  useSLURM                     = $useSLURM"              | tee -a $logParameters
echo "  clusterConfig                = $clusterConfig"         | tee -a $logParameters
echo "  maxJobsCluster               = $maxJobsCluster"        | tee -a $logParameters
echo "  maxNoRestartJobsUponFailure  = $maxRestartsPerJob"     | tee -a $logParameters
echo "  mailType                     = $mailType"              | tee -a $logParameters
echo "  customJobStatusScript        = $customJobStatusScript" | tee -a $logParameters



# Copy the configuration files etc to the input folder
if [ -n "$configFile" ]; then
  cp $configFile $inputDir
fi

cp $snakefile $inputDir

if [ -n "$clusterConfig" ]; then
  if [ "$submitToCluster" = true ] ; then
    cp $clusterConfig $inputDir
  fi
fi

configfileDirective=""
verboseDirective=""
tempDirective=""
printShellDirective=""
clusterDirective=""
forceRerunDirective=""
dryRunDirective=""
touchDirective=""
allowedRulesDirective=""
runSpecificRuleDirective=""
maxRestartDirective=""
condaDirective=""
keepGoingDirective="--keep-going"
nolockDirective=""
rerunIncompleteDirective=""
printShellCommands=false
otherOptionsDirective=""
shadowDirDirective=""
singularityDirective=""


if [ "$ignoreZeroSizedFiles" = false ] ; then

  # Check for 0-sized output files and abort if some are present
  echo "

  Check for zero-sized files (excluding files in $outputDirLog)...."

  command="find $outputDir -type f -size 0 ! -path '*$logDirBasename*' ! -path '*/.snakemake/*'"

  echo "Execute command \"$command\""


  nEmptyFiles=$(eval "$command | wc -l")
  if [ $nEmptyFiles -gt 0 ] ; then
    echo -e "\nWARNING\nThe following $nEmptyFiles zero-sized files have been found:"
    emptyFiles=$(eval "$command")
    echo $emptyFiles
    echo "Check them carefully and delete them to avoid potential issues"
    echo "Use '$command -delete' to delete them"
    exit 1
  fi

else
  echo "\n\nZero sized files have been chosen to ignore. Use this option if you are sure that all zero sized files are supposed to be zero. It might be that during the Snakemake run something went wrong."
fi


if [ ! -f "$snakefile" ];  then
  echo "Error: Snakefile $snakefile not found."
  exit 1
fi

if [ "$touchOutputFiles" = true ] ; then
   touchDirective="--touch"
elif [ "$touchOutputFiles" = false ] ; then
  :
else
  echo "Error: Parameter touchOutputFiles must be true or false, not \"$touchOutputFiles\""
  exit 1
fi

if [ -n "$otherOptions" ] ; then
   otherOptionsDirective="$otherOptions"
fi

if [ "$useSingularity" = true ] ; then
   singularityDirective="--use-singularity "

   echo "Singularity is to be used, forcing unloading of all modules now to avoid issues..."

   if [ "$(module --redirect -t list | grep -c snakemake)" -ge 1 ]; then
     echo "The Snakemake module is loaded. Due to a known problem with Singularity for the current version, the snakemake module shall not be used. Please install your local Snakemake version (e.g., via conda) first and unload all modules (module purge)"
     exit 1
     #module purge
     #module load snakemake
   else
     module purge
   fi


   if [ -n "$singularityPrefix" ]; then
      singularityDirective="$singularityDirective --singularity-prefix \"$singularityPrefix\""
   fi

   if [ -n "$singularityArgs" ]; then
        singularityDirective="$singularityDirective --singularity-args \"$singularityArgs\""
   fi

elif [ "$useSingularity" = false ] ; then
 :
else
 echo "Error: Parameter useSingularity must be true or false, not \"$useSingularity\""
 exit 1
fi



if [ -n "$shadowDir" ]; then
   shadowDirDirective="--shadow-prefix $shadowDir"
fi


if [ -n "$configFile" ]; then
   configfileDirective="--configfile $configFile"
fi

if [ "$nolock" = true ] ; then
   nolockDirective="--nolock"
elif [ "$nolock" = false ] ; then
  :
else
  echo "Error: Parameter nolock must be true or false, not \"$nolock\""
  exit 1
fi

if [ "$useConda" = true ] ; then
   condaDirective="--use-conda --conda-prefix $condaDir"
elif [ "$useConda" = false ] ; then
  :
else
  echo "Error: Parameter useConda must be true or false, not \"$useConda\""
  exit 1
fi

if [ "$abortAfterFirstError" = true ] ; then
   keepGoingDirective=""
elif [ "$abortAfterFirstError" = false ] ; then
  :
else
  echo "Error: Parameter abortAfterFirstError must be true or false, not \"$abortAfterFirstError\""
  exit 1
fi



if [ "$useVerbose" = true ] ; then
   verboseDirective="--verbose --reason "
   printShellCommands=true
elif [ "$useVerbose" = false ] ; then
  :
else
  echo "Error: Parameter useVerbose must be true or false, not \"$useVerbose\""
  exit 1
fi

if [ "$dryRun" = true ] ; then


   if [ "$useVerbose" = true ] ; then
      dryRunDirective="--dryrun"
   else
     dryRunDirective="--dryrun --quiet"
   fi
elif [ "$dryRun" = false ] ; then
  :
else
  echo "Error: Parameter dryRun must be true or false, not \"$dryRun\""
  exit 1
fi


if [ -n  "$allowedRules" ] ; then
   allowedRulesDirective="--allowed-rules $allowedRules"

fi


if [ -n  "$runSpecificRule" ] ; then
   runSpecificRuleDirective="--forcerun $runSpecificRule"
   if [ "$runAlsoDownstreamRules" = false ] ; then
     runSpecificRuleDirective="--forcerun $runSpecificRule --until $runSpecificRule"
     allowedRulesDirective="" # For now, reset this as the combination does not seem to work properly
   fi

fi

if [ "$ignoreTemp" = true ] ; then
   tempDirective="--notemp"
fi

if [ "$rerunIncomplete" = true ] ; then
   rerunIncompleteDirective="--rerun-incomplete"
 elif [ "$rerunIncomplete" = false ] ; then
   :
 else
   echo "Error: Parameter rerunIncomplete must be true or false, not \"$rerunIncomplete\""
   exit 1
 fi

if [ "$printShellCommands" = true ] ; then
   printShellDirective="--printshellcmds"
elif [ "$printShellCommands" = false ] ; then
   :
else
   echo "Error: Parameter printShellCommands must be true or false, not \"$printShellCommands\""
   exit 1
fi

if [ "$submitToCluster" = false ] ; then
  if [ $nCores -gt 5 ] ; then
    echo "$nCores CPUs will be used for local computation, make sure this value is ok."

  fi
elif [ "$submitToCluster" = true ] ; then
   :
else
   echo "Error: Parameter submitToCluster must be true or false, not \"$submitToCluster\""
   exit 1
fi

if [ $maxRestartsPerJob  -gt 0 ] ; then
  if [ "$submitToCluster" = true ] ; then
    maxRestartDirective="--restart-times $maxRestartsPerJob"
  fi
fi


if [ "$submitToCluster" = true ] ; then

  if [ ! -f "$clusterConfig" ];  then
    echo "Error: File $clusterConfig not found."
    exit 1
  fi

  # if [ $maxJobsCluster  -gt 500 ] ; then
  #   echo "Warning: maxJobsCluster=$maxJobsCluster too high! Adjust maxJobsCluster to 500..."
  #   maxJobsCluster=500
  # fi

  # Check if output and error directory can be written to

  outputFile=`grep -P "\\"output\\"\s*:(.+)" $clusterConfig | cut -d":" -f2 | sed 's/[\"]//g' | sed  -e 's/^[ \t]*//' | sed  -e 's/[,]*//g'`
  errorFile=`grep -P "\\"error\\"\s*:(.+)" $clusterConfig | cut -d":" -f2 | sed 's/[\"]//g' | sed  -e 's/^[ \t]*//' | sed  -e 's/[,]*//g'`

  nElemsOut=$(dirname $outputFile | uniq | wc -l)
  nElemsErr=$(dirname $errorFile | uniq | wc -l)

  if [ "$nElemsOut" -gt "1" ]; then
    echo "Error: At least two different \"output\" directories in \"$clusterConfig\" have been specified. Make sure they are all the same."
    exit 1
  fi

  if [ "$nElemsErr" -gt "1" ]; then
    echo "Error: At least two different \"error\" directories in \"$clusterConfig\" have been specified. Make sure they are all the same."
    exit 1
  fi

  outputDir=$(dirname $outputFile | uniq)
  errorDir=$(dirname $errorFile | uniq)

  if [ ! -d "$outputDir" ]; then
    echo "Error: The specified \"output\" directory \"$outputDir\" in \"$clusterConfig\" is invalid and does not exist."
    exit 1
  fi

  if [ ! -d "$errorDir" ]; then
    echo "Error: The specified \"error\" directory \"$errorDir\" in \"$clusterConfig\" is invalid and does not exist."
    exit 1
  fi

  # Check HOSTNAME
  hostname=`hostname`

  queueDirective=""
  nodesDirective=""
  nameDirective=""
  groupDirective=""
  nCPUsDirective=""
  memoryDirective=""
  maxTimeDirective=""
  outputDirective=""
  errorDirective=""
  qosDirective=""
  excludeDirective=""
  resourcesDirective=""

  generalMessage="Check the validity of the cluster file on the cluster system (SLURM or LSF) you want to run it on."


  if [ "$hostname" = "login.cluster.embl.de" ] || [ "$hostname" = "spinoza.embl.de" ] || [ "$hostname" = "seneca.embl.de" ] ; then

    echo "Use SLURM cluster because hostname is either \"login.cluster.embl.de\", \"spinoza.embl.de\" or \"seneca.embl.de\"."


    nHits=$(grep -c queueSLURM $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        queueDirective="-p {cluster.queueSLURM}"
    fi

    nHits=$(grep -c nodes $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        nodesDirective="-C {cluster.nodes}"
    fi

    nHits=$(grep -c name $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        nameDirective="-J {cluster.name}"
    fi

    nHits=$(grep -c group $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        groupDirective="-A {cluster.group}"
    fi

    nHits=$(grep -c nCPUs $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        nCPUsDirective="--cpus-per-task {cluster.nCPUs}"
    fi


    nHits=$(grep -c resources $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        resourcesDirective="{cluster.resources}"
    fi

    nHits=$(grep -c threads $clusterConfig)
    if [ "$nHits" -eq "0" ]; then
     echo "Could not find \"threads\" in file \"$clusterConfig\". Use nCPUS: \"\{threads\}\". $generalMessage";
     exit 1;
    fi


    nHits=$(grep -c memory $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        memoryDirective="--mem {cluster.memory}"
    fi

    nHits=$(grep -c maxTime $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        maxTimeDirective="--time {cluster.maxTime}"
    fi

    nHits=$(grep -c output $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        outputDirective="-o \"{cluster.output}\""
    fi


    nHits=$(grep -c exclude $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        excludeDirective="--exclude {cluster.exclude}"
    fi


    nHits=$(grep -c error $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        errorDirective="-e \"{cluster.error}\""
    fi


    nHits=$(grep -c qos $clusterConfig)
    if [ "$nHits" -gt "0" ]; then
        qosDirective="--qos={cluster.qos}"
    fi

    clusterSpecifics="--cluster \" sbatch $queueDirective $nameDirective $groupDirective $nodesDirective $nCPUsDirective $memoryDirective $maxTimeDirective $outputDirective $errorDirective $qosDirective $excludeDirective $resourcesDirective --mail-type=$mailType --parsable \""

  #clusterSpecifics=" --drmaa \" -p {cluster.queueSLURM} -J {cluster.name} -A {cluster.group} -N {cluster.nNodes} -n {cluster.nCores} --mem {cluster.memory} -o \"{cluster.output}\" -e \"{cluster.error}\" --mail-type NONE  \""


  else

    echo "Use bsub because hostname is not \"login.cluster.embl.de\""
    echo "Exiting, LSF/bsub is not supported anymore"
    exit 1

    clusterSpecifics="--cluster \" bsub  -q {cluster.queue} -J {cluster.name} -n {cluster.nCPUs} -R \"{cluster.resources}\" -M {cluster.memory} -o \"{cluster.output}\" -e \"{cluster.error}\" \""

    nHits=$(grep -c queue $clusterConfig)
    if [ "$nHits" -eq "0" ]; then
     echo "Could not find \"queue\" parameter in file \"$clusterConfig\". $generalMessage";
     exit 1;
    fi


    nHits=$(grep -c resources $clusterConfig)
    if [ "$nHits" -eq "0" ]; then
     echo "Could not find \"resources\" parameter in file \"$clusterConfig\". $generalMessage";
     exit 1;
    fi

  fi

  clusterStatusDirective=""
  if [ "$customJobStatusScript" = true ] ; then
    clusterStatusDirective="--cluster-status /g/scb/zaugg/zaugg_shared/scripts/Christian/src/Snakemake/SLURM_jobStatus.py"
  fi

  clusterDirective="--jobs $maxJobsCluster --cluster-config $clusterConfig $clusterSpecifics --local-cores 1 $clusterStatusDirective"

  # nCores=16

fi

if [ "$forceRerunAll" = true ] ; then
   forceRerunDirective="--forceall"
fi


echo ""                                    | tee -a   $logParameters
echo "####################################"  | tee -a   $logParameters
echo "# Execute the following command(s) #"  | tee -a   $logParameters
echo "####################################"  | tee -a   $logParameters

if [ -n  "$runCustomCommand" ] ; then
   echo "Run custom command only:"
   commandFull="$runCustomCommand"
   echo "$commandFull"
else

  # Run 1: Detailed summary about what files will be generated
  command1="snakemake -s $snakefile $configfileDirective $forceRerunDirective $runSpecificRuleDirective $tempDirective --detailed-summary  >$stats2"

  #Run 2: Produce a workflow graph
  command2="snakemake -s $snakefile --configfile $configFile --forceall --dag > $fileDAG"

  if [ "$skipPDFWorkflow" = true ] ; then
     command2a="echo \"skipping creation of PDF workflow graph\""
  else
     command2a="dot $fileDAG -Tpdf > $workflowGraphPDF"
  fi

  # Also do a SVG in addition for easier edits
  command2b="dot $fileDAG -Tsvg > $workflowGraphSVG"

  # Run 3: Main run: Execute the pipeline
  command3="snakemake -s $snakefile $condaDirective $nolockDirective $configfileDirective --latency-wait 60 $dryRunDirective $shadowDirDirective $touchDirective $allowedRulesDirective $tempDirective $runSpecificRuleDirective $verboseDirective  $printShellDirective $forceRerunDirective $rerunIncompleteDirective --cores $nCores $maxRestartDirective $keepGoingDirective --stats $stats $otherOptionsDirective $singularityDirective $clusterDirective"


  #commandFull="$command2 && $command2a && $command2b"

  if [ "$skipSummaryAndDAG" = true ] ; then

    commandFull="$command3"

  else

    commandFull="$command1 && $command2 && $command2a && $command2b && $command3"

    echo "$command1"  | tee -a $logParameters
    echo "#######################"  | tee   -a   $logParameters
    echo "$command2"  | tee  -a    $logParameters
    echo "#######################"  | tee  -a    $logParameters
    echo "$command2a"  | tee  -a    $logParameters
    echo "#######################"  | tee  -a    $logParameters
    echo "$command2b"  | tee   -a   $logParameters
    echo "#######################"  | tee  -a    $logParameters
  fi

  echo "$command3"  | tee   -a   $logParameters
  echo "############################################"  | tee  -a    $logParameters

fi


eval $commandFull
