library(rtracklayer)
library(GenomicRanges)
library(tidyverse)
library(magrittr)

# Input:
#   gff with some intervals asigned to transcripts, can be multiple per feature (Parent=Tr1 Tr2 ...)
#   trancripts gff (Parent=gene)
# Output:
#   bed file, name = gene_id

gff_path = snakemake@input[["gff"]] 
transcripts_path = snakemake@input[["transcripts"]]
outf = snakemake@output[[1]]

gff = import(gff_path)
transcripts = import(transcripts_path)

transcript2gene = data.frame(transcript_id = transcripts$ID, gene_id = unlist(transcripts$Parent))

gff = data.frame(gff)

# replace spaces by commas
gff$Parent = unlist(lapply(gff$Parent, function(x) paste(x, collapse = ",")))

# unnest - one 3'UTR-transcript pair per line
gff %<>% mutate(transcript_id = strsplit(as.character(Parent), ",")) %>% unnest(transcript_id)

# add gene ids
gff = merge(gff, transcript2gene, by = "transcript_id")

# keep one UTR per line
gff = gff[!duplicated(gff[, c("seqnames", "start", "end")]), ]
gff %<>% mutate(transcript_id = NULL, name = gene_id, score = 0)

gff = GRanges(gff)
export(gff, outf,  format = "bed")


