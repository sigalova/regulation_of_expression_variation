####################################################################################
################# snakemake arguments ############################################
####################################################################################

genes = snakemake@input[[1]]
outf = snakemake@output[[1]]

tss_upstr = snakemake@config[["tss_definition"]][["upstream"]]
tss_downstr = snakemake@config[["tss_definition"]][["downstream"]]

print(c(genes, outf, tss_upstr, tss_downstr))


####################################################################################
######################### Setup ####################################################
####################################################################################

library(GenomicRanges)
library(rtracklayer)

####################################################################################
#################### Getting and writing data ######################################
####################################################################################

genes = import(genes)

tss = promoters(genes, upstream = tss_upstr, downstream = tss_downstr)
tss$name = genes$ID
tss$score = 1

export(tss, outf, format = "bed")
print(paste0("Output written to: ", outf))
