<div align="justify">


## Generating the results

For each analysis you can use the corresponding script to generate our results. Note that the file names are consistent with the names of the analysis in our paper.


## Random forest models for human tissues

As in Drosophila, we used random forest regression to predict expression level and variation in three tissues (lung, ovary, and muscle), as well as mean expression level and variation. Feature selection and random forest regression were performed in the same way and with the same parameters as for Drosophila dataset. Boruta feature selection algorithm was used to select important features predictive of expression level (log-transformed) and expression variation in each of the four datasets (three tissues and average). Feature importance scores are reported in Supplementary Table 17. Random forest regressions were run on the sets of selected features for the corresponding datasets. Model performance was reported with coefficient of determination (R2) based on five-fold cross validation (Fig 6a for performance in all 4 datasets).


## Robustness tests on human datasets
Since human gene expression datasets from GTEx project contain high sample heterogeneity (different ages, sexes, reasons of death etc), we have rerun prediction models on the following subsets of individuals (using samples metadata from GTEx website) for the lung tissue expression dataset:

* Only 20-39 year old individuals
* Only 40-59 year old individuals 
* Only 60-79 year old individuals 
* Only males
* Only females
* Only violence group (as the reason of death)
* Only non-violence group (as the reason of death)

Gene expression variation and level were recalculated on the corresponding subsets of samples using the same methodology as above. Random forest regressions were rerun with the same parameters as above and on the set of features selected for the variation prediction on the full set of samples. Performance of the models was measured with R^2 on the five-fold cross-validation (Supplementary fig. 8a).


## Predicting differential expression prior in human

Differential expression Prior data (DE Prior rank) was obtained from (Crow et al. 2019). Ensembl ids were converted to entrez ids by using BioMart package in R (Durinck et al. 2009).
We had information on both DE prior and mean variation (average of 43 tissue-specific variations across individuals, see above) for 11312 human genes. As above, we reformulated variation prediction into classification task to predict top-30% (class = 1) vs. bottom-30% (class = -1) of genes ranked by their expression variation and used trained model to predict top-30% (class = 1) versus bottom-30% (class = -1) genes ranked by DE prior. Training and testing were performed on the set of features predictive of mean expression variation in the main dataset (Supplementary table 17). Training and testing were done on the non-overlapping sets of genes using the following approach. First, we defined top-prior (top-30% by DE prior) and bottom-prior genes (bottom-30% by DE prior). 50% of genes from both groups were randomly sampled and assigned to test set. From the remaining genes, top-30% and bottom-30% by mean expression variation were selected for train set. The model was trained on the test set to classify top versus bottom variable genes (random forest classification with default parameters; mlr package in R, ranger implementation of random forest). Trained model was then used on the test set to predict top versus bottom DE prior genes. Similarly, another model was both trained and tested on classifying top versus bottom DE prior genes on the same train and test sets, respectively. Performance of the models on the test set was assessed by Area Under the ROC curve (AUC). 10 rounds of random sampling of genes were performed, and mean AUC was reported (Fig 6d). 


</div>