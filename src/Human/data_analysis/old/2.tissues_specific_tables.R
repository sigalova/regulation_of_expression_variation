suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(data.table))
suppressPackageStartupMessages(library(ggpubr))
suppressPackageStartupMessages(library(ggrepel))
suppressPackageStartupMessages(library(RColorBrewer))
suppressPackageStartupMessages(library(gridExtra))
suppressPackageStartupMessages(library(ggExtra))
suppressPackageStartupMessages(library(pheatmap))
options(stringsAsFactors = FALSE)


table1 <- read.csv('/g/scb2/zaugg/shaeiri/variation_prediction/table/data/Lung.csv')
table2 <- read.csv('/g/scb2/zaugg/shaeiri/variation_prediction/table/data/Ovary.csv')
table3 <- read.csv('/g/scb2/zaugg/shaeiri/variation_prediction/table/data/Muscle.csv')


preprocess_data = function(table) {
  
  table[ ,grep("residual_cv", names(table))] = NULL
  names(table)[grep("_cv", names(table))] = "cv"
  names(table)[grep("_median", names(table))] = "median"

  names(table)[names(table) == "number_tr"] <- "number of transcripts"
  names(table)[names(table) == "mean_exons"] <- "mean exon length"
  names(table)[names(table) == "dispersion_exons"] <- "exon length mean absolute deviation"
  names(table)[names(table) == "number_exons"] <- "number of exons"
  names(table)[names(table) == "length_genes"] <- "gene length"
  names(table)[names(table) == "width_transcript"] <- "transcript width"
  names(table)[names(table) == "GCcontent"] <- "GC-content"
  names(table)[names(table) == "length"] <- "CpG length"
  names(table)[names(table) == "cpgNum"] <- "CpG num"
  names(table)[names(table) == "gcNum"] <- "GC num"
  names(table)[names(table) == "TATA"] <- "TATA-box"
  names(table)[names(table) == "width"] <- "promoter width"
  names(table)[names(table) == "type"] <- "promoter shape"
  names(table)[names(table) == "percent_broad"] <- "percent of broad"
  names(table)[names(table) == "mean_width"] <- "mean promoter width"
  
  names(table)[names(table) == "mean_TssA"] <- "mean TssA"
  names(table)[names(table) == "mean_TssAFlnk"] <- "mean TssAFlnk"
  names(table)[names(table) == "mean_TxFlnk"] <- "mean TxFlnk"
  names(table)[names(table) == "mean_Tx"] <- "mean Tx"
  names(table)[names(table) == "mean_TxWk"] <- "mean TxWk"
  names(table)[names(table) == "mean_EnhG"] <- "mean EnhG"
  names(table)[names(table) == "mean_Enh"] <- "mean Enh"
  names(table)[names(table) == "mean_ZNF_Rpts"] <- "mean ZNF_Rpts"
  names(table)[names(table) == "mean_Het"] <- "mean Het"
  names(table)[names(table) == "mean_TssBiv"] <- "mean TssBiv"
  names(table)[names(table) == "mean_BivFlnk"] <- "mean BivFlnk"
  names(table)[names(table) == "mean_EnhBiv"] <- "mean EnhBiv"
  names(table)[names(table) == "mean_ReprPC"] <- "mean ReprPC"
  names(table)[names(table) == "mean_ReprPCWk"] <- "mean ReprPCWk"
  names(table)[names(table) == "mean_Quies"] <- "mean Quies"
  
  
  table$si <- NULL
  table$mean_si <- NULL
  
  table$H3K4me3 <- NULL
  table$H3K4me1 <- NULL
  table$H3K9me3 <- NULL
  table$H3K9ac <- NULL
  table$H3K27me3 <- NULL
  table$H3K27ac <- NULL
  table$H3K36me3 <- NULL
  
  table$mean_H3K4me1 <- NULL
  table$mean_H3K4me3 <- NULL
  table$mean_H3K9me3 <- NULL
  table$mean_H3K9ac <- NULL
  table$mean_H3K27me3 <- NULL
  table$mean_H3K27ac <- NULL
  table$mean_H3K36me3 <- NULL
  
  table$number_TFs <- table$pos_tfs + table$neg_tfs
  table$pos_tfs <- NULL
  table$neg_tfs <- NULL
  names(table)[names(table) == "number_TFs"] <- "number of TFs"
  
  ## filter ##
  
  table %<>% filter(median > 5)
  
  span1 <- 0.25
  degree1 <- 1
  model1 <- loess(cv ~ median, data = table, degree = degree1, span = span1)
  r1 <- resid(model1)
  
  table$resid_cv <- r1
  
  #table$gene_id <- NULL
  table$cv <- NULL
  
  names(table) = gsub(" ", "_", names(table))

  table  

}

df1 = preprocess_data(table1)
df2 = preprocess_data(table2)
df3 = preprocess_data(table3)


outdir = file.path(project_folder, "/analysis/human_data")

of1 = file.path(outdir, "/mt_lung.csv")
write.csv(df1, of1, row.names = FALSE)

of2 = file.path(outdir, "/mt_ovary.csv")
write.csv(df2, of2, row.names = FALSE)

of3 = file.path(outdir, "/mt_muscle.csv")
write.csv(df3, of3, row.names = FALSE)



