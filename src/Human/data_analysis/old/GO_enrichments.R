suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(magrittr))
suppressPackageStartupMessages(library(rtracklayer))
suppressPackageStartupMessages(library(data.table))
#suppressMessages(library(gProfileR))
suppressMessages(library(clusterProfiler))
library('org.Hs.eg.db')
source("/g/furlong/project/62_expression_variation/src/shared/utils.R")

config = load_config() # json file with all paths to data
project_folder = config$project_parameters$project_dir

outdir = file.path(project_folder, "/analysis/GO_enrichments")
make_recursive_dir(outdir)

path = file.path(project_folder, "/data/datasets_for_validation")

df_h = read.csv(file.path(path, "/lung_final_prior_table.csv"))

df_h %>% group_by(share_broad)%>% summarize(median(residual_cv), n()) 

#tmp = df_h %>% filter(share_broad < 2) %>% select(residual_cv, gene_id, entrezgene_id)

tmp = df_h

tmp %<>%
  filter(share_broad %in% c(0, 3)) %>% 
  mutate(shape = ifelse(share_broad == 3, "broad", "narrow")) %>%
  group_by(shape) %>%
  mutate(bin_var = cut(residual_cv, quantile(residual_cv, 0:3/3), include.lowest = TRUE, label = FALSE))

res= compareCluster(entrezgene_id ~ shape + bin_var, data = tmp, 
                                fun="enrichGO", OrgDb = org.Hs.eg.db,  
                                pvalueCutoff=0.01,  ont = "BP")



df = data.frame(res)
df$GeneRatio = sapply(df$GeneRatio, function(x) eval(parse(text = x)))
df %<>% arrange(bin_var, p.adjust) 

write.csv(df , file.path(outdir, "GO_q3.shape_separate_quant.bp_pval01.csv"), row.names = FALSE)



# MF
formula_res_4q = compareCluster(Entrez ~ bin4_var + shape, data = mt, 
                                fun="enrichGO", OrgDb = org.Dm.eg.db,  
                                pvalueCutoff=0.01,  ont = "MF")

df = data.frame(formula_res_4q)
df$GeneRatio = sapply(df$GeneRatio, function(x) eval(parse(text = x)))
df %<>% arrange(shape, bin4_var, p.adjust) 

write.csv(df , file.path(outdir, "GO_q4.shape_separate_quant.mf_pval01.csv"), row.names = FALSE)



# select 4 top matches per group
df_subset = df %>% 
  #  filter(filter == "k") %>%
  group_by(shape, bin_var) %>% 
  mutate(r = rank(pvalue)) %>%
  filter(r <= 5)

# for selected categories - check matches in other groups
df_subset = df[df$Description %in% df_subset$Description, ]
df_subset %<>% arrange(shape, bin_var, pvalue) 
df_subset$Description = factor(df_subset$Description, levels = rev(unique(df_subset$Description)))

#df_subset$quantile = ifelse(df_subset$bin4_var == 1, "q25", "q75")

pmin = min(df_subset$p.adjust)

ggplot(df_subset, aes(x = factor(bin_var), y = Description, size = GeneRatio, color = p.adjust)) +
  geom_point() +
 # facet_wrap(~shape) +
  theme_bw() +
  ylab("") + 
  #  scale_color_gradient(low = "darkred", high = "darkblue", trans = "log10", limits = c(pmin, 0.05), name = "p-value\n") +
  xlab("Quartiles by expression variation") + ylab("") + 
  theme(axis.text.y = element_text(size=16), axis.text.x = element_text(size=16), 
        axis.title.x = element_text(size=16), axis.title.y = element_text(size=16),
        legend.text=element_text(size=16), legend.title=element_text(size=16),
        strip.text.x = element_text(size = 16), strip.text.y = element_text(size = 16))

ggsave(filename = file.path(path, "GO_q4_variation.bp_pval01.pdf"), width = 15, height = 8)




tmp = df_h

tmp %<>%
  mutate(bin_var = cut(residual_cv, quantile(residual_cv, 0:4/4), include.lowest = TRUE, label = FALSE))

res= compareCluster(entrezgene_id ~ bin_var, data = tmp, 
                    fun="enrichGO", OrgDb = org.Hs.eg.db,  
                    pvalueCutoff=0.01,  ont = "BP")



df = data.frame(res)
df$GeneRatio = sapply(df$GeneRatio, function(x) eval(parse(text = x)))
df %<>% arrange(bin_var, p.adjust) 

write.csv(df , file.path(outdir, "GO_q3.shape_separate_quant.bp_pval01.csv"), row.names = FALSE)

