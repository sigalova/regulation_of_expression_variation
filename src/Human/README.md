<div align="justify">

## Generating the results

* #### The order of using the codes:

You can first use `features` folder to produce features, and then you can use `target` folder to produce gene expression variation and gene expression level. After producing features and target you can use `making_table` folder to produce final tables. In the end you can use `data_analysis` folder to predict and do related analysis.

You can also check `data_paths.txt` for seeing the summary of data paths!

</div>