<div align="justify">

## Generating the results

* #### Downloading the raw data:

You can download raw EBI GWAS catalog data from [EBI website](https://www.ebi.ac.uk/gwas/docs/file-downloads). For downloading essential genes and drug targets you can check [MacArthur Lab Git repo](https://github.com/macarthur-lab/gene_lists). Also, for downloading the DE prior data you can check [this git repo](https://github.com/maggiecrow/DEPrior).

For this analysis we used another reference model, which you can download from the [Ensembl website](http://ftp.ensemblorg.ebi.ac.uk/pub/release-75/gtf/homo_sapiens).

* #### Producing final tables:

You can use `which.R` for selecting the main transcript per gene, and removing some chromosomes such as sex chromosomes and non-standard chromosomes.

You can use `lung.R`, `muscle.R`, and `ovary.R` and then `tissues_final.R` to produce final tables for three three tissues.

You can use `share_prior_disease.R` to produce final tables for the aggregated dataset, prior, and disease.


## Genome details

Sex chromosomes and non-standard chromosomes were removed for all subsequent analyses. For selecting the main transcript per gene we used GRCH37/hg19 genome annotation downloaded from Ensembl website (Cunningham et al. 2019).


## Compiling final feature tables

We collated three tissue-specific datasets for lung, muscle, and ovary by combining the above promoter features and tissue-specific expression data (Supplementary tables 13-15). These tables included three types of features:

* Tissue-specific features (promoter width and chromatin states)
* Features aggregated across tissues (mean promoter width, percentage of broad, mean chromatin states – see above)
* Non tissue-specific features (all other features, e.g. TATA-box or TF peaks)

These tables included genes that were expressed and had CAGE signal (passing the above filtering criteria in both datasets) in the corresponding tissues. For muscle tissue, ‘Skeletal muscle male’ dataset was used for tissue-specific chromatin states. The fourth feature table included only non-tissue-specific and aggregated features along with mean expression level and variation (Supplementary table 16). This table was comprised of genes that were expressed and had CAGE signal in a least one of the analysed tissues. Expression variation was adjusted for the expression level on these final sets of genes in each table (see above).


## Differential expression Prior data

Differential expression Prior data (DE Prior rank) was obtained from (Crow et al. 2019). Ensembl ids were converted to entrez ids by using BioMart package in R (Durinck et al. 2009).


## Essential genes, drug targets, and GWAS catalogue

Essential genes (essential in multiple cultured cell lines based on CRISPR/Cas screens (Hart et al. 2017)) and drug targets (FDA-approved drug targets (Wishart et al. 2018) and drug targets according from (Nelson et al. 2015)) were downloaded from Macarthur lab repository (https://github.com/macarthur-lab/gene_lists). GWAS dataset was downloaded from EBI GWAS catalog (Buniello et al. 2019). Genes with GWAS associations within upstream regions or downstream regions were considered. These gene annotations were used in Fig. 6f and Supplementary fig. 8c, but not included in prediction models. Information on these gene types is provided in Supplementary table 9.

</div>