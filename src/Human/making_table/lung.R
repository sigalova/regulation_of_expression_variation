
## Essential libraries.
library(plyr)
library(dplyr)


## Reading tables.
loc0 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/making_table/which/which.csv"
which <- read.csv(loc0, header = TRUE, sep = ",")
table <- which


loc1 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/target/results/variations/variation_v3_Lung.csv"
target <- read.csv(loc1, header = TRUE, sep = ",")
table <- join(table, target, by = "gene_id", type = "inner")
table$Lung_mean <- NULL


loc2 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/genes/results/genes.csv"
genes <- read.csv(loc2, header = TRUE, sep = ",")
names(genes)[1] <- "gene_id"
table <- join(genes, table, by = "gene_id", type = "inner")


loc3 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/exons/results/exons.csv"
exons <- read.csv(loc3, header = TRUE, sep = ",")
names(exons)[1] <- "gene_id"
table <- join(exons, table, by = "gene_id", type = "inner")


loc4 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/transcripts/results/tr_number.csv"
tr_number <- read.csv(loc4, header = TRUE, sep = ",")
table <- join(tr_number, table, by = "gene_id", type = "inner")


loc5 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/transcripts/results/transcripts.csv"
transcripts <- read.csv(loc5, header = TRUE, sep = ",")
table <- join(table, transcripts, by = "TXNAME", type = "inner")


loc6 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/gccontents/results/gccontents.csv"
gccontents <- read.csv(loc6, header = TRUE, sep = ",")
table <- join(table, gccontents, by = "TXNAME", type = "inner")


loc7 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/CpGislands/results/CpGislands.csv"
CpGislands <- read.csv(loc7, header = TRUE, sep = ",")
table <- join(table, CpGislands, by = "TXNAME", type = "inner")


loc8 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/TFs-motifs/results/tata.csv"
tata <- read.csv(loc8, header = TRUE, sep = ",")
table <- join(table, tata, by = "TXNAME", type = "inner")


loc9 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/TFs-motifs/results/TFs2.csv"
TFs <- read.csv(loc9, header = TRUE, sep = ",")
table <- join(table, TFs, by = "TXNAME", type = "inner")


loc10 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/promoter_architecture/results/lung_shape.csv"
# loc10 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/promoter_architecture/results/lung/lung_shape.csv"
shape <- read.csv(loc10, header = TRUE, sep = ",")
table <- join(table, shape, by = "TXNAME", type = "inner")
table$type <- ifelse(table$type == "Narrow", 1, 0)


loc11 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/promoter_architecture/results/share/share_shape.csv"
share_shape <- read.csv(loc11, header = TRUE, sep = ",")
table <- join(table, share_shape, by = "TXNAME", type = "inner")
table$share_broad <- NULL
table$median_si <- NULL
table$median_width <- NULL


loc12 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/states/E096.csv"
state <- read.csv(loc12, header = TRUE, sep = ",")
table <- join(table, state, by = "TXNAME", type = "inner")


loc13 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/share_states"
share_state <- read.csv(loc13, header = TRUE, sep = ",")
table <- join(table, share_state, by = "TXNAME", type = "inner")
table$median_BivFlnk <- NULL
table$median_Enh <- NULL
table$median_EnhBiv <- NULL
table$median_EnhG <- NULL
table$median_Het <- NULL
table$median_Quies <- NULL
table$median_ReprPC <- NULL
table$median_ReprPCWk <- NULL
table$median_TssA <- NULL
table$median_TssAFlnk <- NULL
table$median_TssBiv <- NULL
table$median_Tx <- NULL
table$median_TxFlnk <- NULL
table$median_TxWk <- NULL
table$median_ZNF_Rpts <- NULL


loc14 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/share_histones1"
share_histones <- read.csv(loc14, header = TRUE, sep = ",")
table <- join(table, share_histones, by = "TXNAME", type = "inner")
table$median_H3K4me1 <- NULL
table$median_H3K9me3 <- NULL
table$median_H3K27ac <- NULL
table$median_H3K9ac <- NULL
table$median_H3K27me3 <- NULL
table$median_H3K36me3 <- NULL
table$median_H3K4me3 <- NULL


loc15 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/histones/GSM906395_UCSD.Lung.H3K27ac.STL002.csv"
loc16 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/histones/GSM906411_UCSD.Lung.H3K9me3.STL002.csv"
loc17 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/histones/GSM910572_UCSD.Lung.H3K4me1.STL002.csv"
loc18 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/histones/GSM915336_UCSD.Lung.H3K4me3.STL002.csv"
loc19 <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/features/chromatins/results/histones/GSM956014_UCSD.Lung.H3K36me3.STL002.csv"
loc <- c(loc15, loc16, loc17, loc18, loc19)
label <- c("H3K27ac", "H3K9me3", "H3K4me1", "H3K4me3", "H3K36me3")
for (i in 1:length(loc)) {
  y <- read.csv(loc[i], header = TRUE, sep = ",")
  names(y)[2] <- label[i]
  table <- join(table, y, by = "TXNAME", type = "inner")
}


## Adding new features.
list1 <- c()
list2 <- c()
for (i in seq(17, 460)) {
  
  sd <- sd(table[, i])
  if (sd == 0 | is.na(sd))
    next
  
  value <- cor(table$Lung_residual_cv, table[, i])
  
  if (value > 0)
    list1 <- c(list1, names(table)[i])
  else
    list2 <- c(list2, names(table)[i])
}

matrix1 <- as.matrix(table[, list1])
sum1 <- rowSums(matrix1)

matrix2 <- as.matrix(table[, list2])
sum2 <- rowSums(matrix2)

table$pos_tfs <- sum1
table$neg_tfs <- sum2


## Removing features!
table$TXNAME <- NULL


## Saving.
location <- "/g/scb2/zaugg/shaeiri/variation_prediction/table/data/Lung.csv"
write.csv(table, location, row.names = FALSE)

