<div align="justify">

## Generating the results

* #### Downloading the raw data:
You can download the raw data from the [GTEx website](https://www.gtexportal.org/home/). You need 5 different files, which can be downloaded from the following link. Note that, files name are the same as original files.

[Gene expression matrix/Gene read counts](https://storage.googleapis.com/gtex_analysis_v8/rna_seq_data/GTEx_Analysis_2017-06-05_v8_RNASeQCv1.1.9_gene_reads.gct.gz)

[Four GTEx sample details](https://gtexportal.org/home/datasets)

* #### Producing the Expression variation and the Expression level:
You can use `exp.R` to produce the Expression level and Expression variation for the arbitrary tissue. It is notable that this code also contains all plots and sanity checks that we used to produce the final data. 

You can use `main.sh`, `submit.sh`, and `job.R` to produce the results for all tissues. Actually, `job.R` is based on a summarization of the `exp.R` file, which is explained below.

You can also use `agg.R` to make aggrigated expression level and expression variation data, which will be used for making the final table.

## Quantifying expression level and variation

Gene expression matrix (raw read counts) was downloaded from the GTEx project website (GTEx Consortium 2013). Gene read counts matrices per tissues were produced by using GTEx sample details downloaded from GTEx website. Tissues with more than 100 samples (43 tissues in total) were chosen for further analysis (Supplementary table 8). In each tissue, genes with 0 median counts were removed and expression counts were normalized using size factor normalization form DESeq2 package in R (Love, Anders, and Huber 2014). Median expression levels were calculation for each gene in each tissue and converted to log-scale (natural logarithm) for subsequent analysis.

Next, we removed top-5% of genes by median expression level as potential outliers, following the same reasoning as for Drosophila. Since distributions of gene expression in all tissues had long left tails, we set additional stringent threshold on lowly expressed genes (minimum median of 5).

Gene expression variation was calculated on the final set of genes for each tissue following the same approach as for Drosophila. Namely, gene expression variation was defined as the residuals from the local linear regression of coefficient of variation (CV) on the median expression (both on the log-scale, loess function in R from stats library, span = 0.25 and degree = 1). Gene expression levels and variations in all tissues are provided in Supplementary table 9.

Mean expression variation for each gene was calculated as the mean of expression variations in all tissues where a gene was expressed using final tables that passed all filtering steps. Similarly, mean expression level was calculated by computing the mean of median expression levels in all tissues where a gene was expressed. Mean expression variation calculated in this way exhibited weak dependence on mean expression level (Spearman correlation, Rho=-0.11). To control for this effect, we also calculated ‘global mean variation’ as the residuals from the local linear regression of the mean CV on the mean expression level (calculated as above). This measure was highly correlated with mean variation (Supplementary fig S6) and showed similar results in the downstream analysis (results not shown, global mean variation is provided in Supplementary Table 9).

</div>