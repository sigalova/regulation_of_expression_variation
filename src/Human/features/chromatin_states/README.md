<div align="justify">

## Generating the results

* #### Downloading the raw data:
You can download the raw data from [Epigenomics Roadmap project website](https://egg2.wustl.edu/roadmap/web_portal/chr_state_learning.html). For choosing the cell type you can check [this link](https://egg2.wustl.edu/roadmap/figures/mainFigs/Figure_2.jpg). Also, we used tissues, which are mentioned in 'names.txt'. And you can use 'unzip.sh' for unziping the downloaded files.

* #### Producing chromatin states related features:
You can use `chromatin_states.R` to produce the related fatures for the arbitrary tissue.

You can use `agg_states.R`, and `share_states.R` to produce the aggregated features. Actually, `share_states.R` is based on a summarization of the `chromatin_states.R` file, which is explained below.


## Chromatin States

Chromatin States dataset (chromHMM core 15-state model with 5 marks and 127 epigenomes (Ernst and Kellis 2017)) was downloaded from Epigenomics Roadmap project. We considered 26 tissues (Supplementary table 12). For each tissue, 15 features (one for each state, e.g. TssA or TssBiv) were obtained. Each feature was calculated by overlapping corresponding state regions and gene TSS-proximal regions (-500/+500 bp) and counting the number of overlaps for each region. Finally, aggregated features (e.g. mean_TssA or mean_TssBiv ) were calculated as the mean of feature values for each state over all 26 tissues.

</div>