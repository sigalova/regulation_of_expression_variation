<div align="justify">

## Generating the results

* #### Downloading the raw data:
For this analysis we used another reference model, which you can download from the [Ensembl website](http://ftp.ensemblorg.ebi.ac.uk/pub/release-75/gtf/homo_sapiens).

* #### Producing transcripts overview related features:
You can use `transcrpts.R` to produce the related fatures. And you can use `tr_number.R` for producing number of transcripts per gene feature.


## Gene properties

Number of transcripts, gene length, mean exon length, number of exons and exon length mean absolute deviation were calculated for each gene directly using hg19 genome annotation from Ensembl website (Cunningham et al. 2019). Transcripts width was calculated for each transcript by using the same file, and length of the main transcript was assigned to each gene.

</div>