<div align="justify">

## Generating the results

* #### Downloading the raw data:
You can produce the raw data of motifs from [EPFL pwm tools](https://ccg.epfl.ch//pwmtools/pwmscan.php). And raw data of TFs can be downloaded from [this link](https://figshare.com/articles/Human_and_mouse_cistromes_genomic_maps_of_putative_cis-regulatory_regions_bound_by_transcription_factors/7087697).

* #### Producing motifs related features:
You can use `motifs.R` to produce the related fatures.

* #### Producing TFs related features:
You can use `TFs.R` to produce the related fatures.


## TATA-box motif
TATA-box motif coordinates were obtained from the PWMTools web server (Ambrosini, Groux, and Bucher 2018): JASPAR core 2018 vertebrates motif library (Khan et al. 2018), p-value cutoff of 10-4, GRCh37/hg19 genome assembly). Motif coordinates were overlapped with gene core promoter regions (-300/+200 bp), and number of overlaps for each gene was recorded (TATA_box feature).


## Transcription Factors
Transcription factors dataset (444 TFs, peaks with motifs, hg19 genome) ware obtained from (Vorontsov et al. 2018). If several datasets were available for the same TF, the dataset with the best quality was selected. For each TF, the corresponding feature was calculated by overlapping the TF regions and gene TSS-proximal regions (-500/+500 bp) and counting the number of overlaps for each gene.

</div>